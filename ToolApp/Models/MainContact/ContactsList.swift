//
//  ContactsList.swift
//  Danone
//
//  Created by Renendra Saxena on 07/01/19.
//  Copyright © 2019 Zaman Meraj. All rights reserved.
//

import Foundation

class ContactsList {

    let id: Int!
    let name: String!
    let job_title: String!
    let description: String!
    let phone_number: String!
    let email_address: String!
    let avatar: String!
   
    init(responseDict: NSDictionary) {
        self.id = responseDict.value(forKey: "id") as? Int ?? 0
        self.name = responseDict.value(forKey: "name") as? String ?? ""
        self.job_title = responseDict.value(forKey: "job_title") as? String ?? ""
        self.description = responseDict.value(forKey: "description") as? String ?? ""
        self.avatar = responseDict.value(forKey: "avatar") as? String ?? ""
        self.phone_number = responseDict.value(forKey: "phone_number") as? String ?? "Normal"
        self.email_address = responseDict.value(forKey: "email_address") as? String ?? ""
    }
}
