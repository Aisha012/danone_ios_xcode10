//
//  GalleryCategoryViewController.swift
//  ToolApp
//
//  Created by Phaninder on 05/05/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import OpalImagePicker
import Lightbox
import MediaPlayer
import AVFoundation
import AVKit
import Photos
import Alamofire
import MobileCoreServices


class GalleryCategoryViewController: BaseClassVC , UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    @IBOutlet weak var addPhotoBtn: UIButton!
    
    var showMenuButton = true
    private let refreshControl = UIRefreshControl()
    var eventId: String?
    var eventData: EventData!
    var categories: [GalleryCategory]!
    var photosPagination = GalleryPagination()
    var isFirstTime = true
    var isFetchingData = false
    var galleryCategory: GalleryCategory!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id
        addPhotoBtn.addTarget(self , action: #selector(addMoreBtn), for: .touchUpInside)
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        
        self.titleLabel.text = "Albums".localized
        
        
        self.collectionView.dataSource = self
        self.collectionView.delegate   = self
        self.collectionView.isScrollEnabled = true
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 4.0
            layout.minimumInteritemSpacing = 4.0
            layout.scrollDirection = .vertical
        }
        self.collectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func showViewsAccordingly() {
        if photosPagination.photos.count == 0 {
            collectionView.isHidden = true
            addPhotoBtn.isHidden = false
//            emptyPlaceholderLabel.isHidden = false
        } else {
            collectionView.reloadData()
            addPhotoBtn.isHidden = true

//            emptyPlaceholderLabel.isHidden = true
        }
    }
    @objc func refreshData(_ sender: Any) {
        
        //        fetchGallery()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    @objc func addMoreBtn(_ sender: Any) {
        let alert = UIAlertController(title: "Upload Media".localized, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Take Photo".localized, style: .default, handler: { _ in
            self.camera()
        }))
        
        alert.addAction(UIAlertAction(title: "Photo Gallery".localized, style: .default, handler: { _ in
            self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction(title: "Record Video".localized, style: .default, handler: { _ in
            _ = self.startCameraFromViewController(viewController: self, withDelegate: self)
        }))
        
        alert.addAction(UIAlertAction(title: "Video Gallery".localized, style: .default, handler: { _ in
            self.videoLibrary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary() {
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = 10
        
        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        
        let configuration = OpalImagePickerConfiguration()
        configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select more than 10 images", comment: "")
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        imagePicker.navigationBar.tintColor = UIColor.black
        imagePicker.selectionImageTintColor = UIColor.black
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        imagePicker.configuration = configuration
        imagePicker.imagePickerDelegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    func videoLibrary() {
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = 1
        
        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.video])
        
        let configuration = OpalImagePickerConfiguration()
        configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select more than 1 video", comment: "")
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        imagePicker.navigationBar.tintColor = UIColor.black
        imagePicker.selectionImageTintColor = UIColor.black
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        imagePicker.configuration = configuration
        imagePicker.imagePickerDelegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    func startCameraFromViewController(viewController: UIViewController, withDelegate delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate) -> Bool {
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            return false
        }
        
        let cameraController = UIImagePickerController()
        cameraController.sourceType = .camera
        cameraController.mediaTypes = [kUTTypeMovie as NSString as String]
        cameraController.allowsEditing = false
        cameraController.delegate = delegate
        
        present(cameraController, animated: true, completion: nil)
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
     
        let mediaType =  info[.mediaType] as? String
        
        if mediaType == "public.movie" {
            let videoPath = info[.mediaURL] as! URL
            self.encodeVideo(videoPath)
            //            let _ = encodeVideo(videoUrl: videoPath) { (url) in
            //                print(url)
            //
            //                DispatchQueue.main.async {
            //                    self.uploadVideo(url!)
            //                }
            //            }
        } else {
            let image = info[.originalImage] as! UIImage
            uploadImages([image])
        }
        
        dismiss(animated: true, completion: nil)
    }
    func uploadImages(_ images: [UIImage]) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        //
        if let auth_Token = auth_Token {
            
            self.showHUD(forView: self.view, excludeViews: [])
            let params = ["user_token": auth_Token,
                          "api_token": apiToken,
                          "caption": "",
                          "category": ""] as [String : Any]
            //                          "category": galleryCategory.id!] as [String : Any]
            
            let url = "http://eu.aio-events.com/api/events/\(eventId!)/galleries?user_token=\(auth_Token)&api_token=\(apiToken)"
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                for (key, value) in params {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                // import image to request
                for (index, image) in images.enumerated() {
                    if let imageData = image.jpegData(compressionQuality: 0.5) {
                        multipartFormData.append(imageData, withName: "picture[\(index)]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                    }
                }
            }, to: url,
               
               encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print(response.response as Any) // URL response
                        self.photosPagination.paginationType = .reload
                        DispatchQueue.main.async {
                            self.hideHUD(forView: self.view)
                            self.isFirstTime = true
                            self.fetchGallery()
                        }
                    }
                case .failure(let error):
                    //Show alert
                    print(error)
                }
                
            })
        }
    }
    
    func fetchGallery() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            if photosPagination.paginationType != .old {
                self.photosPagination.currentPageNumber = 1
                if isFirstTime == true {
                    self.showHUD(forView: self.view, excludeViews: [])
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                }
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            isFetchingData = true
            
//            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/galleries/\(galleryCategory.id!)?user_token=\(auth_Token)&api_token=\(apiToken)&page=\(photosPagination.currentPageNumber))", parameter: nil, type: .get) { (netResponse)
            
                  _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/galleries?user_token=\(auth_Token)&api_token=\(apiToken)&page=\(photosPagination.currentPageNumber))", parameter: nil, type: .get) { (netResponse)
                -> (Void) in
                self.isFetchingData = false
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        let newPagination = GalleryPagination(array: JSON)
                        self.photosPagination.appendDataFromObject(newPagination)
                        DispatchQueue.main.async {
                            if self.photosPagination.paginationType != .old && self.isFirstTime == true {
                                self.hideHUD(forView: self.view)
                                self.isFirstTime = false
                            } else {
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                            self.showViewsAccordingly()
                        }
                    } else {
                        self.hideHUD(forView: self.view)
                    }
                }else {
                    self.hideHUD(forView: self.view)
                }
            }
        }
    }
    
    
    func encodeVideo(_ videoURL: URL)  {
        
        let avAsset = AVURLAsset(url: videoURL, options: nil)
        
        let startDate = Foundation.Date()
        
        //Create Export session
        let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough)
        
        // exportSession = AVAssetExportSession(asset: composition, presetName: mp4Quality)
        //Creating temp path to save the converted video
        
        
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let myDocumentPath = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("temp.mp4").absoluteString
        let url = URL(fileURLWithPath: myDocumentPath)
        
        let documentsDirectory2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
        
        let filePath = documentsDirectory2.appendingPathComponent("rendered-Video.mp4")
        deleteFile(filePath)
        
        //Check if the file already exists then remove the previous file
        if FileManager.default.fileExists(atPath: myDocumentPath) {
            do {
                try FileManager.default.removeItem(atPath: myDocumentPath)
            }
            catch let error {
                print(error)
            }
        }
        
        
        
        exportSession!.outputURL = filePath
        exportSession!.outputFileType = AVFileType.mp4
        exportSession!.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
        let range = CMTimeRangeMake(start: start, duration: avAsset.duration)
        exportSession!.timeRange = range
        
        exportSession!.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession!.status {
            case .failed:
                print("%@",exportSession!.error)
            case .cancelled:
                print("Export canceled")
            case .completed:
                //Video conversion finished
                let endDate = Foundation.Date()
                
                let time = endDate.timeIntervalSince(startDate)
                print(time)
                print("Successful!")
                print(exportSession!.outputURL)
                
                DispatchQueue.main.async {
                    self.uploadVideo(exportSession!.outputURL!)
                }
            default:
                break
            }
            
        })
        
        
    }
    
    func deleteFile(_ filePath:URL) {
        guard FileManager.default.fileExists(atPath: filePath.path) else {
            return
        }
        
        do {
            try FileManager.default.removeItem(atPath: filePath.path)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    func uploadVideo(_ videoPath: URL) {
                let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        //
                if let auth_Token = auth_Token {
        
                    self.showHUD(forView: self.view, excludeViews: [])
                    let params = ["user_token": auth_Token,
                                  "api_token": apiToken,
                                  "caption": "",
                                  "category": ""] as [String : Any]
        
                    let url = "http://eu.aio-events.com/api/events/\(eventId!)/galleries?user_token=\(auth_Token)&api_token=\(apiToken)"
        
                    Alamofire.upload(multipartFormData: { multipartFormData in
                        for (key, value) in params {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                        }
                        multipartFormData.append(videoPath, withName: "picture", fileName: "\(Date().timeIntervalSince1970).mp4", mimeType: "video/mp4")
                    }, to: url,
        
                       encodingCompletion: { encodingResult in
        
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                self.photosPagination.paginationType = .reload
                                DispatchQueue.main.async {
                                    self.hideHUD(forView: self.view)
                                    self.isFirstTime = true
                                    self.fetchGallery()
                                }
                            }
                        case .failure(let error):
                            //Show alert
                            print(error)
                        }
        
                    })
                }
    }
    
    func generateBoundaryString() -> String{
        return "Boundary-\(UUID().uuidString)"
    }
    
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension GalleryCategoryViewController: UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCollectionViewCell.cellIdentifier(), for: indexPath) as! CategoryCollectionViewCell
        cell.categoryName.text = categories[indexPath.row].name ?? ""
        cell.categoryImageView.sd_setImage(with: URL(string: categories[indexPath.row].pictures[0]), placeholderImage: UIImage(named: "gallery_placeholder"), options: .retryFailed, completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "PhotoGallaryVc") as! PhotoGallaryVc
        destination.galleryCategory = categories[indexPath.row]
        destination.eventData = eventData
        destination.showMenuButton = false
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    
}

extension GalleryCategoryViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.collectionView.bounds.width / 2) - 15 , height: 175.0)
        
    }
}
extension GalleryCategoryViewController: OpalImagePickerControllerDelegate {
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        if assets[0].mediaType == PHAssetMediaType.video {
            assets[0].requestContentEditingInput(with: PHContentEditingInputRequestOptions(), completionHandler: { (contentEditingInput, dictInfo) in
                if let strURL = (contentEditingInput!.avAsset as? AVURLAsset)?.url {
                    self.encodeVideo(strURL)
                }
            })
        } else {
            var imagesArray = [UIImage]()
            for asset in assets {
                if let image = getUIImage(asset: asset) {
                    imagesArray.append(image)
                }
            }
            
            uploadImages(imagesArray)
        }
        dismiss(animated: true , completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        print("cancel")
    }
    
    func getUIImage(asset: PHAsset) -> UIImage? {
        
        var img: UIImage?
        let manager = PHImageManager.default()
        let options = PHImageRequestOptions()
        options.version = .original
        options.isSynchronous = true
        manager.requestImageData(for: asset, options: options) { data, _, _, _ in
            
            if let data = data {
                img = UIImage(data: data)
            }
        }
        return img
    }
}

extension GalleryCategoryViewController: LightboxControllerPageDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

extension GalleryCategoryViewController: LightboxControllerDismissalDelegate {
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
    
    func lightboxShareOption(_ controller: LightboxController, forItem index: Int) {
        // ...
    }
    
}

