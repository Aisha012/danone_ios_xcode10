//
//  Participant.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class Participant {
    
    let clientId: Int!
    let firstName: String!
    let lastName: String!
    let email: String!
    let qrCode: String!
    let role: String!
    let avatar: String!
    let authenticationToken: String!
    let position: String!
    let organization: String!
    let mobile: String!
    let category: String!
    let country: String!
    
    init(responseDict: NSDictionary) {
        self.clientId = responseDict.value(forKey: "id") as? Int ?? 0
        self.firstName = responseDict.value(forKey: "first_name") as? String ?? ""
        self.lastName = responseDict.value(forKey: "last_name") as? String ?? ""
        self.email = responseDict.value(forKey: "email") as? String ?? ""
        self.avatar = responseDict.value(forKey: "avatar") as? String ?? ""
        self.role = responseDict.value(forKey: "role") as? String ?? "Normal"
        self.qrCode = responseDict.value(forKey: "qr_code_string") as? String ?? ""
        self.authenticationToken = responseDict.value(forKey: "authentication_token") as?
            String ?? ""
        self.position = responseDict.value(forKey: "position") as? String ?? ""
        self.organization = responseDict.value(forKey: "organization") as? String ?? ""
        self.mobile = responseDict.value(forKey: "mobile") as? String ?? ""
        self.category = responseDict.value(forKey: "category") as? String ?? ""
        self.country = responseDict.value(forKey: "country") as? String ?? ""
    }
    

    
}
