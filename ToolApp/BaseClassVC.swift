//
//  BaseClassVC.swift
//  REGAIN_GAD
//
//  Created by Prashant on 01/06/17.
//  Copyright © 2017 Prashant. All rights reserved.
//

import UIKit
import CoreData

class BaseClassVC: UIViewController {
   
    var delegate            :   SideManu_Vc?
    var commonView          =   UIView()
    var lodingView          =   UIView()
    var spinner             =   UIActivityIndicatorView()
    var transparentView     =   UIView()
    var isSlideViewOpen     :   Bool    =   false
    var sideView            =   SideManu_Vc()
    let btnShowMen  =   UIButton(type: UIButton.ButtonType.custom)
    var font                : Int     = 15
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.createLoader()

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func createLoader(){
        spinner.style  =   UIActivityIndicatorView.Style.whiteLarge
        spinner.center      =   self.view.center
        spinner.color       =   UIColor.white
        spinner.hidesWhenStopped    =   true
        commonView.frame    =   CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        commonView.backgroundColor  =   UIColor.black
        self.view.addSubview(commonView)
        self.view.addSubview(spinner)
        commonView.isUserInteractionEnabled     =   false
        commonView.alpha                        =   0;
    }
    
//    func start_Loader() {
//        commonView.alpha    =   0.5
//        UIApplication.shared.beginIgnoringInteractionEvents()
//        spinner.startAnimating()
//    }
    
    func showHUD(forView view: UIView!, excludeViews: [UIView?]) {
        DispatchQueue.main.async {
            let hudView = MBProgressHUD.showAdded(to: view, animated: true)
            view.bringSubviewToFront(hudView)
            if !excludeViews.isEmpty {
                for subView in excludeViews {
                    view.bringSubviewToFront(subView!)
                }
            }
        }
    }
    
    func hideHUD(forView view: UIView!) {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: view, animated: true);
        }
    }

    func getElapsedTimefromDate(startTime: String?, endTime: String?) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"

        if let endDateString = endTime {
            let endDateindex = endDateString.index(endDateString.startIndex, offsetBy: 19)
            let endFormattedDateString = String(endDateString[..<endDateindex])
            if let endDate = dateFormatter.date(from: endFormattedDateString) {
                let endTimeInterval: Double = endDate.timeIntervalSinceNow
                if endTimeInterval < 0 {
                    return "Event Completed"
                }
            }
        }
        
        guard let dateString = startTime else {
            return "Not available"
        }
        let index = dateString.index(dateString.startIndex, offsetBy: 19)
        let formattedDateString = String(dateString[..<index])
        
        guard let createdDate = dateFormatter.date(from: formattedDateString) else {
            return "Not available"
        }
        
        let timeInterval: Double = createdDate.timeIntervalSinceNow
        if timeInterval < 0 {
            return "Event Started"
        } else if timeInterval == 0 {
            return "Started now"
        } else {
            let days = (Int(timeInterval) / 86400)
            let hours = Int(timeInterval / 3600) - Int(days * 24)
            let minutes = Int(timeInterval / 60) - Int(days * 24 * 60 + hours * 60)
            let seconds = Int(timeInterval) - (Int(timeInterval / 60) * 60)
            return "Event starts in \n\(days) Days \(hours) Hrs \(minutes) Mins \(seconds) Secs"
        }
    }

//    func stop_Loader(){
//        UIApplication.shared.endIgnoringInteractionEvents()
//        commonView.alpha    =   0
//        spinner.stopAnimating()
//    }
    
    func setNavigationBar(){
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        self.sideMenu()
        self.navigationItem.setHidesBackButton(true, animated:true);
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor   =   UIColor.white
        }
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    
    func sideMenu(){
        let rightImage  =   UIImage(named:"sideBar.png")
        self.btnShowMen.setImage(rightImage, for: UIControl.State.normal)
        self.btnShowMen.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControl.Event.touchUpInside)
        let customBarIte = UIBarButtonItem(customView: btnShowMen)
        self.navigationItem.leftBarButtonItem = customBarIte;
        if UI_USER_INTERFACE_IDIOM() == .pad {
            self.btnShowMen.frame = CGRect(x:0,y: 0,width: 45,height: 40)
        }else{
            self.btnShowMen.frame = CGRect(x:0,y: 0,width: 30,height: 20)
        }
    }

    @objc func navigateBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Add_Button_In_Navigation_Item
    func rightBarButton(imageName : UIImage){
        let img         =   imageName
        let frameImg    =   CGRect.init(x: 0, y: 0, width:img.size.width, height:img.size.height)
        let someButton  =   UIButton.init(frame: frameImg)
        someButton.clipsToBounds    =   true
        someButton.setBackgroundImage(img, for: .normal)
        someButton.addTarget(self, action: #selector(BaseClassVC.rightButtonAction), for: .touchUpInside)
        let rightBtn    =   UIBarButtonItem.init(customView: someButton)
        self.navigationItem.rightBarButtonItem  =   rightBtn
    }
    
    @objc func rightButtonAction(){

    }
    
    //MARK:- Navigation_Bar_Color_Mthd
    func navigationBarColor() -> UIColor {
        if let color_Value  =  UserDefaults.standard.value(forKey: "color_Value") as? String {
            return UIColor.colorWithHexString(hex: color_Value)
        }else{
            return UIColor.init(red: 0.0/255.0, green: 195.0/255.0, blue: 231/255.0, alpha: 1)
        }
    }
    
    func tostMessage(msg : String){
        self.view.makeToast(msg, duration: 2.0, position: CSToastPositionCenter)
    }
    
    func matchData(tableName: String, tableId : String ) -> [NSManagedObject]{
        var newArr                  =   [NSManagedObject]()
        if #available(iOS 10.0, *) {
            let appDelegate         =   (UIApplication.shared.delegate) as! AppDelegate
            let context             =   appDelegate.persistentContainer.viewContext
            let request             =   NSFetchRequest<NSFetchRequestResult>(entityName:tableName)
            let checkPredicate      =   NSPredicate(format: "product_id == %@", tableId)
            request.predicate       =   checkPredicate;
            newArr                  =   try! context.fetch(request) as! [NSManagedObject]
            return newArr
        } else {
            return newArr
        }
    }
    func fetchData(tableName: String, tableId : String, columName : String) -> [NSManagedObject]{
        var newArr                  =   [NSManagedObject]()
        if #available(iOS 10.0, *) {
            let appDelegate         =   (UIApplication.shared.delegate) as! AppDelegate
            let context             =   appDelegate.persistentContainer.viewContext
            let request             =   NSFetchRequest<NSFetchRequestResult>(entityName:tableName)
            let checkPredicate      =   NSPredicate(format: "%@ == %@", columName, tableId)
            request.predicate       =   checkPredicate;
            newArr                  =   try! context.fetch(request) as! [NSManagedObject]
            return newArr
        } else {
            return newArr
        }
    }
 
    func navigateToSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            } else {
                UIApplication.shared.openURL(settingsUrl)
            }
        }
    }

    
    static func handleNotificationWhenLive(userInfo: [AnyHashable : Any]) {
        let loginData = MyDatabase.getProgramData(tableName: "LoginData") as? [LoginData]
        
        if let users = loginData, users.count > 0 {
            if let aps = userInfo["aps"] as? NSDictionary,
                let alert = aps["alert"] as? NSDictionary,
                let page = alert["page"] as? String {
                switch page {
                case "Event":
                    if let eventId = alert["id"] as? String {
                        BaseClassVC.navigateToEventDashboardWithId(eventId: eventId)
                    }
                    break
                case "Chat":
                    if let eventId = alert["event_id"] as? String,
                        let conversationId = alert["id"] as? String {
                        BaseClassVC.navigateToChatScreenWithConversationId(conversationId, eventId: eventId, userData: users[0])
                    }
                case "EventAgenda":
                    if let eventId = alert["event_id"] as? String,
                        let agendaId = alert["id"] as? String {
                        BaseClassVC.navigateToAgendaScreen(eventId, agendaId: agendaId)
                    }
                default:
                    break
                }
            }

        }
    }
    
    static func navigateToEventDashboardWithId(eventId: String) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)

        let viewController = storyBoard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        viewController.eventId  = eventId
        let eventsData = MyDatabase.matchProgramData(tableName: "EventData", tableId: eventId, columnName: "event_id") as? [EventData]
        if let eventsData = eventsData, eventsData.count > 0 {
            menuViewController.navigateToViewController(vc: viewController)
        }
    }
    
    static func navigateToChatScreenWithConversationId(_ conversationId: String, eventId: String, userData: LoginData) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        viewController.eventId  = eventId
        viewController.userData = userData

        viewController.conversationId = conversationId
        viewController.isFromNotification = true
        let eventsData = MyDatabase.matchProgramData(tableName: "EventData", tableId: eventId, columnName: "event_id") as? [EventData]
        if let eventsData = eventsData, eventsData.count > 0 {
            viewController.eventData = eventsData[0]
            menuViewController.navigateToViewController(vc: viewController)
        }
    }

    static func navigateToAgendaScreen(_ eventId: String, agendaId: String) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AgendaSubDetailVc") as! AgendaSubDetailVc
        viewController.eventId  = eventId
        
        viewController.agendaId = agendaId
        let eventsData = MyDatabase.matchProgramData(tableName: "EventData", tableId: eventId, columnName: "event_id") as? [EventData]
        if let eventsData = eventsData, eventsData.count > 0 {
            menuViewController.navigateToViewController(vc: viewController)
        }
    }
}
