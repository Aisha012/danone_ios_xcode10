//
//  MainContactTableViewCell.swift
//  Danone
//
//  Created by Renendra Saxena on 07/01/19.
//  Copyright © 2019 Zaman Meraj. All rights reserved.
//

class  MainContactTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var jobTitleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var phoneCallBtn: UIButton!

    func configureCell(contact: ContactsList) {
        let avatarURL = contact.avatar ?? ""
        profileImageView.sd_setShowActivityIndicatorView(true)
        profileImageView.sd_setIndicatorStyle(.gray)
        profileImageView.layer.borderColor = UIColor.black.cgColor
        profileImageView.layer.borderWidth = 1.0
        profileImageView.sd_setImage(with: URL(string: avatarURL), placeholderImage: UIImage(named: "profile.png"), options: .progressiveDownload, completed: nil)
        
        
        jobTitleLbl.isHidden = contact.job_title.isEmpty
        descLbl.isHidden = contact.description .isEmpty
        phoneLbl.isHidden = contact.phone_number.isEmpty
        emailLbl.isHidden = contact.email_address.isEmpty
        
        nameLabel.text = contact.name
        jobTitleLbl.text = contact.job_title ?? ""
        descLbl.text = (contact.description).trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        phoneLbl.text = contact.phone_number ?? ""
        emailLbl.text =  contact.email_address ?? ""
        
        phoneCallBtn.addTarget(self, action: #selector(PhoneCallBtnTapped(_:)), for: .touchUpInside)
    }
    @objc func PhoneCallBtnTapped(_ sender : UIButton){
        print("hey")
        let phoneNumber =   (phoneLbl.text)?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")

        if let url = URL(string: "tel://\(phoneNumber!)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        
    }
    }
}
