//
//  CategoryViewController.swift
//  ToolApp
//
//  Created by Phaninder on 06/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class CategoryViewController: BaseClassVC, IndicatorInfoProvider {

    @IBOutlet weak var tableView: UITableView!
    
    var completeModal = [CompleteAgendaModal]()
    var eventId: String!
    var categories = [CategoryModal]()

    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        for agendas in completeModal {
            for agenda in agendas.list {
                if let category = agenda.category {
                    let containsAlready = categories.contains(where: { (categoryModal) -> Bool in
                        return category.name == categoryModal.name
                    })
                    if !containsAlready {
                        categories.append(category)
                    }
                }
            }
        }
        
        emptyPlaceholderLabel.isHidden = !categories.isEmpty

    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Category".localized)
    }

}

extension CategoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckableTableViewCell",
                                                 for: indexPath) as! CheckableTableViewCell
        cell.textLabel?.text = categories[indexPath.row].name ?? "No Text".localized
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell =  tableView.cellForRow(at: indexPath) as! CheckableTableViewCell
        var category = ""
        if cell.isSelected {
            tableView.deselectRow(at: indexPath, animated: true)
            let userInfo = ["data": category];
            NotificationCenter.default.post(name: .categorySelected, object: nil, userInfo:userInfo)

            return nil
        } else {
            category = categories[indexPath.row].category_id!
            let userInfo = ["data": category];
            NotificationCenter.default.post(name: .categorySelected, object: nil, userInfo:userInfo)
            return indexPath
        }

    }

}
