//
//  PollParentViewController.swift
//  ToolApp
//
//  Created by Phaninder on 17/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import StepProgressBar

class PollParentViewController: BaseClassVC {

    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var questionProgressIndicatorLabel: UILabel!
    @IBOutlet weak var questionProgressBar: StepProgressBar!
    @IBOutlet weak var pollContainerView: UIView!
    
    //    @IBOutlet weak var bottomView: UIView!

    var pagerViewController: PollCustomPagerViewController!
    var showMenuButton = false
    var eventId: String!
    var polls = [Poll]()
    var agendaName = ""
    var questionNumber = 1
    var agendaId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        titleLabel.text = agendaName
        showProgress()
        questionProgressBar.stepsCount = polls.count
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(questionChanged(_:)),
                                               name: .questionChanged,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(questionSubmitted(_:)),
                                               name: .questionSubmitted,
                                               object: nil)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PollNav" {
            let destination = segue.destination as! PollCustomPagerViewController
            destination.polls = polls
        }
    }
    
    func showProgress() {
        
        questionProgressIndicatorLabel.text = "Question ".localized + "\(questionNumber + 1)" + " of ".localized + "\(polls.count)"
        questionProgressBar.progress = questionNumber + 1
    }
    
    @objc func questionChanged(_ notification: NSNotification) {
        if let questionNumber = notification.userInfo?["data"] as? Int {
            self.questionNumber = questionNumber
        }
        showProgress()
    }
    
    @objc func questionSubmitted(_ notification: NSNotification) {
        if let questionId = notification.userInfo?["questionId"] as? String, let answerId = notification.userInfo?["answerId"] as? String {
            submitPoll(questionId: questionId, optionId: String(answerId))
        }
    }


    func submitPoll(questionId: String, optionId: String) {
        
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            let params = ["user_token": auth_Token,
                          "api_token": apiToken,
                          "question_id": questionId,
                          "selected_option_id": optionId]
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/\(agendaId!)/survey_questions", parameter: params, type: .post) { (netResponse) -> (Void) in
                
                self.hideHUD(forView: self.view)
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    if let _ = netResponse.responseDict as? NSDictionary {
                        self.tostMessage(msg: "Submitted".localized)
                    }
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }
        }
        
    }
    
}
