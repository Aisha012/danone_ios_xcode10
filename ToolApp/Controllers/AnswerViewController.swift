//
//  AnswerViewController.swift
//  ToolApp
//
//  Created by Phaninder on 18/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class AnswerViewController: BaseClassVC {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var answerTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    var question: Question!
    var answers = [Answer]()
    var eventId: String!
    var agendaId: String!
    var questionId: Int!
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
        menuButton.setImage(nil, for: .normal)
        backImage.image = UIImage(named: "back.png")
        tableView.tableFooterView = UIView()
        titleLabel.text = "Answers".localized
        tableView.estimatedRowHeight = 86.0
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)

        fetchAnswers()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func refreshData(_ sender: Any) {
        question = nil
        answers = [Answer]()
        fetchAnswers()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }


    @IBAction func sendButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let answer = answerTextField.text else {
            return
        }
        
        let trimmedAnswerString = answer.components(separatedBy: .whitespacesAndNewlines).joined(separator: "")

        guard !trimmedAnswerString.isEmpty else {
            return
        }
        answerTextField.endEditing(true)
        answerTextField.text = ""
        postAnswer(answer)
    }
    
}


extension AnswerViewController {

    func fetchAnswers() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            let urlString = "http://eu.aio-events.com/api/events/\(eventId!)/agendas/\(agendaId!)/questions/\(questionId!)?user_token=\(auth_Token)&api_token=\(apiToken)"
            _ = CallApi.getData(url: urlString, parameter: nil, type: .get) { (netResponse) -> (Void)
                in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSON = netResponse.responseDict as? NSDictionary {
                        let questionDict = JSON.value(forKey: "question") as! NSDictionary
                        self.question = Question(responseDict: questionDict)
                        
                        let answersArray = JSON.value(forKey: "answers") as! [NSDictionary]
                        for answerDict in answersArray {
                            let answer = Answer(responseDict: answerDict)
                            self.answers.append(answer)
                        }
                        self.tableView.reloadData()
                    } else {
                        self.tableView.reloadData()
                    }
                    self.hideHUD(forView: self.view)
                } else {
                    self.hideHUD(forView: self.view)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func postAnswer(_ answer: String) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            let param: NSDictionary = ["answer" : answer,
                                       "user_token" : auth_Token,
                                       "api_token": apiToken]
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/\(agendaId!)/questions/\(questionId!)/answers", parameter: param as? Dictionary<String, Any>, type: .post) { (netResponse) -> (Void) in
                
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSON = netResponse.responseDict as? NSDictionary {
                        let answer = Answer(responseDict: JSON)
                        self.answers.append(answer)
                        self.tableView.reloadData()
                    }
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }
        }
    }

    
}

extension AnswerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let _ = question else {
            return 0
        }
        return answers.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionListCell", for: indexPath) as! QuestionListCell
            cell.configureCell(question: question, atIndex: 0)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: AnswerTableViewCell.cellIdentifier(), for: indexPath) as! AnswerTableViewCell
            cell.configureCell(answer: answers[indexPath.row - 1])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
