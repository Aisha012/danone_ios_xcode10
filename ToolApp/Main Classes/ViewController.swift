//
//  ViewController.swift
//  ToolApp
//
//  Created by Zaman Meraj on 26/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import JSSAlertView

class ViewController: BaseClassVC, UITextFieldDelegate {

    var emailSel = 0
    var passSel  = 0

    var eventOptions = [EventOption]()
    var shouldShowEventOptions = false
    var APIToken = "113cf28b-728f-4696-8aa7-8f4365f6c475"
    var isNormalLogin = true
    var linkedInUser: LinkedInUser!
    var checkBtnEmail: UIButton!
    var checkBtnPaasword: UIButton!
    var emailTF: UITextField!
    var passwordTF: UITextField!
    
//    let tapGesture: UITapGestureRecognizer!
    
    @IBOutlet weak var optionTableView: UITableView!
    @IBOutlet weak var optionHolderView: UIView!
    @IBOutlet weak var optionHolderViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var optionHolderViewVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var loginTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        optionTableView.dataSource = self
        optionTableView.delegate = self
        loginTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        self.navigationController?.navigationBar.isHidden = true
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
//        self.view.addGestureRecognizer(tapGesture)
        self.optionHolderViewVerticalConstraint.constant = ScreenHeight + 250

//        self.emailTF.text    = "shiwei.ng@imavox.global"
//        self.passwordTF.text = "planitswiss"
//        self.emailTF.text    = "shashank@hipster-inc.com"
//        self.passwordTF.text = "123456789"

//        self.emailTF.text    = "renendra@hipster-inc.com"
//        self.passwordTF.text = "12345678"
    }
    
   @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.setTextField()
        self.setButtonProperty()
    }
    
    
    func setTextField() {
    }
    
    func setButtonProperty() {
//        self.loginBtn.addTarget(self, action: #selector(self.loginBtnTapped), for: .touchUpInside)
    }
    
    
    func linkedinButtonTapped() {
    
        if let appUrl = URL(string: "linkedin://app"),
            UIApplication.shared.canOpenURL(appUrl) {
            navigateToLinkedInApp()
        } else {
            let webViewCnt = self.storyboard!.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webViewCnt.linkedInDelegate = self
            self.navigationController?.pushViewController(webViewCnt, animated: true)
        }
    }
    
    func navigateToLinkedInApp() {
        LISDKSessionManager.clearSession()
        if LISDKSessionManager.hasValidSession() {
            handleLinkedInLogin()
        } else {
            let permissions = [LISDK_BASIC_PROFILE_PERMISSION, LISDK_EMAILADDRESS_PERMISSION]
            
            LISDKSessionManager.createSession(withAuth: permissions,
                                              state: "RandomState",
                                              showGoToAppStoreDialog: true,
                                              successBlock: { (success) in
                                                self.handleLinkedInLogin()
            }) { (error) in
                print(error.debugDescription)
            }
        }
    }
    
    func handleLinkedInLogin() {
        
        if LISDKSessionManager.hasValidSession() {
            self.showHUD(forView: self.view, excludeViews: [])

            LISDKAPIHelper.sharedInstance().apiRequest("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,email-address)?format=json",
                                                       method: "GET",
                                                       body: nil, success: { (response) in
                                                        self.hideHUD(forView: self.view)
                                                        if response?.statusCode == 200 {
                                                            let dataString = response!.data
                                                            if let data = dataString?.data(using: String.Encoding.utf8) {
                                                                do {
                                                                    let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                                                                    if let myDictionary = dictionary {
                                                                    let linkedInUser = LinkedInUser(responseDict: myDictionary)
                                                                        self.AIOlinkedInLogin(linkedInUser: linkedInUser)
                                                                    }
                                                                } catch let error as NSError {
                                                                    print(error)
                                                                }
                                                            }
                                                        } else {
                                                            print("Error")
                                                        }
            }, error: { (error) in
                self.hideHUD(forView: self.view)
            })
        }
    }
    
    
    func linkedInLogin() {
        guard let linkedInUser = self.linkedInUser else {
            return
        }
        let param: NSDictionary = ["email" : linkedInUser.email,
                                   "linkedin_id" : linkedInUser.id,
                                   "first_name": linkedInUser.firstName,
                                   "last_name": linkedInUser.lastName,
                                   "api_token": APIToken,
                                   "password": Utilities.randomString(length: 12)]
        
        self.showHUD(forView: self.view, excludeViews: [])
        
        _ = CallApi.getData(url: "http://eu.aio-events.com/api/users", parameter: param as? Dictionary<String, Any>, type: .post) { (netResponse) -> (Void) in
            self.hideHUD(forView: self.view)
            
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
//                UserDefaults.standard.set("113cf28b-728f-4696-8aa7-8f4365f6c475", forKey: "API_TOKEN")
//                UserDefaults.standard.synchronize()

                self.hideHUD(forView: self.view)
                if let JSON = netResponse.responseDict as? NSDictionary {
                    self.handleLoginData(JSON: JSON)
                } else {
                    var message = "Something didn't go as expected"
                    if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                        message = errorMessage
                    }
                    DispatchQueue.main.async {
                        JSSAlertView().danger(
                            self,
                            title: "Error!".localized,
                            text: message,
                            buttonText: "Ok".localized)
                    }
                }
            } else {
                var message = "Something didn't go as expected"
                if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                    message = errorMessage
                }
                DispatchQueue.main.async {
                    JSSAlertView().danger(
                        self,
                        title: "Error!".localized,
                        text: message,
                        buttonText: "Ok".localized)
                }
                
                self.hideHUD(forView: self.view)
            }
        }
    }
    
    func AIOlinkedInLogin(linkedInUser: LinkedInUser) {
        self.linkedInUser = linkedInUser
        let param: NSDictionary = ["email" : linkedInUser.email,
                                   "linkedin_id" : linkedInUser.id,
                                   "first_name": linkedInUser.firstName,
                                   "last_name": linkedInUser.lastName,
                                   "password": Utilities.randomString(length: 12)]
        
        self.showHUD(forView: self.view, excludeViews: [])
        
        _ = CallApi.getData(url: "http://eu.aio-events.com/api/aio", parameter: param as? Dictionary<String, Any>, type: .post) { (netResponse) -> (Void) in
            self.hideHUD(forView: self.view)
            
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                self.hideHUD(forView: self.view)
                if let JSON = netResponse.responseDict as? NSDictionary {
                    UserDefaults.standard.set("113cf28b-728f-4696-8aa7-8f4365f6c475", forKey: "API_TOKEN")
                    UserDefaults.standard.synchronize()

                    self.handleLoginData(JSON: JSON)
                } else {
                    var message = "Something didn't go as expected"
                    if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                        message = errorMessage
                    }
                    DispatchQueue.main.async {
                        JSSAlertView().danger(
                            self,
                            title: "Error!",
                            text: message,
                            buttonText: "OK")
                    }
                }
            } else {
                self.hideHUD(forView: self.view)
                if netResponse.statusCode == 300, let JSON = netResponse.responseDict as? [NSDictionary], JSON.count > 0 {
                    self.isNormalLogin = false
                    self.eventOptions.removeAll()
                    for eventOpt in JSON {
                        let eventOption = EventOption(responseDict: eventOpt)
                        self.eventOptions.append(eventOption)
                    }
                    DispatchQueue.main.async {
                        self.showBlurView()
                    }
                } else {
                    var message = "Something didn't go as expected"
                    if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                        message = errorMessage
                    }
                    DispatchQueue.main.async {
                        JSSAlertView().danger(
                            self,
                            title: "Error!",
                            text: message,
                            buttonText: "OK")
                    }
                    
                }
            }
        }
    }

    
    func loginBtnTapped() {
        if self.emailTF.text == ""{
            self.view.makeToast("Email field can't be blank".localized, duration: 2, position: CSToastPositionCenter)
        }else if self.passwordTF.text == ""{
            self.view.makeToast("Password field can't be blank".localized, duration: 2, position: CSToastPositionCenter)
        }else{
            self.AIOLogin()
        }
    }
    
    func normalLogin() {
        let param: NSDictionary = ["email" : self.emailTF.text ?? "",
                                   "password" : self.passwordTF.text ?? "",
                                   "api_token": APIToken]
        self.showHUD(forView: self.view, excludeViews: [])
        _ = CallApi.getData(url: "http://eu.aio-events.com/api/users/authenticate_user", parameter: param as? Dictionary<String, Any>, type: .post) { (netResponse) -> (Void) in
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                self.hideHUD(forView: self.view)
                if let JSON = netResponse.responseDict as? NSDictionary {
                    self.handleLoginData(JSON: JSON)
                } else {
                    var message = "Something didn't go as expected".localized
                    if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                        message = errorMessage
                    }
                    DispatchQueue.main.async {
                        JSSAlertView().danger(
                            self,
                            title: "Error!".localized,
                            text: message,
                            buttonText: "Ok".localized)
                    }
                }
            }else {
                var message = "Something didn't go as expected".localized
                if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                    message = errorMessage
                }
                DispatchQueue.main.async {
                    JSSAlertView().danger(
                        self,
                        title: "Error!".localized,
                        text: message,
                        buttonText: "Ok".localized)
                }

                self.hideHUD(forView: self.view)
            }

        }
        
    }
    
    func AIOLogin() {
        let param: NSDictionary = ["email" : self.emailTF.text ?? "",
                                   "password" : self.passwordTF.text ?? ""]
        self.showHUD(forView: self.view, excludeViews: [])
        
        
        _ = CallApi.getData(url: "http://eu.aio-events.com/api/aio/authenticate_user", parameter: param as? Dictionary<String, Any>, type: .post) { (netResponse) -> (Void) in
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                self.hideHUD(forView: self.view)
                if let JSON = netResponse.responseDict as? NSDictionary {
                    self.handleLoginData(JSON: JSON)
                    UserDefaults.standard.set("113cf28b-728f-4696-8aa7-8f4365f6c475", forKey: "API_TOKEN")
                    UserDefaults.standard.synchronize()
                } else {
                    var message = "Something didn't go as expected"
                    if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                        message = errorMessage
                    }
                    DispatchQueue.main.async {
                        JSSAlertView().danger(
                            self,
                            title: "Error!",
                            text: message,
                            buttonText: "OK")
                    }
                }
            }else {
                self.hideHUD(forView: self.view)

                if netResponse.statusCode == 300, let JSON = netResponse.responseDict as? [NSDictionary], JSON.count > 0 {
                    self.isNormalLogin = true

                    self.eventOptions.removeAll()
                    for eventOpt in JSON {
                        let eventOption = EventOption(responseDict: eventOpt)
                        self.eventOptions.append(eventOption)
                    }
                    DispatchQueue.main.async {
                        self.showBlurView()
                    }
                } else {
                    var message = "Something didn't go as expected"
                    if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                        message = errorMessage
                    }
                    DispatchQueue.main.async {
                        JSSAlertView().danger(
                            self,
                            title: "Error!",
                            text: message,
                            buttonText: "OK")
                    }
                    
                }
            }
            
        }
        
    }

    
    func handleLoginData(JSON: NSDictionary) {
        MyDatabase.clearAllData()
        let loginModal = LoginModal(responseDict: JSON)
        MyDatabase.saveLoginDataLocally(loginModal: loginModal)
        
        guard let events = JSON.value(forKey: "event") as? [NSDictionary] else { return }
        
        if events.count > 0 {
            if self.checkBtnEmail.currentImage == UIImage(named: "checkbox_checked_brand") {
                UserDefaults.standard.set(self.emailTF.text, forKey: "email")
                UserDefaults.standard.set(1, forKey: "rem_email")
                
            }else{
                UserDefaults.standard.set("", forKey: "email")
                UserDefaults.standard.set(0, forKey: "rem_email")
            }
            if self.checkBtnPaasword.currentImage == UIImage(named: "checkbox_checked_brand") {
                UserDefaults.standard.set(self.passwordTF.text, forKey: "pass")
                UserDefaults.standard.set(1, forKey: "rem_pass")
            }else{
                UserDefaults.standard.set("", forKey: "pass")
                UserDefaults.standard.set(0, forKey: "rem_pass")
            }
            UserDefaults.standard.set("userRegister", forKey: "Registered")
            
            if events.count == 1 {
                
                let event = events[0]
                
                let eventModal = EventModal(responseDict: event)
                MyDatabase.saveEventLocally(eventModal: eventModal)
                if let banners = event.value(forKey: "banners") as? [NSDictionary] {
                    
                    for banner in banners {
                        let bannerModal = BannersModal(responseDict: banner, eventId: eventModal.eventId!)
                        MyDatabase.saveBannerLocally(bannerModal: bannerModal)
                        
                    }
                }
                
                if let pages = event.value(forKey: "pages") as? [NSDictionary] {
                    
                    for page in pages {
                        let pageModal = PageModal(responseDict: page, eventId: eventModal.eventId!)
                        MyDatabase.savePageLocally(pageModal: pageModal)
                        
                    }
                }
                
                
                self.navigateToEventsPage()
            } else {
                for event in events {
                    let eventModal = EventModal(responseDict: event)
                    MyDatabase.saveEventLocally(eventModal: eventModal)
                    if let banners = event.value(forKey: "banners") as? [NSDictionary] {
                        for banner in banners {
                            let bannerModal = BannersModal(responseDict: banner, eventId: eventModal.eventId!)
                            MyDatabase.saveBannerLocally(bannerModal: bannerModal)
                        }
                        if let pages = event.value(forKey: "pages") as? [NSDictionary] {
                            
                            for page in pages {
                                let pageModal = PageModal(responseDict: page, eventId: eventModal.eventId!)
                                MyDatabase.savePageLocally(pageModal: pageModal)
                                
                            }
                        }
                    }
                }
                self.navigateToEventsPage()
                
            }
        }
    }
    
    func navigateToEventsPage() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let frontviewcontroller = storyboard.instantiateViewController(withIdentifier: "EventListVc") as! EventListVc
        let objNavigationController : UINavigationController    =   UINavigationController.init(rootViewController: frontviewcontroller)
        let rearViewController = storyboard.instantiateViewController(withIdentifier: "menuViewController") as? menuViewController
        let mainRevealController = SWRevealViewController()
        mainRevealController.frontViewController = objNavigationController
        mainRevealController.rearViewController = rearViewController
        (UIApplication.shared.delegate as! AppDelegate).window!.rootViewController = mainRevealController
        (UIApplication.shared.delegate as! AppDelegate).window?.makeKeyAndVisible()
    }
    
    func heightForOptionsView() -> CGFloat {
        return CGFloat(41.0 + (75.0 * Double(eventOptions.count)))
    }
    
    @IBAction func rem_EmailBtn(_ sender: Any) {
    }
    @IBAction func rem_PassBtn(_ sender: Any) {
    }
    
    func forgotBtn() {
        let vc : ForgotVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotVC") as! ForgotVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func rememberPasswordCheckBtnTapped(sender: UIButton) {
    
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func hideBlurView() {

        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.9, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.optionHolderViewVerticalConstraint.constant = ScreenHeight + (self.optionHolderViewHeightConstraint.constant / 2)
            self.blurView.alpha = 0
            
            self.view.layoutIfNeeded()
        }) { (completed) in
            if completed {
                self.blurView.isHidden = true
                self.blurView.alpha = 1
                self.optionHolderView.isHidden = true
            }
        }
    }
    
    func showBlurView() {
        optionHolderViewHeightConstraint.constant = heightForOptionsView()
        optionTableView.isUserInteractionEnabled = true
        
        optionTableView.reloadData()
        view.layoutIfNeeded()
        optionHolderViewVerticalConstraint.constant = -(ScreenHeight / 2) - (optionHolderViewHeightConstraint.constant / 2)
        self.optionHolderView.isHidden = false
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.optionHolderViewVerticalConstraint.constant = 0
            self.blurView.isHidden = false
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }

    
    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == loginTableView {
            return 1
        }
        return eventOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == loginTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoginTableViewCell.cellIdentifier(), for: indexPath) as! LoginTableViewCell
            self.checkBtnEmail = cell.checkBtnEmail
            self.checkBtnPaasword = cell.checkBtnPaasword
            self.emailTF = cell.emailTF
            self.passwordTF = cell.passwordTF
//                    self.emailTF.text    = "shiwei.ng@imavox.global"
//                    self.passwordTF.text = "planitswiss"

            cell.delegate = self
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: EventOptionTableViewCell.cellIdentifier(), for: indexPath) as! EventOptionTableViewCell
        cell.nameLabel.text = eventOptions[indexPath.row].name
        cell.eventImageView.sd_setShowActivityIndicatorView(true)
        cell.eventImageView.sd_setIndicatorStyle(.gray)
        
        cell.eventImageView.sd_setImage(with: URL(string: eventOptions[indexPath.row].logo), placeholderImage: UIImage(named: "gallery_placeholder"), options: .progressiveDownload, completed: nil)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView != loginTableView {
            APIToken = self.eventOptions[indexPath.row].apiToken
            UserDefaults.standard.set(APIToken, forKey: "API_TOKEN")
            UserDefaults.standard.synchronize()
            hideBlurView()
            if isNormalLogin {
                normalLogin()
            } else {
                linkedInLogin()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == loginTableView {
            return 570
        }
        return 75.0
    }
    
}


extension UIColor {
    
    static func colorWithHexString(hex: String) -> UIColor {
        var hexString = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if hexString.hasPrefix("#") {
            hexString = hexString.substring(from: String.Index.init(encodedOffset: 1))
        }
        
        if hexString.characters.count != 6 {
            return UIColor.gray
        }
        
        let rString = hexString.substring(to: String.Index.init(encodedOffset: 2))
        let gString = hexString.substring(from: String.Index.init(encodedOffset: 2)).substring(to: String.Index.init(encodedOffset: 2))
        let bString = hexString.substring(from: String.Index.init(encodedOffset: 4)).substring(to: String.Index.init(encodedOffset: 2))
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        
        
        Scanner.init(string: rString).scanHexInt32(&r)
        Scanner.init(string: gString).scanHexInt32(&g)
        Scanner.init(string: bString).scanHexInt32(&b)
//        if (countElements(hexString) != 6) {
//            return UIColor.gray
//        }
        
        return UIColor(red: CGFloat(Float(r) / 255.0), green: CGFloat(Float(g) / 255.0), blue: CGFloat(Float(b) / 255.0), alpha: CGFloat(Float(1)))
        
    }
    
    
}

extension ViewController: LoginTableViewCellDelegate {
    
    func loginButtonTapped() {
        self.loginBtnTapped()
    }
    
    func forgotPasswordButtonTapped() {
        self.forgotBtn()
    }
    
    func linkedInButtonTapped() {
        self.linkedinButtonTapped()
    }
}

extension ViewController: LinkedInLoginDelegate {
    
    func successfullyFetchedAccessToken(_ token: String) {

        let targetURLString = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,email-address)?format=json"
        
        // Initialize a mutable URL request object.
        var request = URLRequest.init(url: URL.init(string: targetURLString)!)
        
        // Indicate that this is a GET request.
        request.httpMethod = "GET"
        
        // Add the access token as an HTTP header field.
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        // Initialize a NSURLSession object.
        let session = URLSession(configuration: .default)
        
        // Make the request.
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            let statusCode = (response as! HTTPURLResponse).statusCode
            if statusCode == 200 {
                // Convert the received JSON data into a dictionary.
                do {
                    let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                    if let myDictionary = dataDictionary  as? [String: AnyObject] {
                        let linkedInUser = LinkedInUser(responseDict: myDictionary)
                        self.AIOlinkedInLogin(linkedInUser: linkedInUser)
                    }
                }
                catch {
                    print("Could not convert JSON data into a dictionary.")
                }
            } else {
                
            }
        }
        
        task.resume()
    }
}




