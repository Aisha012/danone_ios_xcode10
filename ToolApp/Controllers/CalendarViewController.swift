//
//  CalendarViewController.swift
//  ToolApp
//
//  Created by Phaninder on 26/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import CalendarKit
import DateToolsSwift
import JSSAlertView

class CalendarViewController: DayViewController {

    @IBOutlet weak var filterView: UIView!
    
    var completeModal = [CompleteAgendaModal]()
    var currentModal = [CompleteAgendaModal]()

    var eventId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        currentModal = completeModal
        
        self.title = "Agenda"
        let x = ScreenWidth - 30 - 16
        let y = ScreenHeight - 30 - 80
        let button = UIButton(frame: CGRect.init(x: x, y: y, width: 30, height: 30))
        button.setImage(UIImage(named: "funnel"), for: .normal)
        button.addTarget(self, action: #selector(navigateToFilterScreen), for: .touchUpInside)
        self.view.addSubview(button)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        configureNavBar()
    }

    @objc func navigateToFilterScreen() {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        destination.completeModal = self.completeModal
        destination.eventId = self.eventId
        destination.filterDelegate = self
        self.navigationController?.pushViewController(destination, animated: true)
    }

    func configureNavBar() {
        let color: UIColor!
        if let color_Value  =  UserDefaults.standard.value(forKey: "color_Value") as? String {
            color = UIColor.colorWithHexString(hex: color_Value)
        }else{
            color =  UIColor.init(red: 0.0/255.0, green: 195.0/255.0, blue: 231/255.0, alpha: 1)
        }
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = color
        self.navigationController?.navigationBar.barTintColor = color
        self.navigationController?.navigationBar.isHidden = false
        var image = UIImage(named: "back.png")
        image = image?.withRenderingMode(.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(navigateBack))
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }

    @objc func navigateBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func eventsForDate(_ date: Date) -> [EventDescriptor] {
        var events = [Event]()

        for (sectionIndex, agendas) in currentModal.enumerated() {
            for (index, agenda) in agendas.list.enumerated() {
                let event = Event()
                
                let startDate = Utilities.getDateFromString(agenda.start_date)
                let endDate = Utilities.getDateFromString(agenda.end_date)
                // Specify StartDate and EndDate
                event.startDate = startDate!
                event.endDate = endDate!

                event.userInfo = ["index": index, "section": sectionIndex]
                var info = [agenda.title!]

                
                event.text = info.reduce("", {$0 + $1 + "\n"})
                events.append(event)
            }
        }
        return events
    }
    
    
    override func dayViewDidSelectEventView(_ eventView: EventView) {
        guard let descriptor = eventView.descriptor as? Event else {
            return
        }
        if let userInfo = descriptor.userInfo as? [String: Int],
            let index = userInfo["index"],
            let sectionIndex = userInfo["section"] {
            let alert = JSSAlertView().info(self,
                                            title: AppName,
                                            text: descriptor.text,
                                            buttonText: "View".localized,
                                            cancelButtonText: "Close".localized)
            alert.addAction {
                let destination = self.storyboard?.instantiateViewController(withIdentifier: "AgendaSubDetailVc") as! AgendaSubDetailVc
                destination.lisModal      = self.currentModal[sectionIndex].list[index]
                destination.eventId       = self.eventId
                self.navigationController?.pushViewController(destination, animated: true)
            }
        }
    }
    
}

extension CalendarViewController: FilterViewControllerDelegate {
    
    func filterApplied(_ category: String, room: String, date: String) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            let hudView = MBProgressHUD.showAdded(to: view, animated: true)
            
            var url = "http://eu.aio-events.com/api/events/\(self.eventId!)/agenda_list?user_token=\(auth_Token)&api_token=\(apiToken)"
            if !category.isEmpty {
                url += "&category=\(category)"
            }
            
            if !room.isEmpty {
                url += "&room=\(room)"
            }
            
            if !date.isEmpty {
                url += "&date=\(date)"
            }
            
            _ = CallApi.getData(url: url, parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    DispatchQueue.main.async {
                        hudView.hide(animated: true)
                    }
                    
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        self.currentModal.removeAll()
                        for jsonValue in JSON {
                            let completeModal = CompleteAgendaModal(responseDic: jsonValue, eventId: self.eventId!)
                            self.currentModal.append(completeModal)
                            DispatchQueue.main.async {
                                self.reloadData()
                            }
                        }
                        if self.currentModal.count <= 0{
                            self.view.makeToast("There is no agenda listing".localized)
                        }else{

                        }
                        
                    }
                }
            }
        }

    }
    
}
