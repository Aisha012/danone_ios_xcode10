//
//  QRScannerViewController.swift
//  GetSip
//
//  Created by Phaninder on 13/12/17.
//  Copyright © 2017 Phaninder. All rights reserved.
//

import UIKit
import AVFoundation
import JSSAlertView

//MARK: - QRScannerViewController Class -

class QRScannerViewController: BaseClassVC {
    
    //MARK: - IBOutlets -
    
    @IBOutlet var cameraView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet var flashButton: UIButton!

    //MARK: - Variables and Constants -

    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var scannedData: String!
    let screenHeight = UIScreen.main.bounds.height
    let supportedBarCodes = [AVMetadataObject.ObjectType.qr, AVMetadataObject.ObjectType.code128, AVMetadataObject.ObjectType.code39, AVMetadataObject.ObjectType.code93, AVMetadataObject.ObjectType.upce, AVMetadataObject.ObjectType.pdf417, AVMetadataObject.ObjectType.ean13, AVMetadataObject.ObjectType.aztec]
    var isFlashEnable = false
    var eventId: String!
    var accessPointId: String!
    
    //MARK: - View Life Cycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
        flashButton.setImage(UIImage(named: isFlashEnable ? "flash_on" : "flash_off"), for: .normal)
        configureVideoCapture()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)        
        self.view.backgroundColor = UIColor.lightGray
        if UIImagePickerController.isCameraDeviceAvailable(.rear) {
            if (captureSession?.isRunning == false) {
                captureSession.startRunning()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isFlashEnable == true {
            toggleFlash()
        }

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: - IBActions -
    
    @IBAction func flashButtonTapped(_ sender: UIButton) {
        toggleFlash()
    }
    
    
    //MARK: - Helper Methods -
    
    func toggleFlash() {
        let avDevice = AVCaptureDevice.default(for: AVMediaType.video)!
        
        if avDevice.hasTorch {
            do {
                try avDevice.lockForConfiguration()
            } catch {
                print(error.localizedDescription)
            }
            
            if avDevice.isTorchActive || isFlashEnable {
                isFlashEnable = false
                avDevice.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try avDevice.setTorchModeOn(level: 1.0)
                    isFlashEnable = true
                } catch {
                    print(error.localizedDescription)
                }
            }
            flashButton.setImage(UIImage(named: isFlashEnable ? "flash_on" : "flash_off"), for: .normal)
            avDevice.unlockForConfiguration()
        } else {
            //Do something here
            self.view.makeToast("Your device doesn’t support flash.".localized, duration: 2.0, position: CSToastPositionCenter)
        }
    }

    func configureVideoCapture() {
        
        AVCaptureDevice .requestAccess(for: AVMediaType.video) { (granted) in
            DispatchQueue.main.async {
                if granted == true {
                    self.runCaptureSession()
                } else {
                    self.showNoCameraAccessMessage()
                }
            }
        }
    }
    
    func runCaptureSession() {
        captureSession = AVCaptureSession()

        let videoCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        flashButton.isHidden = !(videoCaptureDevice?.hasTorch)!
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice!)
        } catch {
            failed()
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        
        // Set delegate and use the default dispatch queue to execute the call back
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        // Detect all the supported bar code
        captureMetadataOutput.metadataObjectTypes = supportedBarCodes
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds;
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraView.layer.addSublayer(previewLayer)

        captureSession.startRunning()
    }
    
    func showNoCameraAccessMessage() {
        let alertview = JSSAlertView().show(
            self,
            title: "Camera Permission".localized,
            text: "Please allow Camera settings to use this feature.".localized,
            buttonText: "Ok".localized,
            cancelButtonText: "Later".localized,
            color: UIColor.white)
        alertview.addAction(navigateToSettings)
    }
    
    func failed() {
        JSSAlertView().show(
            self,
            title: "Scanning not supported".localized,
            text: "Your device does not support scanning a code from an item.".localized,
            buttonText: "Ok".localized,
            color: UIColor.white)
    }
    
    func found(code: String) {
        scannedData = code
        checkInUser(withQRCode: scannedData)
//        delegate?.fetchedQRCode(scannedCode: self.scannedData)
//        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK: - AVCaptureMetadataOutputObjects Delegate Methods -

extension QRScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {

        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            print("nothing detected")
            return
        }
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        // Here we use filter method to check if the type of metadataObj is supported
        // Instead of hardcoding the AVMetadataObjectTypeQRCode, we check if the type
        // can be found in the array of supported bar codes.
        if supportedBarCodes.contains(metadataObj.type) {

            //if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            if metadataObj.stringValue != nil {
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                found(code: metadataObj.stringValue!)
                captureSession.stopRunning()
            } else {
                //Do something here
                self.view.makeToast("meta object doesn have string value.".localized, duration: 2.0, position: CSToastPositionCenter)
            }
        }
    }
    
}

extension QRScannerViewController {
    
    func checkInUser(withQRCode code: String) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            let urlString = "http://eu.aio-events.com/api/events/\(eventId!)/access_points"
            let params = ["access_point_id": accessPointId!,
                          "qr_token": code,
                          "user_token": auth_Token,
                          "api_token": apiToken]
            
            _ = CallApi.getData(url: urlString, parameter: params, type: .post) { (netResponse) -> (Void)
                in
                print("Heyy", netResponse)
                
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    var message = "Successfully Checked In".localized
                    if let JSON = netResponse.responseDict as? NSDictionary, let successMesage = JSON.value(forKey: "message") as? String {
                        message = successMesage
                    }
                    DispatchQueue.main.async {
                        let alert = JSSAlertView().success(
                            self,
                            title: "Success!".localized,
                            text: message,
                            buttonText: "Ok".localized)
                        alert.addAction {
                            self.captureSession.startRunning()
                        }
                    }
                    self.hideHUD(forView: self.view)
                } else {
                    self.hideHUD(forView: self.view)
                    var message = "Invalid QR code".localized
                    if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                        message = errorMessage
                    }
                    DispatchQueue.main.async {
                        let alert = JSSAlertView().danger(
                            self,
                            title: "Error!".localized,
                            text: message,
                            buttonText: "Ok".localized)
                        alert.addAction {
                            self.captureSession.startRunning()
                        }

                    }
                }
            }
        }
    }

}
