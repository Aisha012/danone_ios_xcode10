//
//  AskQuestionVC.swift
//  CCBT Unified
//
//  Created by devlopment on 07/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import JSSAlertView

class AskQuestionVC: BaseClassVC {
    
    @IBOutlet weak var questionTextView: UITextView!
    
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!

    var eventId : String?
    var agendaModel: AgendaListModal?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        self.titleLabel.text = "Ask Question".localized
        questionTextView.becomeFirstResponder()
        menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
        menuButton.setImage(nil, for: .normal)
        backImage.image = UIImage(named: "back.png")
    }
    
    func setLayout(){
        self.sendBtn.layer.shadowColor = UIColor.lightGray.cgColor
        self.sendBtn.layer.shadowOffset = CGSize(width: 0, height: 3)//CGSize(5, 5)
        self.sendBtn.layer.shadowRadius = 5
        self.clearBtn.layer.shadowColor = UIColor.lightGray.cgColor
        self.clearBtn.layer.shadowOffset = CGSize(width: 0, height: 3)//CGSize(5, 5)
        self.clearBtn.layer.shadowRadius = 5
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let themeColor = navigationBarColor()
        customNavBar.backgroundColor = themeColor
        bottomView.backgroundColor = themeColor
        self.navigationController?.navigationBar.isHidden = true
        questionTextView.layer.cornerRadius = 3.0
        questionTextView.layer.borderColor = UIColor.darkGray.cgColor
        questionTextView.layer.borderWidth = 1.0
        questionTextView.delegate = self
        self.clearBtn.backgroundColor = themeColor
        self.sendBtn.backgroundColor = themeColor

        placeholderLabel.font = UIFont.italicSystemFont(ofSize: (questionTextView.font?.pointSize)!)
        placeholderLabel.isHidden = !questionTextView.text.isEmpty
    }
    
    @IBAction func sendBtn(_ sender: Any) {

        self.view.endEditing(true)
        if (self.questionTextView.text?.isEmpty)! {
            JSSAlertView().danger(
                self,
                title: "Error!".localized,
                text: "Please enter your question.".localized,
                buttonText: "Ok".localized)
        }else{
            self.QuestionPostApi()
        }
    }
    
    @IBAction func clearBtn(_ sender: Any) {
        self.questionTextView.text = ""
    }
    
    func QuestionPostApi() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        let param: NSDictionary = ["title" : self.questionTextView.text ?? "",
                                   "user_token" : auth_Token!,"api_token": apiToken]
        _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/\(agendaModel?.agendaListId! ?? "0")/questions", parameter: param as? Dictionary<String, Any>, type: .post) { (netResponse) -> (Void) in
            
            
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                
                if let JSON = netResponse.responseDict as? NSDictionary {
                    print(JSON)
                    JSSAlertView().success(
                        self,
                        title: AppName,
                        text: "Question Posted Successfully.".localized,
                        buttonText: "Ok".localized)
                    self.questionTextView.text = ""
                }
                
            }else {
                self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
            }
        }
        
    }
}

extension AskQuestionVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.text = !textView.text.isEmpty ? "" : "Description".localized
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        placeholderLabel.text = !textView.text.isEmpty ? "" : "Description".localized
    }

}


