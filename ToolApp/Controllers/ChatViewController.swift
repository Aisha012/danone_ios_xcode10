//
//  ChatViewController.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import MessageKit
import Starscream
import ActionCableClient
import MessageInputBar

class ChatViewController: MessagesViewController, UIGestureRecognizerDelegate {
    
    let refreshControl = UIRefreshControl()
    var eventData: EventData!
    var eventId: String!
    var conversationId: String!
    var friendId: String!
    var userData: LoginData!
    var chatUser: ChatUser!
    var messagePagination = MessagePagination()
    var isFirstTime = true
    var isFetchingData = false
    var participantName: String!
    let client = ActionCableClient(url: URL(string: "ws://yourevent2go.com/cable?user_token=\(UserDefaults.standard.value(forKey: "Auth_Token") as? String ?? "")")!)
    var roomChannel: Channel!
    var userOneImage: UIImage!
    var userTwoImage: UIImage!
    var isFromNotification =  false
    var showsDateHeaderAfterTimeInterval: TimeInterval = 86400

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        subscribeToChannel()
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(loadMoreMessages), for: .valueChanged)
        messagesCollectionView.messageCellDelegate = self
        defaultStyle()
        if isFromNotification {
            fetchConversation()
        } else {
            normalLoad()
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        configureNavBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        roomChannel?.unsubscribe()
        client.disconnect()
    }
    
    func normalLoad() {
        self.title = participantName
        eventId = eventData.event_id
        downloadAvatarImages(imageUrl: userData.avatar, isFirst: true)
        fetchMessages()
    }
    
    func downloadAvatarImages(imageUrl: String?, isFirst: Bool) {

        guard let downloadImageURL = imageUrl, let imgUrl = URL(string: downloadImageURL) else {
            if isFirst == true {
                self.downloadAvatarImages(imageUrl: self.chatUser.avatar, isFirst: false)
            }
            return
        }

        let session = URLSession.shared
        let task = session.dataTask(with:imgUrl) { (data, response, error) in
            if error == nil {
                if isFirst == true {
                    if let image = UIImage(data: data!) {
                        self.userOneImage = image
                    }
                    
                    self.downloadAvatarImages(imageUrl: self.chatUser.avatar, isFirst: false)
                } else {
                    if let image = UIImage(data: data!) {
                        self.userTwoImage = image
                    }
                }
            }
        }
        
        task.resume()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func configureNavBar() {
        let color: UIColor!
        if let color_Value  =  UserDefaults.standard.value(forKey: "color_Value") as? String {
            color = UIColor.colorWithHexString(hex: color_Value)
        }else{
            color =  UIColor.init(red: 0.0/255.0, green: 195.0/255.0, blue: 231/255.0, alpha: 1)
        }
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = color
        self.navigationController?.navigationBar.barTintColor = color
        self.navigationController?.navigationBar.isHidden = false
        var image = UIImage(named: "back.png")
        image = image?.withRenderingMode(.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(navigateBack))
        
        let originalImage = UIImage(named: "user.png")
        let templateImage = originalImage!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: templateImage, style: .plain, target: self, action: #selector(showUserProfile))
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

    }
    
    @objc func loadMoreMessages() {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        configureNavBar()
        messagePagination.paginationType = .old
        fetchMessages()
    }

    @objc func navigateBack() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func showUserProfile() {
        self.view.endEditing(true)
//        let alert = UIAlertController(title: AppName, message: "Choose an action", preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "Share My Profile", style: .default) { action in
//            DispatchQueue.main.async {
                let phoneNumber = self.userData.mobile ?? ""
                let position = self.userData.position ?? ""
                let company = self.userData.company ?? ""
                let role = self.userData.role ?? ""

//                Name.Email,CompanyName.Job Description)
                var text = "Name - \(self.userData.first_name! + " " + self.userData.last_name!)"
                text += "\nEmail - \(self.userData.email!)"
                if !phoneNumber.isEmpty {
                    text += "\nPhone Number - \(phoneNumber)"
                }
                if !company.isEmpty {
                    text += "\nCompany - \(company)"
                }
                if !position.isEmpty {
                    text += "\nPosition - \(position)"
                }
                if !role.isEmpty {
                    text += "\nRole - \(role)"
                }
                self.messageInputBar.inputTextView.text = text
//            }
//        })

//        alert.addAction(UIAlertAction(title: "View \(self.chatUser.firstName ?? "") Profile", style: .default) { action in
//            DispatchQueue.main.async {
//                let profileController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
//                profileController.showMenuButton = false
//                profileController.chatUser = self.chatUser
//                self.navigationController?.pushViewController(profileController, animated: true)
//            }
//        })
//        present(alert, animated: true, completion: nil)
//
    }
    
    func subscribeToChannel() {
        
        self.client.willConnect = {
            print("Will Connect")
        }
        
        self.client.onConnected = {
            self.roomChannel.subscribe()
            print("Connected to \(self.client.url)")
        }
        
        self.client.onDisconnected = {(error: ConnectionError?) in
            print("Disconected with error: \(String(describing: error))")
        }
        
        self.client.willReconnect = {
            print("Reconnecting to \(self.client.url)")
            return true
        }
        

        let room_identifier = ["conversation_id" : conversationId!]
        self.roomChannel = client.create("ConversationsChannel", identifier: room_identifier, autoSubscribe: false, bufferActions: true)

        roomChannel.onReceive = { (JSON : Any?, error : Error?) in
            print("Message!")

            if error == nil, let JSONObject = JSON as? NSDictionary {
                let message = MessageInfo(responseDict: JSONObject["message"] as! NSDictionary)
                if message.sender.id != self.userData.user_id! {
                    self.messagePagination.messages.append(message)
                    DispatchQueue.main.async {
                        self.messagesCollectionView.reloadData()
                        self.messagesCollectionView.scrollToBottom()
                    }
                }
            }
            
        }
        
        self.roomChannel.onSubscribed = {
            print("Yay!")
        }
        
        self.roomChannel.onUnsubscribed = {
            print("Unsubscribed")
        }
        
        self.roomChannel.onRejected = {
            print("Rejected")
        }
        
        client.connect()

    }
    
    func defaultStyle() {
        let newMessageInputBar = messageInputBar
        newMessageInputBar.sendButton.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
        newMessageInputBar.delegate = self
        messageInputBar = newMessageInputBar
        reloadInputViews()
    }

}

extension ChatViewController {

    func showHUD(forView view: UIView!, excludeViews: [UIView?]) {
        DispatchQueue.main.async {
            let hudView = MBProgressHUD.showAdded(to: view, animated: true)
            view.bringSubviewToFront(hudView)
            
            if !excludeViews.isEmpty {
                for subView in excludeViews {
                    view.bringSubviewToFront(subView!)
                }
            }
        }
    }
    
    func hideHUD(forView view: UIView!) {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: view, animated: true);
        }
    }

}

extension ChatViewController {
    
    //API Calls
    func fetchMessages() {
        let user_token = userData.authenticate_user
        isFetchingData = true
        if messagePagination.paginationType != .old {
            self.messagePagination.currentPageNumber = 1
            if isFirstTime == true {
                showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/messages?conversation_id=\(conversationId!)&user_token=\(user_token!)&api_token=\(apiToken)&page=\(messagePagination.currentPageNumber))", parameter: nil, type: .get) { (netResponse) -> (Void) in
            self.isFetchingData = false
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                
                if let JSON = netResponse.responseDict as? [NSDictionary] {
                    let newPagination = MessagePagination(array: JSON)
                    self.messagePagination.appendDataFromObject(newPagination)
                    self.hideHUD(forView: self.view)
                    DispatchQueue.main.async {
                        if self.messagePagination.paginationType == .new {
                            self.messagesCollectionView.reloadData()
                            self.messagesCollectionView.scrollToBottom()
                        } else {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            self.messagesCollectionView.reloadDataAndKeepOffset()
                            self.refreshControl.endRefreshing()
                        }
                    }
                } else {
                    self.hideHUD(forView: self.view)
                }
            } else {
                self.hideHUD(forView: self.view)
            }
        }
    }
    
    func sendMessage(message: String) {
        
        let user_token = userData.authenticate_user

        let param: NSDictionary = ["conversation_id" : self.conversationId,
                                   "body" : message,
                                   "api_token": apiToken,
                                   "user_token": user_token!]
        _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/messages", parameter: param as? Dictionary<String, Any>, type: .post) { (netResponse) -> (Void) in
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                
                if let JSON = netResponse.responseDict as? NSDictionary {
                    let message = MessageInfo(responseDict: JSON)
                    self.messagePagination.messages.append(message)
                    DispatchQueue.main.async {
                        self.messagesCollectionView.reloadData()
                        self.messagesCollectionView.scrollToBottom()
                    }
                }
            }
        }
    }

    func fetchConversation() {
        
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            _ = self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/conversations/\(conversationId!)?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    self.hideHUD(forView: self.view)
                    if let JSON = netResponse.responseDict as? NSDictionary,
                        let conversation = JSON["conversation"] as? NSDictionary {
                        let convo = Conversation(responseDict: conversation)
                        self.chatUser = ChatUser(conversation: convo)
                        self.participantName = convo.firstName
                        DispatchQueue.main.async {
                            self.normalLoad()
                        }
                    }
                }
            }
        }
    }
    
}

extension ChatViewController: MessagesDataSource {
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messagePagination.messages.count
    }
    
    
    func currentSender() -> Sender {
        let id = userData.user_id
        let name = userData.first_name ?? "John Doe"
        return Sender(id: id!, displayName: name)
    }
    
    func numberOfMessages(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messagePagination.messages.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messagePagination.messages[indexPath.section]
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.section == 2, self.messagePagination.hasMoreToLoad, !isFetchingData {
            loadMoreMessages()
        }
    }
    
      func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = "\(message.sentDate)"
//        Added date and time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, d MMM yyyy"
        let formattedDate: String? = dateFormatter.string(from: message.sentDate)
        
        return NSAttributedString(string: formattedDate ?? "", attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }
    
    func cellBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
    
        struct ConversationDateFormatter {
            static let formatter: DateFormatter = {
                let formatter = DateFormatter()
                formatter.dateStyle = .medium
                return formatter
            }()
        }
        let formatter = ConversationDateFormatter.formatter
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
    
   
    
    
//    func cellTopLabelAlignment(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> LabelAlignment {
//
//        if isFromCurrentSender(message: message) {
//            return messagePadding(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10))
//        } else {
//            return messagePadding(UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0))
//        }
//    }
    
//    func cellBottomLabelAlignment(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> LabelAlignment {
//        if isFromCurrentSender(message: message) {
//            return messagePadding(UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0))
//        } else {
//            return messagePadding(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10))
//        }
//    }

}


extension ChatViewController: MessagesDisplayDelegate {
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        var profileImage: UIImage!
        if let messageInfo = message as? MessageInfo {
            if messageInfo.sender.id == userData.user_id {
                profileImage = userOneImage
            } else {
                profileImage = userTwoImage
            }
            let avatar = Avatar(image: profileImage, initials: "\(messageInfo.sender.displayName.first!)")
            avatarView.set(avatar: avatar)
            
        }
    }

    
}

extension ChatViewController: MessagesLayoutDelegate {
    
    func avatarPosition(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> AvatarPosition {
        return AvatarPosition(horizontal: .natural, vertical: .messageBottom)
    }
    
    func messagePadding(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIEdgeInsets {
        if isFromCurrentSender(message: message) {
            return UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 4)
        } else {
            return UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 30)
        }
    }
    
    func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        
        return CGSize(width: messagesCollectionView.bounds.width, height: 10)
    }
    
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {

        guard let dataSource = messagesCollectionView.messagesDataSource else { return 0 }
        if indexPath.section == 0 { return 30 }
        let previousSection = indexPath.section - 1
        let previousIndexPath = IndexPath(item: 0, section: previousSection)
        let previousMessage = dataSource.messageForItem(at: previousIndexPath, in: messagesCollectionView)
        let timeIntervalSinceLastMessage = message.sentDate.timeIntervalSince(previousMessage.sentDate)
        if  timeIntervalSinceLastMessage >= self.showsDateHeaderAfterTimeInterval{
            return 30
        }
        return 0
    }
    
    
    
    // MARK: - Location Messages
    
    func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 200
    }
    
}


extension ChatViewController: MessageInputBarDelegate {
    
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        
        for component in inputBar.inputTextView.components {
            
           if let text = component as? String {
            sendMessage(message: text)
            }
        }
        
        
        inputBar.inputTextView.text = String()
        messagesCollectionView.scrollToBottom()
    }
    
}

extension ChatViewController: MessageCellDelegate {
    
    func didTapAvatar(in cell: MessageCollectionViewCell) {
        if let index = messagesCollectionView.indexPath(for: cell) {
            let userInfo = messagePagination.messages[index.section].sender
            if userInfo.id == userData.user_id {
                let profileController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                profileController.showMenuButton = false
                
                profileController.user = userData
                self.navigationController?.pushViewController(profileController, animated: true)
                
            }
        }
    }

}


