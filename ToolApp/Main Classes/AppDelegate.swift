//
//  AppDelegate.swift
//  ToolApp
//
//  Created by Zaman Meraj on 26/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var revealViewController = SWRevealViewController()
    var fetchedEventsData = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        Fabric.with([Crashlytics.self])

        Utilities.shared.setupReachability()
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        }
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(ChatViewController.self)
        registerForPushNotifications()

        UINavigationBar.appearance().tintColor = UIColor.white
        let reg   = UserDefaults.standard.object(forKey: "Registered") as? String
        
        if let launchOptions = launchOptions,
            let remoteNotificationDict = launchOptions[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable : Any], reg == "userRegister" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let frontviewcontroller = storyboard.instantiateViewController(withIdentifier: "EventListVc") as! EventListVc
            frontviewcontroller.userInfo = remoteNotificationDict
            let objNavigationController : UINavigationController    =   UINavigationController.init(rootViewController: frontviewcontroller)
            let rearViewController = storyboard.instantiateViewController(withIdentifier: "menuViewController") as? menuViewController
            revealViewController = SWRevealViewController()
            revealViewController.frontViewController = objNavigationController
            revealViewController.rearViewController = rearViewController
            self.window!.rootViewController = revealViewController
            self.window?.makeKeyAndVisible()
        } else {
            self.checkAutoLogin()
        }

        return true
    }

    func checkAutoLogin(){
        let reg   = UserDefaults.standard.object(forKey: "Registered") as? String
        self.window = UIWindow(frame: UIScreen.main.bounds)
        guard Utilities.shared.isNetworkReachable() else {
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "NoInternetViewController") as! NoInternetViewController
            let objNavigationController : UINavigationController    =   UINavigationController.init(rootViewController: controller)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = objNavigationController
            self.window?.makeKeyAndVisible()
            return
        }
        if reg != "userRegister" || reg == nil{
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            revealViewController = storyBoard.instantiateViewController(withIdentifier: "RevealViewController") as! SWRevealViewController
            window?.rootViewController = revealViewController
            self.window?.makeKeyAndVisible()
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let frontviewcontroller = storyboard.instantiateViewController(withIdentifier: "EventListVc") as! EventListVc
            let objNavigationController : UINavigationController    =   UINavigationController.init(rootViewController: frontviewcontroller)
            let rearViewController = storyboard.instantiateViewController(withIdentifier: "menuViewController") as? menuViewController
            revealViewController = SWRevealViewController()
            revealViewController.frontViewController = objNavigationController
            revealViewController.rearViewController = rearViewController
            self.window!.rootViewController = revealViewController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, LISDKCallbackHandler.shouldHandle(url) {
//            return LISDKCallbackHandler.application(<#T##application: UIApplication!##UIApplication!#>, open: <#T##URL!#>, sourceApplication: <#T##String!#>, annotation: Any!)
            return LISDKCallbackHandler.application(app, open: url, sourceApplication: sourceApplication, annotation: nil)
        }
        return true
    }

    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                guard granted else { return }
                self.getNotificationSettings()
            }
        } else {
            let setting = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
            getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {
            if UIApplication.shared.isRegisteredForRemoteNotifications == false {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        UserDefaults.standard.set(token, forKey: "DeviceToken")
        UserDefaults.standard.synchronize()
        Utilities.shared.registerDeviceToken()
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        UserDefaults.standard.set("", forKey: "DeviceToken")
        print("Failed to register: \(error)")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        Utilities.shared.registerDeviceToken()
        UserDefaults.standard.set(UIApplication.shared.applicationIconBadgeNumber, forKey: BadgeNumber)
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: .notificationCountChanged, object: nil, userInfo:nil)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let badgeNumber = UserDefaults.standard.integer(forKey: BadgeNumber)
        UserDefaults.standard.set(badgeNumber + 1, forKey: BadgeNumber)
        UserDefaults.standard.synchronize()

        NotificationCenter.default.post(name: .notificationCountChanged, object: nil, userInfo:nil)

        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("userInfo: \n", userInfo)
        BaseClassVC.handleNotificationWhenLive(userInfo: userInfo)
        switch application.applicationState {
        case .active:
            //app is currently active, can update badges count here
            print("active")
        case .inactive:
            //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
            print("inactive")
        case .background:
            //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
            print("background")
        }
    }
    
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "ToolApp", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "ToolApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // iOS 9 and below
    lazy var applicationDocumentsDirectory: URL = {
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    
    

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    
    // MARK: - Core Data Saving support

    func saveContext () {
        if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
    }

}

