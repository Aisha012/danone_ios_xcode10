//
//  EventModal.swift
//  ToolApp
//
//  Created by Zaman Meraj on 03/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import Foundation

struct EventModal {
    
    var eventId:String?
    var eventName: String?
    var eventDescription: String?
    var startTime: String?
    var endTime: String?
    var eventLogo: String?
    var youtube: String?
    var is_floor_plan: String?
    var gallery: String?
    var speaker:String?
    var appToken: String?
    var appColor: String?
    var checkin: String?
    var sponsors: String?
    var floor_plan: String?
    var document: String?
    var chat: String?
    var agendas: String!
    var allowShare: String!
    var allowUpload: String!
    var postEventSurvey: String!
    
    init(responseDict: NSDictionary) {
        
        self.eventId            = "\(String(describing: responseDict.value(forKey: "id") as! Int))"
        self.eventName          = responseDict.value(forKey: "name") as? String
        self.eventDescription   = responseDict.value(forKey: "description") as? String
        self.startTime          = responseDict.value(forKey: "start_time") as? String
        self.endTime            = responseDict.value(forKey: "end_time") as? String
        self.eventLogo          = responseDict.value(forKey: "logo") as? String
        self.youtube            = responseDict.value(forKey: "youtube") as? String
        self.floor_plan         = responseDict.value(forKey: "floor_plan") as? String
        
        self.agendas = ((responseDict.value(forKey: "agendas") as! Bool) ? "1" : "0")
        self.is_floor_plan      = ((responseDict.value(forKey: "is_floor_plan") as! Bool) ? "1" : "0")
        self.gallery            = ((responseDict.value(forKey: "gallery") as! Bool) ? "1" : "0")
        self.speaker            = ((responseDict.value(forKey: "speakers") as! Bool) ? "1" : "0")
        self.sponsors           = ((responseDict.value(forKey: "sponsors") as! Bool) ? "1" : "0")
        self.document           = ((responseDict.value(forKey: "documents") as! Bool) ? "1" : "0")
        self.chat           = ((responseDict.value(forKey: "chat") as! Bool) ? "1" : "0")
        let checkInExists = responseDict.value(forKey: "check_in") as? Bool ?? false
        self.checkin = checkInExists ? "1" : "0"
        
        self.appToken           = responseDict.value(forKey: "app_token") as? String
        self.appColor           = responseDict.value(forKey: "app_color") as? String
        self.allowShare           = ((responseDict.value(forKey: "allow_share_in_gallery") as! Bool) ? "1" : "0")
        self.allowUpload           = ((responseDict.value(forKey: "allow_upload_in_gallery") as! Bool) ? "1" : "0")
        self.postEventSurvey =  responseDict.value(forKey: "post_event_survey_link") as? String
    }
    
    
    
}
