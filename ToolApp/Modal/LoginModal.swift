//
//  LoginData.swift
//  ToolApp
//
//  Created by Zaman Meraj on 01/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import Foundation

struct LoginModal {
    
    var userId: String?
    var first_name: String?
    var last_name: String?
    var email: String?
    var mobile: String?
    var role: String?
    var qrCode: String?
    var avatar: String?
    var authenticate_user: String?
    var position: String?
    var company: String?
    
    init(responseDict: NSDictionary) {
        self.userId             = "\(String(describing: responseDict.value(forKey: "id") as! Int))"
        self.first_name         = responseDict.value(forKey: "first_name") as? String
        self.last_name          = responseDict.value(forKey: "last_name") as? String
        self.email              = responseDict.value(forKey: "email") as? String
        self.mobile             = responseDict.value(forKey: "mobile") as? String
        self.role               = responseDict.value(forKey: "role") as? String
        self.qrCode             = responseDict.value(forKey: "qr_code_string") as? String
        self.avatar             = responseDict.value(forKey: "avatar") as? String
        self.authenticate_user  = responseDict.value(forKey: "authentication_token") as? String
        self.position  = responseDict.value(forKey: "position") as? String ?? ""
        self.company  = responseDict.value(forKey: "organization") as? String ?? ""

        UserDefaults.standard.set(self.userId, forKey: "UUID")
        UserDefaults.standard.set(self.authenticate_user, forKey: "Auth_Token")        
        UserDefaults.standard.synchronize()
    }
    
    
}

