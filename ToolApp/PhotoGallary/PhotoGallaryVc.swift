//
//  PhotoGallaryVc.swift
//  ToolApp
//
//  Created by devlopment on 05/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
 import Photos
import Alamofire
import OpalImagePicker
import Lightbox
import MobileCoreServices
import MediaPlayer
import AVFoundation
import AVKit

class PhotoGallaryVc: BaseClassVC {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var plusIcon: UIImageView!
    
    fileprivate let imageSession = URLSession(configuration: .default)
    var useCustomOverlay = false
    let picker = UIImagePickerController()
    var selectedMediaObject     =    Data()
    var fileData                =    Data()
    var eventId           :   String?
    var eventData: EventData!
    var pageIndex = 1
    var imageArray :[Data]      =    []
    var dirPath: String?
    var photosPagination = GalleryPagination()
    var showMenuButton = true
    var isFirstTime = true
    var isFetchingData = false
    var galleryCategory: GalleryCategory!
    
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id
        addPhotoButton.isHidden = eventData.allow_upload! != "1"
        plusIcon.isHidden = eventData.allow_upload! != "1"
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }

        self.titleLabel.text = galleryCategory.name
        fetchGallery()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate   = self
        self.collectionView.isScrollEnabled = true
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 4.0
            layout.minimumInteritemSpacing = 4.0
            layout.scrollDirection = .vertical
        }

        self.collectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }

    func showViewsAccordingly() {
        if photosPagination.photos.count == 0 {
            collectionView.isHidden = true
            emptyPlaceholderLabel.isHidden = false
        } else {
            collectionView.reloadData()
            emptyPlaceholderLabel.isHidden = true
        }
    }

    @objc func refreshData(_ sender: Any) {
        photosPagination.paginationType = .reload
        fetchGallery()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    func fetchGallery() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            if photosPagination.paginationType != .old {
                self.photosPagination.currentPageNumber = 1
                if isFirstTime == true {
                    self.showHUD(forView: self.view, excludeViews: [])
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                }
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            isFetchingData = true
            
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/galleries/\(galleryCategory.id!)?user_token=\(auth_Token)&api_token=\(apiToken)&page=\(photosPagination.currentPageNumber))", parameter: nil, type: .get) { (netResponse) -> (Void) in
                self.isFetchingData = false
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        let newPagination = GalleryPagination(array: JSON)
                        self.photosPagination.appendDataFromObject(newPagination)
                        DispatchQueue.main.async {
                            if self.photosPagination.paginationType != .old && self.isFirstTime == true {
                                self.hideHUD(forView: self.view)
                                self.isFirstTime = false
                            } else {
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                            self.showViewsAccordingly()
                        }
                    } else {
                        self.hideHUD(forView: self.view)
                    }
                }else {
                    self.hideHUD(forView: self.view)
                }
            }
        }
    }
    
    func uploadImages(_ images: [UIImage]) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String

        if let auth_Token = auth_Token {
            
            self.showHUD(forView: self.view, excludeViews: [])
            let params = ["user_token": auth_Token,
                          "api_token": apiToken,
                          "caption": "",
                          "category": galleryCategory.id!] as [String : Any]
            
            let url = "http://eu.aio-events.com/api/events/\(eventId!)/galleries?user_token=\(auth_Token)&api_token=\(apiToken)"
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                for (key, value) in params {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                // import image to request
                for (index, image) in images.enumerated() {
                    if let imageData = image.jpegData(compressionQuality: 0.5)
                        
                {
                        multipartFormData.append(imageData, withName: "picture[\(index)]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                    }
                }
            }, to: url,
               
               encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print(response.response as Any) // URL response
                        self.photosPagination.paginationType = .reload
                        DispatchQueue.main.async {
                            self.hideHUD(forView: self.view)
                            self.isFirstTime = true
                            self.fetchGallery()
                        }
                    }
                case .failure(let error):
                    //Show alert
                    print(error)
                }
                
            })
        }
    }
    
    func uploadVideo(_ videoPath: URL) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        
        if let auth_Token = auth_Token {
            
            self.showHUD(forView: self.view, excludeViews: [])
            let params = ["user_token": auth_Token,
                          "api_token": apiToken,
                          "caption": "",
                          "category": galleryCategory.id!] as [String : Any]
            
            let url = "http://eu.aio-events.com/api/events/\(eventId!)/galleries?user_token=\(auth_Token)&api_token=\(apiToken)"
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                for (key, value) in params {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                multipartFormData.append(videoPath, withName: "picture", fileName: "\(Date().timeIntervalSince1970).mp4", mimeType: "video/mp4")
            }, to: url,
               
               encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        self.photosPagination.paginationType = .reload
                        DispatchQueue.main.async {
                            self.hideHUD(forView: self.view)
                            self.isFirstTime = true
                            self.fetchGallery()
                        }
                    }
                case .failure(let error):
                    //Show alert
                    print(error)
                }
                
            })
        }
    }
        
    func generateBoundaryString() -> String{
        return "Boundary-\(UUID().uuidString)"
    }
    
    @IBAction func addMoreBtn(_ sender: Any) {
        let alert = UIAlertController(title: "Upload Media".localized, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Take Photo".localized, style: .default, handler: { _ in
            self.camera()
        }))
        
        alert.addAction(UIAlertAction(title: "Photo Gallery".localized, style: .default, handler: { _ in
            self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction(title: "Record Video".localized, style: .default, handler: { _ in
            _ = self.startCameraFromViewController(viewController: self, withDelegate: self)
        }))

        alert.addAction(UIAlertAction(title: "Video Gallery".localized, style: .default, handler: { _ in
            self.videoLibrary()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
    
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
    func encodeVideo(videoUrl: URL, outputUrl: URL? = nil, resultClosure: @escaping (URL?) -> Void ) {
        
        var finalOutputUrl: URL? = outputUrl
        
        if finalOutputUrl == nil {
            var url = videoUrl
            url.deletePathExtension()
            url.appendPathExtension("mp4")
            finalOutputUrl = url
        }
        
        if FileManager.default.fileExists(atPath: finalOutputUrl!.path) {
            print("Converted file already exists \(finalOutputUrl!.path)")
            resultClosure(finalOutputUrl)
            return
        }
        
        let asset = AVURLAsset(url: videoUrl)
        if let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetPassthrough) {
            exportSession.outputURL = finalOutputUrl!
            exportSession.outputFileType = AVFileType.mp4
            let start = CMTimeMakeWithSeconds(0.0, 0)
            let range = CMTimeRangeMake(start, asset.duration)
            exportSession.timeRange = range
            exportSession.shouldOptimizeForNetworkUse = true
            exportSession.exportAsynchronously() {
                
                switch exportSession.status {
                case .failed:
                    print("Export failed: \(exportSession.error != nil ? exportSession.error!.localizedDescription : "No Error Info")")
                case .cancelled:
                    print("Export canceled")
                case .completed:
                    resultClosure(finalOutputUrl!)
                default:
                    break
                }
            }
        } else {
            resultClosure(nil)
        }
    }

*/
    
    func encodeVideo(_ videoURL: URL)  {
        
        let avAsset = AVURLAsset(url: videoURL, options: nil)
        
        let startDate = Foundation.Date()
        
        //Create Export session
        let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough)
        
        // exportSession = AVAssetExportSession(asset: composition, presetName: mp4Quality)
        //Creating temp path to save the converted video
        
        
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let myDocumentPath = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("temp.mp4").absoluteString
        let url = URL(fileURLWithPath: myDocumentPath)
        
        let documentsDirectory2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
        
        let filePath = documentsDirectory2.appendingPathComponent("rendered-Video.mp4")
        deleteFile(filePath)
        
        //Check if the file already exists then remove the previous file
        if FileManager.default.fileExists(atPath: myDocumentPath) {
            do {
                try FileManager.default.removeItem(atPath: myDocumentPath)
            }
            catch let error {
                print(error)
            }
        }
        
        
        
        exportSession!.outputURL = filePath
        exportSession!.outputFileType = AVFileType.mp4
        exportSession!.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
        let range = CMTimeRangeMake(start: start, duration: avAsset.duration)
        exportSession!.timeRange = range
        
        exportSession!.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession!.status {
            case .failed:
                print("%@",exportSession!.error)
            case .cancelled:
                print("Export canceled")
            case .completed:
                //Video conversion finished
                let endDate = Foundation.Date()
                
                let time = endDate.timeIntervalSince(startDate)
                print(time)
                print("Successful!")
                print(exportSession!.outputURL)
                
                DispatchQueue.main.async {
                    self.uploadVideo(exportSession!.outputURL!)
                }
            default:
                break
            }
            
        })
        
        
    }
    
    func deleteFile(_ filePath:URL) {
        guard FileManager.default.fileExists(atPath: filePath.path) else {
            return
        }
        
        do {
            try FileManager.default.removeItem(atPath: filePath.path)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }

}

extension PhotoGallaryVc: UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryCollectionViewCell.cellIdentifier(), for: indexPath) as! GalleryCollectionViewCell
        
        cell.galleryImageView.sd_setImage(with: URL(string: photosPagination.photos[indexPath.row].thumb), placeholderImage: UIImage(named: "gallery_placeholder"), options: .retryFailed, completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosPagination.photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == photosPagination.photos.count - 1 && self.photosPagination.hasMoreToLoad && !isFetchingData {
            self.photosPagination.paginationType = .old
            fetchGallery()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var images = [LightboxImage]()
        for image in photosPagination.photos {
            if image.banner.range(of:".mp4") == nil {
                images.append(LightboxImage.init(imageURL: URL.init(string: image.banner)!, text: image.caption))
            } else {
                images.append(LightboxImage.init(imageURL: URL.init(string: image.thumb)!, text: image.caption, videoURL: URL.init(string: image.banner)!))
            }
            
        }

        let controller = LightboxController(images: images)
        LightboxConfig.InfoLabel.ellipsisText = "Show more"
        LightboxConfig.DeleteButton.image = UIImage(named: "share-file")
        LightboxConfig.DeleteButton.enabled = eventData.allow_share! == "1"
        LightboxConfig.DeleteButton.text = ""

        LightboxConfig.CloseButton.image = UIImage(named: "cross")
        LightboxConfig.CloseButton.enabled = true
        LightboxConfig.CloseButton.text = ""

        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self

        // Use dynamic background.
        controller.dynamicBackground = true
//        controller.currentPage.hashValue = indexPath.row
//        controller.currentPage = indexPath.row
        // Present your controller.
        present(controller, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionView.elementKindSectionFooter, photosPagination.hasMoreToLoad == true {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "FooterView", for: indexPath) as! GalleryFooterCollectionReusableView
            footerView.activityIndicator.startAnimating()
            return footerView
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if photosPagination.hasMoreToLoad == true {
            return CGSize.init(width: collectionView.frame.size.width, height: 50.0)
        } else {
            return CGSize.init(width: 0, height: 0)
        }
    }
    
}

extension PhotoGallaryVc : UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.collectionView.bounds.width / 2) - 15 , height: self.view.frame.size.height * 0.2)
        
    }
}

extension PhotoGallaryVc: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary() {
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = 10

        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        
        let configuration = OpalImagePickerConfiguration()
        configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select more than 10 images", comment: "")
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        imagePicker.navigationBar.tintColor = UIColor.black
        imagePicker.selectionImageTintColor = UIColor.black
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        imagePicker.configuration = configuration
        imagePicker.imagePickerDelegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    func videoLibrary() {
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = 1
        
        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.video])
        
        let configuration = OpalImagePickerConfiguration()
        configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select more than 1 video", comment: "")
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        imagePicker.navigationBar.tintColor = UIColor.black
        imagePicker.selectionImageTintColor = UIColor.black
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        imagePicker.configuration = configuration
        imagePicker.imagePickerDelegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    func startCameraFromViewController(viewController: UIViewController, withDelegate delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate) -> Bool {
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            return false
        }
        
        let cameraController = UIImagePickerController()
        cameraController.sourceType = .camera
        cameraController.mediaTypes = [kUTTypeMovie as NSString as String]
        cameraController.allowsEditing = false
        cameraController.delegate = delegate
        
        present(cameraController, animated: true, completion: nil)
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        let mediaType = info[.mediaType]  as! String
        if mediaType == "public.movie" {
            let videoPath = info[.mediaURL] as! URL
            self.encodeVideo(videoPath)
//            let _ = encodeVideo(videoUrl: videoPath) { (url) in
//                print(url)
//
//                DispatchQueue.main.async {
//                    self.uploadVideo(url!)
//                }
//            }
        } else {
            let image = info[.originalImage] as! UIImage
            uploadImages([image])
        }

        dismiss(animated: true, completion: nil)
    }
    
}

extension PhotoGallaryVc: OpalImagePickerControllerDelegate {
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        if assets[0].mediaType == PHAssetMediaType.video {
            assets[0].requestContentEditingInput(with: PHContentEditingInputRequestOptions(), completionHandler: { (contentEditingInput, dictInfo) in
                    if let strURL = (contentEditingInput!.avAsset as? AVURLAsset)?.url {
                        self.encodeVideo(strURL)
                    }
            })
        } else {
            var imagesArray = [UIImage]()
            for asset in assets {
                if let image = getUIImage(asset: asset) {
                    imagesArray.append(image)
                }
            }
            
            uploadImages(imagesArray)
        }
        dismiss(animated: true , completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        print("cancel")
    }
    
    func getUIImage(asset: PHAsset) -> UIImage? {
        
        var img: UIImage?
        let manager = PHImageManager.default()
        let options = PHImageRequestOptions()
        options.version = .original
        options.isSynchronous = true
        manager.requestImageData(for: asset, options: options) { data, _, _, _ in
            
            if let data = data {
                img = UIImage(data: data)
            }
        }
        return img
    }
}

extension PhotoGallaryVc: LightboxControllerPageDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

extension PhotoGallaryVc: LightboxControllerDismissalDelegate {
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
    
    func lightboxShareOption(_ controller: LightboxController, forItem index: Int) {
        // ...
    }

}

