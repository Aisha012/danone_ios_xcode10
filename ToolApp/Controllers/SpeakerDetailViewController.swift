//
//  SpeakerDetailViewController.swift
//  ToolApp
//
//  Created by Phaninder on 13/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import SafariServices

protocol SpeakerDetailViewControllerDelegate {
    
    func likedUnlikedButtonTapped(speaker: SpeakersModal)
    
}

class SpeakerDetailViewController: BaseClassVC {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!

    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var authorTitleLabel: UILabel!
    @IBOutlet weak var authorSubtitleLabel: UILabel!
    @IBOutlet weak var authorDescriptionLabel: UITextView!
    
    @IBOutlet weak var thumbsUpButton: UIButton!
    @IBOutlet weak var voteNumberLabel: UILabel!
    @IBOutlet weak var voteTextLabel: UILabel!
    @IBOutlet weak var voteView: UIView!
    
    @IBOutlet weak var fbHolderView: UIView!
    @IBOutlet weak var linkedInHolderView: UIView!
    @IBOutlet weak var googleHolderView: UIView!
    @IBOutlet weak var twitterHolderView: UIView!
    
    var speaker: SpeakersModal!
    var eventId: String?
    var showUpvoteOption = true
    var controllerDelegate: SpeakerDetailViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
        menuButton.setImage(nil, for: .normal)
        backImage.image = UIImage(named: "back.png")
        titleLabel.text = speaker.name ?? "Speaker Details"
        voteView.isHidden = !showUpvoteOption
        fbHolderView.isHidden = speaker.facebookLink.isEmpty
        linkedInHolderView.isHidden = speaker.linkedInLink.isEmpty
        addShadowToView(fbHolderView)
        addShadowToView(linkedInHolderView)
//        addShadowToView(googleHolderView)
//        addShadowToView(twitterHolderView)
        loadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }

    func loadData() {
        speakerImageView.sd_setShowActivityIndicatorView(true)
        speakerImageView.sd_setIndicatorStyle(.gray)
        speakerImageView.sd_setImage(with: URL(string: speaker.avatar ?? ""), placeholderImage: nil, options: .progressiveDownload, completed: nil)
        authorTitleLabel.text = speaker.title ?? "No title"
        authorSubtitleLabel.text = speaker.subTitle ?? ""
        nameLabel.text = speaker.name
        authorDescriptionLabel.text = speaker.description ?? "None"
        let likeImage = UIImage(named: speaker.hasVoted ? "like" : "unlike")
        thumbsUpButton.setImage(likeImage, for: .normal)
        voteNumberLabel.text = String(speaker.votes)
        voteTextLabel.text = speaker.votes == 1 ? "vote" : "votes"
    }
    
    func pushToWebViewWithURL(_ url: String) {
        if let url = URL(string: url) {
            if #available(iOS 11.0, *) {
                let config: SFSafariViewController.Configuration = SFSafariViewController.Configuration.init()
                config.entersReaderIfAvailable = true
                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true, completion: nil)
            } else {
                let safariController = SFSafariViewController(url: url)
                self.present(safariController, animated: true, completion: nil)
            }
        }
    }

    func addShadowToView(_ holder: UIView) {
        holder.layer.shadowOffset = CGSize(width: 0, height: 0)
        holder.layer.shadowColor = UIColor.black.cgColor
        holder.layer.shadowRadius = 4
        holder.layer.cornerRadius = 5
        holder.layer.shadowOpacity = 0.3
        holder.layer.masksToBounds = false
        holder.layer.rasterizationScale = UIScreen.main.scale
        holder.clipsToBounds = false
    }

    @IBAction func upVoteButtonTapped(_ sender: UIButton) {
        
        let speakerId = speaker.speaker_id ?? "0"
        if speaker.hasVoted {
            downVoteSpeaker(speakerId: speakerId)
        } else {
            upVoteSpeaker(speakerId: speakerId)
        }

    }
    
    @IBAction func facebookButtonTapped(_ sender: UIButton) {
        if let fbLink = speaker.facebookLink,
            !fbLink.isEmpty {
            pushToWebViewWithURL(fbLink)
        }
    }
    
    @IBAction func linkedInButtonTapped(_ sender: UIButton) {
        if let linkedInLink = speaker.linkedInLink,
            !linkedInLink.isEmpty {
            pushToWebViewWithURL(linkedInLink)
        }
    }
    
    func upVoteSpeaker(speakerId: String) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/3/upvote/\(speakerId)?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .post) { (netResponse) -> (Void) in
                
                self.hideHUD(forView: self.view)
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    if let _ = netResponse.responseDict as? NSDictionary {
                        self.speaker.hasVoted = true
                        self.speaker.votes += 1
                        DispatchQueue.main.async {
                            self.controllerDelegate?.likedUnlikedButtonTapped(speaker: self.speaker)
                            self.loadData()
                        }
                    }
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }
        }
    }
    
    func downVoteSpeaker(speakerId: String) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/3/downvote/\(speakerId)?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .post) { (netResponse) -> (Void) in
                
                self.hideHUD(forView: self.view)
                
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let _ = netResponse.responseDict as? NSDictionary {
                        self.speaker.votes -= 1
                        self.speaker.hasVoted = false
                        DispatchQueue.main.async {
                            self.loadData()
                            self.controllerDelegate?.likedUnlikedButtonTapped(speaker: self.speaker)
                        }
                    }
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }
        }
    }

}
