//
//  EventLocation.swift
//  ToolApp
//
//  Created by Phaninder on 19/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation
import MapKit
import Contacts
import Mapbox

class EventLocation: NSObject, MKAnnotation , MGLAnnotation{
    
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    let name : String?
   
    
    var subtitle: String? {
        return locationName
    }
    
    init(json: NSDictionary) {
        self.title = json["description"] as? String ?? "No Title"
        self.locationName = json["address"] as? String ?? ""
        self.name = json["name"] as?  String ?? ""
        if let latitude = Double(json.value(forKey: "latitude") as? String ?? "0"),
            let longitude = Double(json.value(forKey: "longitude") as? String ?? "0") {
            self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        } else {
            self.coordinate = CLLocationCoordinate2D()
        }
    }
    
//    init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D) {
//        self.title = title
//        self.locationName = locationName
//        self.discipline = discipline
//        self.coordinate = coordinate
//
//        super.init()
//    }
    
    
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
    
}
