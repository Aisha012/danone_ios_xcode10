//
//  ConversationCustomPagerViewController.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class ConversationCustomPagerViewController: ButtonBarPagerTabStripViewController {

    var eventId: String?
    var eventData: EventData!
    var searchString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id
        self.settings.style.selectedBarHeight = 1
        self.settings.style.buttonBarMinimumInteritemSpacing = 10
        let buttonColor: UIColor!
        if let color_Value  =  UserDefaults.standard.value(forKey: "color_Value") as? String {
            buttonColor = UIColor.colorWithHexString(hex: color_Value)
        }else{
            buttonColor = UIColor.init(red: 0.0/255.0, green: 195.0/255.0, blue: 231/255.0, alpha: 1)
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(searchFieldChanged(_:)),
                                               name: .searchChanged,
                                               object: nil)

        self.settings.style.selectedBarBackgroundColor = UIColor.colorWithHexString(hex: "784E98")
        self.settings.style.buttonBarItemBackgroundColor = buttonColor
        self.settings.style.buttonBarItemTitleColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = buttonColor
        self.settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: 17.0)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.settings.style.buttonBarItemsShouldFillAvailiableWidth = true
    }
    
    @objc func searchFieldChanged(_ notification: NSNotification) {
        if let search = notification.userInfo?["data"] as? String {
            self.searchString = search
        }
        
        let userInfo = ["data": self.searchString, "page": "\(currentIndex)"];
        
        NotificationCenter.default.post(name: .fetchSearchData, object: nil, userInfo:userInfo)
        
        if let chatvc = self.viewControllers[1] as? ChatListViewController {
            chatvc.searchString = searchString
        }
    }

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let participantsVC = self.storyboard?.instantiateViewController(withIdentifier: "ParticipantsViewController") as! ParticipantsViewController
        participantsVC.eventData = self.eventData
        participantsVC.searchString = searchString
        let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatListViewController") as! ChatListViewController
        chatVC.eventData = self.eventData
        chatVC.searchString = searchString
        return [participantsVC, chatVC]
    }
    
    
}
