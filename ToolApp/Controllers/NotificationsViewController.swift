//
//  NotificationsViewController.swift
//  ToolApp
//
//  Created by Phaninder on 30/04/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class NotificationsViewController: BaseClassVC {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!

    var eventId: String?
    var eventData: EventData!
    var showMenuButton = false
    private let refreshControl = UIRefreshControl()
    var isFirstTime = true
    var isFetchingData = false
    var notificationPagination = NotificationPagination()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(0, forKey: BadgeNumber)
        UserDefaults.standard.synchronize()
        UIApplication.shared.applicationIconBadgeNumber = 0
        NotificationCenter.default.post(name: .notificationCountChanged, object: nil, userInfo:nil)

        
        eventId = eventData.event_id!
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        self.titleLabel.text = "Notifications".localized
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        tableView.tableFooterView = UIView()
        fetchNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func refreshData(_ sender: Any) {
        self.notificationPagination.paginationType = .reload
        self.notificationPagination.currentPageNumber = 1
        fetchNotifications()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    func showViewsAccordingly() {
        if notificationPagination.notifications.count == 0 {
            tableView.isHidden = true
            emptyPlaceholderLabel.isHidden = false
        } else {
            tableView.isHidden = false
            emptyPlaceholderLabel.isHidden = true
        }
        tableView.reloadData()
    }
    
    func fetchNotifications(){
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            if notificationPagination.paginationType != .old {
                self.notificationPagination.currentPageNumber = 1
                if isFirstTime == true {
                    self.showHUD(forView: self.view, excludeViews: [])
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                }
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            isFetchingData = true
            
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/notifications?user_token=\(auth_Token)&api_token=\(apiToken)&page=\(notificationPagination.currentPageNumber))", parameter: nil, type: .get) { (netResponse) -> (Void) in
                self.isFetchingData = false
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        let newPagination = NotificationPagination(array: JSON)
                        self.notificationPagination.appendDataFromObject(newPagination)
                        DispatchQueue.main.async {
                            if self.notificationPagination.paginationType != .old && self.isFirstTime == true {
                                self.hideHUD(forView: self.view)
                                self.isFirstTime = false
                                self.showViewsAccordingly()
                            } else {
                                self.showViewsAccordingly()
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                        }
                    } else {
                        self.hideHUD(forView: self.view)
                    }
                }
            }
        }
    }

}

extension NotificationsViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard notificationPagination.notifications.count > 0  else {
            return 0
        }
        return notificationPagination.notifications.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < notificationPagination.notifications.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationTableViewCell.cellIdentifier(), for: indexPath) as! NotificationTableViewCell
        
        cell.configureCellWithNotification(notificationPagination.notifications[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row < notificationPagination.notifications.count else {
            return notificationPagination.hasMoreToLoad ? 50.0 : 0.0
        }
        let notification = notificationPagination.notifications[indexPath.row]
        if notification.notification_type == "Event" {
            return 0.0
        }

        tableView.estimatedRowHeight = 50.0
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.notificationPagination.notifications.count && self.notificationPagination.hasMoreToLoad && !isFetchingData {
            self.notificationPagination.paginationType = .old
            fetchNotifications()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notification = notificationPagination.notifications[indexPath.row]
        let loginData = MyDatabase.getProgramData(tableName: "LoginData") as? [LoginData]
        if let users = loginData, users.count > 0, let _eventId = eventId {
            
            if notification.notification_type == "chat" {
                BaseClassVC.navigateToChatScreenWithConversationId(String(notification.redirect_id), eventId: _eventId, userData: users[0])
            } else if notification.notification_type == "EventAgenda" {
                BaseClassVC.navigateToAgendaScreen(_eventId, agendaId: String(notification.redirect_id))
            }
        }
    }
    
}
