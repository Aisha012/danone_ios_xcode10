//
//  PageData+CoreDataProperties.swift
//  ToolApp
//
//  Created by Phaninder on 04/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation
import CoreData


extension PageData {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<PageData> {
        return NSFetchRequest<PageData>(entityName: "PageData")
    }
    
    @NSManaged public var page_id: String?
    @NSManaged public var name: String?
    @NSManaged public var desc: String?
    @NSManaged public var event_id: String?

}
