//
//  AgendaButtonsTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 18/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import SwiftyStarRatingView

protocol AgendaButtonsTableViewCellDelegate {
    func attendButtonTapped()
    func askQuestionButtonTapped()
    func seeQuestionButtonTapped()
    func joinPollButtonTapped()
    func submitFeedbackButtonTapped(_ rating: CGFloat, feedback: String)
}

class AgendaButtonsTableViewCell: UITableViewCell, UITextViewDelegate {

    
    @IBOutlet weak var attendButton: UIButton!
//    @IBOutlet weak var askQuestionButton: UIButton!
    @IBOutlet weak var seeQuestionButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var placeholderLabel: UILabel!
    
    @IBOutlet weak var pollingHolderView: UIView!
    @IBOutlet weak var pollingHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var feedbackHolderView: UIView!
    @IBOutlet weak var feedbackHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var feedbackTitle: UILabel!

    @IBOutlet weak var questionHolderView: UIView!
    @IBOutlet weak var questionHeightConstraint: NSLayoutConstraint!
    
    var delegate: AgendaButtonsTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "AgendaButtonsTableViewCell"
    }
    
    func configureCellWithColor(_ color: UIColor, agenda: AgendaListModal) -> CGFloat {
        var cellheight: CGFloat = 474.0
        
        attendButton.backgroundColor = color
        submitButton.backgroundColor = color
        
        attendButton.layer.cornerRadius = 3.0
//        askQuestionButton.layer.cornerRadius = 3.0
        seeQuestionButton.layer.cornerRadius = 3.0
        submitButton.layer.cornerRadius = 3.0

        ratingView.tintColor = color

        descriptionTextView.layer.cornerRadius = 3.0
        descriptionTextView.layer.borderColor = UIColor.darkGray.cgColor
        descriptionTextView.layer.borderWidth = 1.0
        descriptionTextView.delegate = self
        
        placeholderLabel.font = UIFont.italicSystemFont(ofSize: (descriptionTextView.font?.pointSize)!)
        placeholderLabel.isHidden = !descriptionTextView.text.isEmpty
        
        feedbackTitle.text = agenda.feedbackTitle
        
        if agenda.hidePooling == true {
            questionHolderView.isHidden = true
            questionHeightConstraint.constant = 0
            cellheight -= 58.0
        }
        
        if agenda.hidePolling == true {
            pollingHolderView.isHidden = true
            pollingHeightConstraint.constant = 0
            cellheight -= 58.0
        }
        
        if agenda.hideFeedback == true {
            feedbackHolderView.isHidden = true
            feedbackHeightConstraint.constant = 0
            cellheight -= 256.0
        }
        
        return cellheight
    }
    
    @IBAction func attendButtonTapped(_ sender: UIButton) {
        delegate?.attendButtonTapped()
    }
    
    @IBAction func askQuestionButtonTapped(_ sender: UIButton) {
        delegate?.askQuestionButtonTapped()
    }
    
    @IBAction func seeQuestionButtonTapped(_ sender: UIButton) {
        delegate?.seeQuestionButtonTapped()
    }
    
    @IBAction func joinPollButtonTapped(_ sender: UIButton) {
        delegate?.joinPollButtonTapped()
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        delegate?.submitFeedbackButtonTapped(ratingView.value, feedback: descriptionTextView.text)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.text = !textView.text.isEmpty ? "" : "Description".localized
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        placeholderLabel.text = !textView.text.isEmpty ? "" : "Description".localized
    }
    
}
