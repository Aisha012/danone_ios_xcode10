//
//  LoginTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 24/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
protocol LoginTableViewCellDelegate {
    func loginButtonTapped()
//    func rememberEmailButtonTapped()
//    func rememberPasswordButtonTapped()
    func linkedInButtonTapped()
    func forgotPasswordButtonTapped()
}

class LoginTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var tfView1: UIView!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var rememberEmailLbl: UILabel!
    @IBOutlet weak var checkBtnView1: UIView!
    @IBOutlet weak var checkBtnEmail: UIButton!
    @IBOutlet weak var tfView2: UIView!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var rememberPasswordLbl: UILabel!
    @IBOutlet weak var checkBtnView2: UIView!
    @IBOutlet weak var checkBtnPaasword: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var loginWithLinkedInBtn: UIButton!
    @IBOutlet weak var notRegisteredBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!

    var delegate: LoginTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "LoginTableViewCell"
    }
    
    override func awakeFromNib() {
        self.checkBtnEmail.setImage(UIImage(named: "checkbox_uncheck_brand"), for: .normal)
        if let remEmail = UserDefaults.standard.value(forKey: "rem_email") as? Int{
            if remEmail == 1{
                self.emailTF.text  =  UserDefaults.standard.value(forKey: "email") as? String
                self.checkBtnEmail.setImage(UIImage(named: "checkbox_checked_brand"), for: .normal)
                self.checkBtnEmail.backgroundColor = .clear
            }else{
                self.emailTF.text  =  ""
                self.checkBtnEmail.setImage(UIImage(named:"checkbox_uncheck_brand"), for: .normal)
                self.checkBtnEmail.backgroundColor = .clear
            }
        }else{
            self.checkBtnEmail.setImage(UIImage(named:"checkbox_uncheck_brand"), for: .normal)
            self.checkBtnEmail.backgroundColor = .clear
            self.emailTF.text  =  ""
        }
        if let remPass = UserDefaults.standard.value(forKey: "rem_pass") as? Int{
            if remPass == 1{
                self.passwordTF.text  =  UserDefaults.standard.value(forKey: "pass") as? String
                self.checkBtnPaasword.setImage(UIImage(named: "checkbox_checked_brand"), for: .normal)
                self.checkBtnPaasword.backgroundColor = .clear
            }else{
                self.passwordTF.text  =  UserDefaults.standard.value(forKey: "pass") as? String
                self.checkBtnPaasword.setImage(UIImage(named:"checkbox_uncheck_brand"), for: .normal)
                self.checkBtnPaasword.backgroundColor = .clear
            }
        }else{
            self.passwordTF.text  =  UserDefaults.standard.value(forKey: "pass") as? String
            self.checkBtnPaasword.setImage(UIImage(named:"checkbox_uncheck_brand"), for: .normal)
            self.checkBtnPaasword.backgroundColor = .clear
        }
        self.passwordTF.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.colorWithHexString(hex: "F3903F")])
        self.emailTF.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.colorWithHexString(hex: "F3903F")])
        self.passwordTF.isSecureTextEntry = true
        self.passwordTF.delegate = self
        self.emailTF.delegate = self
        self.tfView1.layer.borderColor = UIColor.white.cgColor
        self.tfView1.layer.borderWidth = 0.5
        
        self.tfView2.layer.borderColor = UIColor.white.cgColor
        self.tfView2.layer.borderWidth = 0.5

    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        delegate?.loginButtonTapped()
    }
    
    @IBAction func linkedInButtonTapped(_ sender: UIButton) {
        delegate?.linkedInButtonTapped()
    }
    @IBAction func forgotPassButtonTapped(_ sender: UIButton) {
        delegate?.forgotPasswordButtonTapped()
    }
    @IBAction func rememberEmailButtonTapped(_ sender: UIButton) {
        if self.checkBtnEmail.currentImage == UIImage(named: "checkbox_checked_brand") {
            self.checkBtnEmail.setImage(UIImage(named: "checkbox_uncheck_brand"), for: .normal)
            self.checkBtnEmail.backgroundColor = .clear
        }else{
            self.checkBtnEmail.setImage(UIImage(named: "checkbox_checked_brand"), for: .normal)
            self.checkBtnEmail.backgroundColor = .clear
        }

    }
    
    @IBAction func rememberPasswordButtonTapped(_ sender: UIButton) {
        if self.checkBtnPaasword.currentImage == UIImage(named: "checkbox_checked_brand") {
            self.checkBtnPaasword.setImage(UIImage(named: "checkbox_uncheck_brand"), for: .normal)
            self.checkBtnPaasword.backgroundColor = .clear
        }else{
            self.checkBtnPaasword.setImage(UIImage(named: "checkbox_checked_brand"), for: .normal)
            self.checkBtnPaasword.backgroundColor = .clear
        }
    }
    
}
