//
//  ParticipantsViewController.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire

class ParticipantsViewController: BaseClassVC, IndicatorInfoProvider {

    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var eventId: String?
    var eventData: EventData!
    var participantPagination = ParticipantListPagination()
    private let refreshControl = UIRefreshControl()
    var isFirstTime = true
    var isFetchingData = false
    var userData: LoginData!
    var request: DataRequest!
    var searchString = ""
    let currentIndex = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id
        let loginData = MyDatabase.getProgramData(tableName: "LoginData") as? [LoginData]
        userData = loginData![0]
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(searchFieldChanged(_:)),
                                               name: .fetchSearchData,
                                               object: nil)

        tableView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.isFirstTime = true
        fetchParticipantList()
        
    }
    
    @objc func searchFieldChanged(_ notification: NSNotification) {
        if let search = notification.userInfo?["data"] as? String,
            let page = notification.userInfo?["page"] as? String {
            self.searchString = search
            if page == currentIndex {
                if let req = request {
                    req.cancel()
                }
                self.participantPagination.paginationType = .reload
                self.participantPagination.currentPageNumber = 1
                fetchParticipantList()
            }
        }
    }

    @objc func refreshData(_ sender: Any) {
        self.participantPagination.paginationType = .reload
        self.participantPagination.currentPageNumber = 1
        fetchParticipantList()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    @IBAction func valueChangedInSearchField(_ sender: Any) {
        if let req = request {
            req.cancel()
        }
        self.participantPagination.paginationType = .reload
        self.participantPagination.currentPageNumber = 1
        fetchParticipantList()
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
//        Add Changes
            return IndicatorInfo(title: "Participants".localized)

//        return IndicatorInfo(title: "Delegates".localized)
    }

    func showViewsAccordingly() {
        if participantPagination.participants.count == 0 {
            tableView.isHidden = true
            emptyPlaceholderLabel.isHidden = false
        } else {
            tableView.isHidden = false
            emptyPlaceholderLabel.isHidden = true
        }
        tableView.reloadData()
    }
    
    func fetchParticipantList(){
        let user_token = userData.authenticate_user
        if participantPagination.paginationType != .old {
            self.participantPagination.currentPageNumber = 1
            if isFirstTime == true {
                self.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        isFetchingData = true
        
        request = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/participants?user_token=\(user_token!)&api_token=\(apiToken)&page=\(participantPagination.currentPageNumber)&query=\(searchString)", parameter: nil, type: .get) { (netResponse) -> (Void) in
            self.isFetchingData = false
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                
                if let JSON = netResponse.responseDict as? [NSDictionary] {
                    let newPagination = ParticipantListPagination(array: JSON)
                    self.participantPagination.appendDataFromObject(newPagination)
                    DispatchQueue.main.async {
                        if self.participantPagination.paginationType != .old && self.isFirstTime == true {
                            self.hideHUD(forView: self.view)
                            self.isFirstTime = false
                        } else {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                        self.showViewsAccordingly()
                    }
                } else {
                    self.hideHUD(forView: self.view)
                }
            }else {
                self.hideHUD(forView: self.view)
            }
        }
    }
    
    func createConversation(_ friendId: Int) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])

            let param: NSDictionary = ["friend_id" : String(friendId),
                                       "user_token" : auth_Token,
                                       "api_token": apiToken,
                                       "event_id": self.eventId!,
                                       "user_id": userData.user_id!]
        _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/conversations", parameter: param as? Dictionary<String, Any>, type: .post) { (netResponse) -> (Void) in
                self.hideHUD(forView: self.view)
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSON = netResponse.responseDict as? NSDictionary {
                        let conversation = Conversation(responseDict: JSON)
                        let destination = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                        destination.eventData = self.eventData
                        destination.userData = self.userData
                        destination.conversationId = conversation.id
                        destination.chatUser = ChatUser(conversation: conversation)
                        destination.participantName = conversation.firstName
                        self.navigationController?.pushViewController(destination, animated: true)
                    }
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }
        }
    }


}


extension ParticipantsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard participantPagination.participants.count > 0  else {
            return 0
        }
        return participantPagination.participants.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < participantPagination.participants.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ParticipantTableViewCell.cellIdentifier(), for: indexPath) as! ParticipantTableViewCell
        cell.configureCell(participant: participantPagination.participants[indexPath.row ])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row < participantPagination.participants.count else {
            return participantPagination.hasMoreToLoad ? 50.0 : 0.0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.participantPagination.participants.count && self.participantPagination.hasMoreToLoad && !isFetchingData {
            self.participantPagination.paginationType = .old
            fetchParticipantList()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
        createConversation(participantPagination.participants[indexPath.row].clientId)
    }
}
