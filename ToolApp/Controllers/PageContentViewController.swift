//
//  PageContentViewController.swift
//  ToolApp
//
//  Created by Phaninder on 04/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import WebKit

class PageContentViewController: BaseClassVC, WKUIDelegate {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var webViewHolderView: UIView!
    
    var page: PageData!
    var showMenuButton = true
    var pageDescriptionHTML: NSAttributedString!
    var webView: WKWebView!
    
    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
        menuButton.setImage(nil, for: .normal)
        backImage.image = UIImage(named: "back.png")

            webView.loadHTMLString(page.desc!, baseURL: nil)

        //        pageDescriptionHTML = page.desc?.convertHtml(fontFamilyName: "HelveticaNeue-Light", fontSize: 16)
//        contentTextView.attributedText = pageDescriptionHTML
//        contentTextView.contentOffset = CGPoint(x: 0, y: 0)
//        contentTextView.isScrollEnabled = false
//        contentTextView.isScrollEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isTranslucent = false
        configureNavBar()
    }

    func configureNavBar() {
        self.title = page.name ?? "Page"
        let color: UIColor!
        if let color_Value  =  UserDefaults.standard.value(forKey: "color_Value") as? String {
            color = UIColor.colorWithHexString(hex: color_Value)
        }else{
            color =  UIColor.init(red: 0.0/255.0, green: 195.0/255.0, blue: 231/255.0, alpha: 1)
        }
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = color
        self.navigationController?.navigationBar.barTintColor = color
        self.navigationController?.navigationBar.isHidden = false
        var image = UIImage(named: "back.png")
        image = image?.withRenderingMode(.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(navigateBack))
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }

}
