//
//  ParticipantTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class ParticipantTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var organizationLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!

    
    class func cellIdentifier() -> String {
        return "ParticipantTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.holderView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.holderView.layer.shadowColor = UIColor.black.cgColor
        self.holderView.layer.shadowRadius = 4
        self.holderView.layer.cornerRadius = 5
        self.holderView.layer.shadowOpacity = 0.3
        self.holderView.layer.masksToBounds = false
        self.holderView.layer.rasterizationScale = UIScreen.main.scale
        self.holderView.clipsToBounds = false
    }
    
    func configureCell(participant: Participant) {
        let avatarURL = participant.avatar ?? ""
        profileImageView.sd_setShowActivityIndicatorView(true)
        profileImageView.sd_setIndicatorStyle(.gray)
        profileImageView.sd_setImage(with: URL(string: avatarURL), placeholderImage: UIImage(named: "profile.png"), options: .progressiveDownload, completed: nil)
    
        nameLabel.text = participant.firstName + " " + participant.lastName
    
        positionLabel.isHidden = participant.position.isEmpty
        organizationLabel.isHidden = participant.organization.isEmpty
        categoryLabel.isHidden = participant.category.isEmpty
        countryLabel.isHidden = participant.country.isEmpty
        mobileLabel.isHidden = participant.mobile.isEmpty
        
        organizationLabel.text = participant.organization ?? ""
        positionLabel.text = participant.position ?? ""
        mobileLabel.text = participant.mobile
        categoryLabel.text =  participant.category ?? ""
        countryLabel.text = participant.country ?? ""
        
//        if participant.position.isEmpty && participant.organization.isEmpty {
//            roleLabel.text = ""
//        } else if participant.position.isEmpty {
//            roleLabel.text = participant.organization
//        } else if participant.organization.isEmpty {
//            roleLabel.text = participant.position
//        } else {
//            roleLabel.text = participant.position + " at " + participant.organization
//        }
    }

}
