//
//  EventListVc.swift
//  ToolApp
//
//  Created by mayank on 16/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit

class EventListVc: BaseClassVC {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var customNavBar: UIView!
    
    var eventArray:[EventData]?
    var userInfo: [AnyHashable : Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        self.title = "Select an Event"
        tableView.tableFooterView = UIView()
        if let notificationInformation = userInfo {
            BaseClassVC.handleNotificationWhenLive(userInfo: notificationInformation)

        }
        if (UIApplication.shared.delegate as! AppDelegate).fetchedEventsData == false {
            fetchEventsFromAPI()
        } else {
            fetchEventsDataFromLocal()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }

    func fetchEventsDataFromLocal() {
        let eventsData = MyDatabase.getProgramData(tableName: "EventData") as? [EventData]
        if let eventsData = eventsData {
            self.eventArray  =  eventsData
            if self.eventArray?.count == 1{
                let destination = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                destination.eventId = eventArray![0].event_id
                self.navigationController?.pushViewController(destination, animated: true)
            } else {
                tableView.reloadData()
            }
        }else{
            self.eventArray = []
        }
    }
    

}

extension EventListVc : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let events = eventArray else {
            return 0
        }
        return events.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.18;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "EventListCell") as! EventListCell
        cell.eventName.text  = eventArray![indexPath.row].event_name
        if let url = eventArray![indexPath.row].logo{
            
            if let url = URL(string: url) {
                cell.iamgeName.sd_setShowActivityIndicatorView(true)
                cell.iamgeName.sd_setIndicatorStyle(.gray)
                cell.iamgeName.sd_setImage(with: url, placeholderImage: nil, options: .progressiveDownload, completed: nil)
            }
        }
        
        return cell;
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let eventArray = eventArray, let apptoken = eventArray[indexPath.row].app_token {
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
            
            destination.eventId = eventArray[indexPath.row].event_id
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
}

extension EventListVc {
    
    func fetchEventsFromAPI() {
        
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true

            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    if let json = netResponse.responseDict as? [NSDictionary] {
                        MyDatabase.clearData(entityName: "EventData")
                        MyDatabase.clearData(entityName: "BannerData")
                        MyDatabase.clearData(entityName: "PageData")

                        for event in json {

                            let eventModal = EventModal(responseDict: event)
                            MyDatabase.saveEventLocally(eventModal: eventModal)
                            if let banners = event.value(forKey: "banners") as? [NSDictionary] {
                                for banner in banners {
                                    let bannerModal = BannersModal(responseDict: banner, eventId: eventModal.eventId!)
                                    MyDatabase.saveBannerLocally(bannerModal: bannerModal)
                                    
                                }
                            }
                            if let pages = event.value(forKey: "pages") as? [NSDictionary] {
                                for page in pages {
                                    let pageModal = PageModal(responseDict: page, eventId: eventModal.eventId!)
                                    MyDatabase.savePageLocally(pageModal: pageModal)
                                }
                            }

                        }
                        DispatchQueue.main.async {
                            (UIApplication.shared.delegate as! AppDelegate).fetchedEventsData = true
                            self.fetchEventsDataFromLocal()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                }
            }
        }
    }
}

