//
//  GalleryFooterCollectionReusableView.swift
//  ToolApp
//
//  Created by Phaninder on 21/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class GalleryFooterCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
}
