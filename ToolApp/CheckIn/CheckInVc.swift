//
//  CheckInVc.swift
//  CCBT Unified
//
//  Created by devlopment on 06/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
//import AVFoundation
import JSSAlertView

class CheckInVc: BaseClassVC {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!

    var eventData: EventData!
//    lazy var reader: QRCodeReader = QRCodeReader()
//    lazy var readerVC: QRCodeReaderViewController = {
//        let builder = QRCodeReaderViewControllerBuilder{
//            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
//            $0.showTorchButton = true
//        }
//        return QRCodeReaderViewController(builder: builder)
//    }()
    var accessPoints = [AccessPoint]()
    var showMenuButton = true
    var eventId: String!
    var accessPointId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        self.titleLabel.text = "Check In".localized
        emptyPlaceholderLabel.text = "No Access Points".localized
        fetchAccessPoints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    func showViewsAccordingly() {
        if accessPoints.count == 0 {
            tableView.isHidden = true
            emptyPlaceholderLabel.isHidden = false
        } else {
            emptyPlaceholderLabel.isHidden = true
            tableView.reloadData()
        }
    }

    // MARK: - Actions
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {

            
            switch error.code {
            case -11852:
                
                let alertview = JSSAlertView().danger(
                    self,
                    title: "Error!".localized,
                    text: "This app is not authorized to use Back Camera.".localized,
                    buttonText: "Setting".localized,
                    cancelButtonText: "Later".localized)
                alertview.addAction(navigateToSettings)

            case -11814:
                
                JSSAlertView().danger(
                    self,
                    title: "Error!".localized,
                    text: "Reader not supported by the current device.".localized,
                    buttonText: "Ok".localized)
            default:
                break
            }
            return false
        }
    }

}

//CheckIn View Controller API calls

extension CheckInVc {

    func fetchAccessPoints() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            let urlString = "http://eu.aio-events.com/api/events/\(eventId!)/access_points?user_token=\(auth_Token)&api_token=\(apiToken)"
            _ = CallApi.getData(url: urlString, parameter: nil, type: .get) { (netResponse) -> (Void)
                in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        for accessPointDict in JSON {
                            let accessPoint = AccessPoint(responseDict: accessPointDict)
                            self.accessPoints.append(accessPoint)
                        }
                    }
                    self.showViewsAccordingly()
                    self.hideHUD(forView: self.view)
                } else {
                    self.hideHUD(forView: self.view)
                    self.showViewsAccordingly()
                }
            }
        }
    }
    
    
}

extension CheckInVc: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accessPoints.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckInCell", for: indexPath) as! CheckInCell
        cell.checkIn_out.text = accessPoints[indexPath.row].name
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard checkScanPermissions() else { return }
        self.accessPointId = String(accessPoints[indexPath.row].id)
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "QRScannerViewController") as! QRScannerViewController
        destination.eventId = self.eventId
        destination.accessPointId = self.accessPointId
        self.navigationController?.pushViewController(destination, animated: true)

        
//        readerVC.modalPresentationStyle = .formSheet
//        readerVC.delegate               = self
//        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
//            if let result = result {
//                print("Completion with result: \(result.value) of type \(result.metadataType)")
//            }
//        }
//        present(readerVC, animated: true, completion: nil)
    }
    
}

//extension CheckInVc : QRCodeReaderViewControllerDelegate{
//
//    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
//        reader.stopScanning()
//        dismiss(animated: true) { [weak self] in
//            self?.checkInUser(withQRCode: result.value)
//        }
//    }
//
//    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
//        print("Switching capturing to: \(newCaptureDevice.device.localizedName)")
//    }
//
//    func readerDidCancel(_ reader: QRCodeReaderViewController) {
//        reader.stopScanning()
//
//        dismiss(animated: true, completion: nil)
//    }
//}

