//
//  AddressVC.swift
//  ToolApp
//
//  Created by mayank on 30/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Mapbox
import MapboxDirections
import MapboxCoreNavigation
import MapboxNavigation


class AddressVC: BaseClassVC, CLLocationManagerDelegate, MKMapViewDelegate, MGLMapViewDelegate , UITableViewDelegate ,  UITableViewDataSource{
   
    
    @IBOutlet weak var bottomView: UIView!    
 

    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var tbleView: UITableView!

//    @IBOutlet var mapView: MKMapView!
    @IBOutlet var mapView: MGLMapView!


    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var eventId: String?
    var eventData: EventData!
    var showMenuButton = true
    var eventLocation = [EventLocation]()
    let regionRadius: CLLocationDistance = 1000

    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventId = eventData.event_id
        mapView.styleURL = MGLStyle.streetsStyleURL
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()

//        mapView.delegate = self
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())

        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        self.titleLabel.text = "Venue".localized
//        mapView!.showsPointsOfInterest = true
//        if let mapView = self.mapView {
//            mapView.delegate = self
//        }
//        if #available(iOS 11.0, *) {
//            mapView.register(EventMapView.self,
//                             forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
//        } else {
//            // Fallback on earlier versions
//        }

       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
         self.AddressApi()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
//    @IBAction func shareAddressButtonTapped(_ sender: UIButton) {
//
//        var shareArray = [Any]()
//        if UIApplication.shared.canOpenURL(URL.init(string: "comgooglemaps://")!) {
//            shareArray.append(URL.init(string: "comgooglemapsurl://maps.google.com/?q=\(self.eventLocation.coordinate.latitude),\(self.eventLocation.coordinate.longitude)")!)
//        } else {
//            shareArray.append(URL.init(string: "https://maps.google.com/?q=\(self.eventLocation.coordinate.latitude),\(self.eventLocation.coordinate.longitude)")!)
//        }
//        let activityController = UIActivityViewController(activityItems: shareArray, applicationActivities: nil)
//
//        self.present(activityController, animated: true, completion: nil)
//    }
    
    
    func setUpTimerLabel() {
        let timer = Timer.scheduledTimer(timeInterval: 1,
                                         target: self,
                                         selector: #selector(self.updateTime),
                                         userInfo: nil,
                                         repeats: true)
        RunLoop.current.add(timer, forMode: .common)
    }
    
    @objc func updateTime() {
        self.titleLabel.text = getElapsedTimefromDate(startTime: eventData?.start_time, endTime: eventData?.end_time)
    }
    
//    MARK:-  table view Delegate and DataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (eventLocation.count > 0) ? eventLocation.count : 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! AddressTableCell
        cell.nameLbl.text = eventLocation[indexPath.row].name
        cell.addressLbl.text = eventLocation[indexPath.row].title
        cell.addressDescriptionLbl.text = eventLocation[indexPath.row].locationName
        cell.copyAddressBtn.addTarget(self, action: #selector(copyAddressBtnTapped), for: .touchUpInside)
        cell.copyAddressBtn.tag = indexPath.row
        cell.shareAddressBtn.addTarget(self, action: #selector(shareAddressBtnTapped), for: .touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let center = CLLocationCoordinate2D(latitude: self.eventLocation[indexPath.row].coordinate.latitude, longitude: self.eventLocation[indexPath.row].coordinate.longitude)
        self.mapView.setCenter(center, zoomLevel: 7, direction: 0, animated: false)
        if let loc = locationManager.location?.coordinate{
                    calculateRoute(from: (mapView.userLocation?.coordinate)!, to: (self.eventLocation[indexPath.row].coordinate)) { (route, error) in
                        if error != nil {
                            print("Error calculating route")
                        }
                    }
        }else{
            let settingsUrl = NSURL(string: UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.openURL(url as URL)
            }
        }
        

    }
//    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        let coordinate = CLLocationCoordinate2D(latitude: 52.520007, longitude: 13.404954)
//        shareLocation(coordinate: coordinate)
//        let vCardURL = LocationVCard.vCardUrl(from: coordinate, with: "Berlin")
//        let activityViewController = UIActivityViewController(activityItems: [vCardURL], applicationActivities: nil)
//        present(activityViewController, animated: true, completion: nil)

//    }
    @objc func copyAddressBtnTapped(_ sender : UIButton){
        UIPasteboard.general.string =  "Name : \(eventLocation[sender.tag].name!) \n Location : \(eventLocation[sender.tag].title!) \n Description : \(eventLocation[sender.tag].locationName) "
          self.view.makeToast("Copied".localized, duration: 2.0, position: CSToastPositionCenter)
    }
    @objc func shareAddressBtnTapped(_ sender : UIButton){
        guard let cachesPathString = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first else {
            print("Error: couldn't find the caches directory.")
            return
        }
        
        let vCardString = [
            "BEGIN:VCARD",
            "VERSION:3.0",
            "N:;Shared Location;;;",
            "FN:Shared Location",
            "item1.URL;type=pref:http://maps.apple.com/?ll=\(eventLocation[sender.tag].coordinate.latitude),\(eventLocation[sender.tag].coordinate.longitude)",
            "item1.X-ABLabel:map url",
            "END:VCARD"
            ].joined(separator: "\n")
        
        let vCardFilePath = (cachesPathString as NSString).appendingPathComponent("vCard.loc.vcf")
        
        do {
            try vCardString.write(toFile: vCardFilePath, atomically: true, encoding: String.Encoding.utf8)
            let activityViewController = UIActivityViewController(activityItems: [NSURL(fileURLWithPath: vCardFilePath)], applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print,
                                                            UIActivity.ActivityType.copyToPasteboard,
                                                            UIActivity.ActivityType.assignToContact,
                                                            UIActivity.ActivityType.saveToCameraRoll,
                                                            UIActivity.ActivityType.postToVimeo,
                                                            UIActivity.ActivityType.postToTencentWeibo,
                                                            UIActivity.ActivityType.postToWeibo]
            present(activityViewController, animated: true, completion: nil)
        } catch let error {
            print("Error, \(error), saving vCard: \(vCardString) to file path: \(vCardFilePath).")
        }
    }
    
    func shareLocation(coordinate:CLLocationCoordinate2D) {
        
        guard let cachesPathString = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first else {
            print("Error: couldn't find the caches directory.")
            return
        }
        
        let vCardString = [
            "BEGIN:VCARD",
            "VERSION:3.0",
            "N:;Shared Location;;;",
            "FN:Shared Location",
            "item1.URL;type=pref:http://maps.apple.com/?ll=\(coordinate.latitude),\(coordinate.longitude)",
            "item1.X-ABLabel:map url",
            "END:VCARD"
            ].joined(separator: "\n")
        
        let vCardFilePath = (cachesPathString as NSString).appendingPathComponent("vCard.loc.vcf")
        
        do {
            try vCardString.write(toFile: vCardFilePath, atomically: true, encoding: String.Encoding.utf8)
            let activityViewController = UIActivityViewController(activityItems: [NSURL(fileURLWithPath: vCardFilePath)], applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print,
                                                            UIActivity.ActivityType.copyToPasteboard,
                                                            UIActivity.ActivityType.assignToContact,
                                                            UIActivity.ActivityType.saveToCameraRoll,
                                                            UIActivity.ActivityType.postToVimeo,
                                                            UIActivity.ActivityType.postToTencentWeibo,
                                                            UIActivity.ActivityType.postToWeibo]
            present(activityViewController, animated: true, completion: nil)
        } catch let error {
            print("Error, \(error), saving vCard: \(vCardString) to file path: \(vCardFilePath).")
        }
    }
    
    func AddressApi() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {

            self.showHUD(forView: self.view, excludeViews: [])
        _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(eventId!)/venues?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .get) { (netResponse) -> (Void) in
            
//            CallApi.getData(url: "http://eu.aio-events.com/api/events/\(eventId!)/access?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .get) { (netResponse) -> (Void) in
            
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSONArray = netResponse.responseDict as? NSArray {
                        var i = 0
                        self.eventLocation.removeAll()
                        for JSON in JSONArray{
                            let event = EventLocation.init(json: JSON as! NSDictionary)
                            self.eventLocation.append(event)
                            self.addAnotationsOnMap(eventData: self.eventLocation[i])
                            i = i + 1
                        }

//                        self.address_out.text = self.eventLocation[0].locationName
//                        self.address_head.text = self.eventLocation[1].locationName

                        self.mapView.delegate = self
                        self.mapView.showsUserLocation = true
                        self.mapView.setUserTrackingMode(.follow, animated: true)

                        self.tbleView.delegate = self
                        self.tbleView.dataSource = self
                        self.tbleView.reloadData()
//                        let center = CLLocationCoordinate2D(latitude: self.eventLocation[0].coordinate.latitude, longitude: self.eventLocation[0].coordinate.longitude)
//                            self.mapView.setCenter(center, zoomLevel: 7, direction: 0, animated: true)
//                        self.calculateRoute(from: (self.locationManager.location?.coordinate)!, to: (self.eventLocation[0].coordinate)) { (route, error) in
//                            if error != nil {
//                                print("Error calculating route")
//                            }
//                        }
//                        self.hideHUD(forView: self.view)
//                    }else{
//                        self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                    }
                    self.hideHUD(forView: self.view)
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                    self.hideHUD(forView: self.view)
                }
                self.hideHUD(forView: self.view)
            }
        }
        
    }
    
    func addAnotationsOnMap(eventData: EventLocation){
        let eventMarker = MGLPointAnnotation()
        eventMarker.coordinate = CLLocationCoordinate2D(latitude: eventData.coordinate.latitude, longitude: eventData.coordinate.longitude)
        eventMarker.title =  eventData.title
        eventMarker.subtitle = eventData.locationName
        
        // Add marker `eventMarker` to the map.
        mapView.addAnnotation(eventMarker)
    }

    //    Mark: MAP box Delegate methods
    func mapView(_ mapView: MGLMapView, tapOnCalloutFor annotation: MGLAnnotation) {
        
        if  let sLatitude = mapView.userLocation?.coordinate.latitude{
            NavigateToRouteMap(annotation.coordinate)
        }else{
            let settingsUrl = NSURL(string: UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.openURL(url as URL)
            }
        }

        print("MAP")
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        // Always allow callouts to popup when annotations are tapped.
        return true
    }
    var directionsRoute: Route?

    func NavigateToRouteMap(_ cords : CLLocationCoordinate2D){
//
//        let sLatitude = (locationManager.location?.coordinate.latitude)!
//        let sLongitude = (locationManager.location?.coordinate.longitude)!

        guard let route = directionsRoute else { return }
        let viewController = NavigationViewController(for: route)
        self.present(viewController, animated: true, completion: nil)
    }
    
        func calculateRoute(from origin: CLLocationCoordinate2D,
                            to destination: CLLocationCoordinate2D,
                            completion: @escaping (Route?, Error?) -> ()) {
            let sLatitude =  (mapView.userLocation?.coordinate.latitude)!
//                (locationManager.location?.coordinate.latitude)!
            let sLongitude =  (mapView.userLocation?.coordinate.longitude)!
            
//                (locationManager.location?.coordinate.longitude)!
            
           
//             let origin = Waypoint(coordinate: CLLocationCoordinate2D(latitude: sLatitude, longitude: sLongitude), name: "Source")
//              let destination =  Waypoint(coordinate: CLLocationCoordinate2D(latitude: 31.222905, longitude: 121.445386), name: "Destination")
        
//            let origin = Waypoint(coordinate: CLLocationCoordinate2D(latitude: 38.9131752, longitude: -77.0324047), name: "Mapbox")
//            let destination =  Waypoint(coordinate: CLLocationCoordinate2D(latitude: 38.8977, longitude: -77.0365), name: "White House")
            
            
            let origin = Waypoint(coordinate: origin, coordinateAccuracy: -1, name: "Start")
            let destination = Waypoint(coordinate: destination, coordinateAccuracy: -1, name: "Finish")
//
            // Specify that the route is intended for automobiles avoiding traffic
            let options = NavigationRouteOptions(waypoints: [origin, destination], profileIdentifier: .automobile)
//            let options = RouteOptions(waypoints: [origin, destination], profileIdentifier: .automobileAvoidingTraffic)
//            options.includesSteps = true
            
            // Generate the route object and draw it on the map
            _ = Directions.shared.calculate(options) { [unowned self] (waypoints, routes, error) in
                    guard let route = routes?.first else { return }
                print(route)
                self.directionsRoute = routes?.first
//                let leg = route.legs.first
//                let distanceFormatter = LengthFormatter()
//                let formattedDistance = distanceFormatter.string(fromMeters: route.distance)
//
//                let travelTimeFormatter = DateComponentsFormatter()
//                travelTimeFormatter.unitsStyle = .short
//                let formattedTravelTime = travelTimeFormatter.string(from: route.expectedTravelTime)
//
//                print("Distance: \(formattedDistance); ETA: \(formattedTravelTime!)")
//
//                for step in (leg?.steps)! {
//                    print("\(step.instructions)")
//                    let formattedDistance = distanceFormatter.string(fromMeters: step.distance)
//                    print("— \(formattedDistance) —")
//                }
//
                
                
                // Draw the route on the map after creating it
                self.drawRoute(route: self.directionsRoute!)
            }
        }
    
    func drawRoute(route: Route) {
        guard route.coordinateCount > 0 else { return }
        // Convert the route’s coordinates into a polyline
        var routeCoordinates = route.coordinates!
        let polyline = MGLPolylineFeature(coordinates: &routeCoordinates, count: route.coordinateCount)
        
        // If there's already a route line on the map, reset its shape to the new route
        if let source = mapView.style?.source(withIdentifier: "route-source") as? MGLShapeSource {
            source.shape = polyline
        } else {
            let source = MGLShapeSource(identifier: "route-source", features: [polyline], options: nil)
            
            // Customize the route line color and width
            let lineStyle = MGLLineStyleLayer(identifier: "route-style", source: source)
            lineStyle.lineColor = NSExpression(forConstantValue: #colorLiteral(red: 0.1897518039, green: 0.3010634184, blue: 0.7994888425, alpha: 1))
            lineStyle.lineWidth = NSExpression(forConstantValue: 3)
            self.mapView.style?.addSource(source)
            self.mapView.style?.addLayer(lineStyle)
            // Add the source and style layer of the route line to the map

        }
    }
    
    
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! EventLocation
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
  
}
class AddressTableCell : UITableViewCell{
    @IBOutlet weak var addressLbl : UILabel!
    @IBOutlet weak var nameLbl : UILabel!
    @IBOutlet weak var addressDescriptionLbl: UILabel!
    @IBOutlet weak var copyAddressBtn : UIButton!
    @IBOutlet weak var shareAddressBtn: UIButton!
}

