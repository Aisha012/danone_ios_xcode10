//
//  ContactListPagination.swift
//  Danone
//
//  Created by Renendra Saxena on 07/01/19.
//  Copyright © 2019 Zaman Meraj. All rights reserved.
//

import Foundation
class ContactListPagination {
    
    var contactList: [ContactsList] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool = false
    
    init() {
        contactList = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    init(array: [NSDictionary]) {
        for dict in array {
            let contact = ContactsList(responseDict: dict)
            self.contactList.append(contact)
        }
        
        hasMoreToLoad = self.contactList.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: ContactListPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.contactList = [];
            self.contactList = newPaginationObject.contactList
        case .old: break
            self.contactList += newPaginationObject.contactList
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}

