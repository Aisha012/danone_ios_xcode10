//
//  SpeakersTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 18/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

protocol SpeakersTableViewCellDelegate {
    func facebookButtonTappedAtIndex(_ index: Int)
    func linkedInButtonTappedAtIndex(_ index: Int)
    func googleButtonTappedAtIndex(_ index: Int)
    func twitterButtonTappedAtIndex(_ index: Int)
    func likeButtonTapped(_ index: Int)
}

class SpeakersTableViewCell: UITableViewCell {

    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var delegate: SpeakersTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "SpeakersTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addShadowToView(self.holderView)
    }
    
    func addShadowToView(_ holder: UIView) {
        holder.layer.shadowOffset = CGSize(width: 0, height: 0)
        holder.layer.shadowColor = UIColor.black.cgColor
        holder.layer.shadowRadius = 4
        holder.layer.cornerRadius = 5
        holder.layer.shadowOpacity = 0.3
        holder.layer.masksToBounds = false
        holder.layer.rasterizationScale = UIScreen.main.scale
        holder.clipsToBounds = false
    }

    func configureCell(speaker: SpeakersModal, index: Int) {
        speakerImageView.sd_setShowActivityIndicatorView(true)
        speakerImageView.sd_setIndicatorStyle(.gray)
        speakerImageView.sd_setImage(with: URL(string: speaker.avatar ?? ""), placeholderImage: nil, options: .progressiveDownload, completed: nil)
        var title = ""
        if let speakerTitle = speaker.title, !speakerTitle.isEmpty {
            title += speakerTitle
        }
        if let speakerSub = speaker.subTitle, !speakerSub.isEmpty {
            title += "\n" + speakerSub
        }
        let name = speaker.name ?? ""
        
        let finalString = name + "\n" + title
        let attrString = NSMutableAttributedString(string: finalString)
        attrString.addAttributes([NSAttributedString.Key.font: UIFont.monospacedDigitSystemFont(ofSize: 16.0, weight: .medium)], range: NSMakeRange(0, name.count))
//            NSMakeRange(name.count, title.count + name.count))
        nameLabel.attributedText = attrString

    }

}
