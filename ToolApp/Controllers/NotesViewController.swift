//
//  NotesViewController.swift
//  ToolApp
//
//  Created by Phaninder on 06/05/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import UITextView_Placeholder

protocol NotesViewControllerDelegate {
    func notesUpdatedWithText(_ notes: String)
}

class NotesViewController: BaseClassVC {

    @IBOutlet weak var notesTextView: TextViewAutoHeight!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    
    var showMenuButton = false
    var eventId: String?
    var lisModal: AgendaListModal?
    var agendaId: String!
    var notesDelegate: NotesViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        notesTextView.placeholder = "Write your notes here"
//        notesTextView.layer.cornerRadius = 3.0
//        notesTextView.layer.borderColor = UIColor.darkGray.cgColor
//        notesTextView.layer.borderWidth = 1.0

        notesTextView.text = lisModal?.notes ?? ""
        self.titleLabel.text = "Notes".localized

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }

    @IBAction func saveButtonTapped(_ sender: UIButton) {
        saveNotes()
    }
    
    func saveNotes() {
        let agendaId = String((lisModal?.agendaListId)!)
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            _ = self.showHUD(forView: self.view, excludeViews: [])
            
            let param: NSDictionary = ["note" : notesTextView.text]
            
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/\(agendaId)?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: param as? Dictionary<String, Any>, type: .put) { (netResponse) -> (Void) in
                DispatchQueue.main.async {
                    self.hideHUD(forView: self.view)
                    
                    if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                        
                        if let _ = netResponse.responseDict as? NSDictionary {
                            self.notesDelegate?.notesUpdatedWithText(self.notesTextView.text)
                            self.view.makeToast("Saved".localized, duration: 2.0, position: CSToastPositionCenter)
                        }
                    } else {
                        self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                    }
                }
            }
        }
    }

}
