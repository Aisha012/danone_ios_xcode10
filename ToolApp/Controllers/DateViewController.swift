//
//  DateViewController.swift
//  ToolApp
//
//  Created by Phaninder on 06/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class DateViewController: BaseClassVC, IndicatorInfoProvider {

    @IBOutlet weak var tableView: UITableView!
    
    var completeModal = [CompleteAgendaModal]()
    var eventId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        self.completeModal = completeModal.sorted { $0.date! > $1.date! }

    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Date".localized)
    }

}

extension DateViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return completeModal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckableTableViewCell",
                                                 for: indexPath) as! CheckableTableViewCell
//        if indexPath.row == 0 {
//            cell.textLabel?.text = "Today".localized
//        } else {
            cell.textLabel?.text = completeModal[indexPath.row].date

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell =  tableView.cellForRow(at: indexPath) as! CheckableTableViewCell
        var date = ""
        if cell.isSelected {
            tableView.deselectRow(at: indexPath, animated: true)
            let userInfo = ["data": date];
            NotificationCenter.default.post(name: .dateSelected, object: nil, userInfo:userInfo)
            
            return nil
        } else {
            date = (cell.textLabel?.text!)!
            let userInfo = ["data": date];
            NotificationCenter.default.post(name: .dateSelected, object: nil, userInfo:userInfo)
            return indexPath
        }
        
    }

}
