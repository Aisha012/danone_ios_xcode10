//
//  ParticipantPagination.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class ParticipantListPagination {
    
    var participants: [Participant] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool = false
    
    init() {
        participants = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    init(array: [NSDictionary]) {
        for dict in array {
            let participant = Participant(responseDict: dict)
            self.participants.append(participant)
        }
        
        hasMoreToLoad = self.participants.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: ParticipantListPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.participants = [];
            self.participants = newPaginationObject.participants
        case .old:
            self.participants += newPaginationObject.participants
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}

