//
//  EventData+CoreDataProperties.swift
//  ToolApp
//
//  Created by mayank on 31/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//
//

import Foundation
import CoreData


extension EventData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<EventData> {
        return NSFetchRequest<EventData>(entityName: "EventData")
    }

    @NSManaged public var agendas: String?
    @NSManaged public var app_color: String?
    @NSManaged public var app_token: String?
    @NSManaged public var event_description: String?
    @NSManaged public var event_id: String?
    @NSManaged public var event_name: String?
    @NSManaged public var gallery: String?
    @NSManaged public var is_floor_plan: String?
    @NSManaged public var logo: String?
    @NSManaged public var speakers: String?
    @NSManaged public var sponsors: String?
    @NSManaged public var start_time: String?
    @NSManaged public var end_time: String?
    @NSManaged public var youtube: String?
    @NSManaged public var floor_plan: String?
    @NSManaged public var document: String?
    @NSManaged public var chat: String?
    @NSManaged public var checkin: String?
    @NSManaged public var allow_share: String?
    @NSManaged public var allow_upload: String?
    @NSManaged public var eventSurvey: String?

}
