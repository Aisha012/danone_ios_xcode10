//
//  DashboardVC.swift
//  ToolApp
//
//  Created by Zaman Meraj on 26/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import SDWebImage
import ASPVideoPlayer
import SafariServices

class DashboardVC: BaseClassVC, SWRevealViewControllerDelegate {

    @IBOutlet weak var menuCollection: UICollectionView!
    @IBOutlet weak var myCOntentView: UIView!
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var menuButton: UIButton!
//    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var dropDownArrow: UIImageView!

    
    var offSet: CGFloat = 0
    let cellId = "DashboardVCCell"
    let headerId = "DashboardVCHeader"
    var tilesArray = ["agenda", "access", "Event", "sponsors", "delegates", "speaker", "gallery", "CheckIn"]
    var eventData: EventData?
    var showTiles = [String]()
    var tileImage = [String]()
    var headerImage = [String]()
    var videoURL = ""
    var eventId: String?
    let videoPlayer = ASPVideoPlayerView()
    var currentUser: LoginData!
    var pages: [PageData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utilities.shared.registerDeviceToken()

        let revealViewController = self.revealViewController()
        menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())

        let userID = UserDefaults.standard.value(forKey: "UUID") as! String
        let loginData = MyDatabase.matchProgramData(tableName: "LoginData", tableId: userID
            , columnName: "user_id") as? [LoginData]
        currentUser = loginData![0]
        
        notificationButton.setTitle("Notifications".localized, for: .normal)
        
        let imageName = "drop-down-arrow"
        let originalImage = UIImage(named: imageName)
        let templateImage = originalImage!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        dropDownArrow.image = templateImage
        dropDownArrow.tintColor = UIColor.white
        
        UserDefaults.standard.set(self.eventId!, forKey: "EventId")
        self.setCollectionViewDatasource()
        self.fetchData()
        self.setUpApp()
        self.setScrollView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .notificationCountChanged, object: nil)
    }

    func setUpTimerLabel() {
//        let timer = Timer.scheduledTimer(timeInterval: 1,
//                             target: self,
//                             selector: #selector(self.updateTime),
//                             userInfo: nil,
//                             repeats: true)
//        RunLoop.current.add(timer, forMode: .commonModes)
    }
    
//    @objc func notificationChanged() {
//        DispatchQueue.main.async {
//            let badgeNumber = UserDefaults.standard.integer(forKey: BadgeNumber)
//            if badgeNumber == 0 {
//                self.notificationButton.setImage(UIImage(named:"notification"), for: .normal)
//            } else {
//                self.notificationButton.setImage(UIImage(named:"notification_red"), for: .normal)
//            }
//
//        }
//    }

//    @objc func updateTime() {
//        self.titleLabel.text = getElapsedTimefromDate(startTime: eventData?.start_time, endTime: eventData?.end_time)
//    }
    
    func setCollectionViewDatasource() {
        self.menuCollection.dataSource = self
        self.menuCollection.delegate    = self
        self.menuCollection.register(UINib.init(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
        self.menuCollection.register(UINib.init(nibName: cellId, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        if let layout = self.menuCollection.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.minimumInteritemSpacing = 1
            layout.minimumLineSpacing      = 1
        }
    }
    
    func fetchData() {
        let eventsData = MyDatabase.matchProgramData(tableName: "EventData", tableId: self.eventId!, columnName: "event_id") as? [EventData]
        pages = MyDatabase.matchProgramData(tableName: "PageData", tableId: self.eventId!, columnName: "event_id") as? [PageData]
        guard let banners = MyDatabase.matchProgramData(tableName: "BannerData", tableId: self.eventId!, columnName: "event_id") as? [BannerData] else { return }
        UserDefaults.standard.set(self.eventId!, forKey: "Event_Id")
        if banners.count > 0 {
            for (index, banner) in banners.enumerated() {
                let bannerImageURL = banner.banner_image!
                let bannerVideoURL = banner.banner_video!
                if index == 0, !bannerVideoURL.isEmpty {
                    self.videoURL = bannerVideoURL
                    break
                } else if bannerVideoURL.isEmpty {
                    self.headerImage.append(bannerImageURL)
                }
            }
        }
        
        if let eventsData = eventsData {
            let eventData = eventsData[0]
            self.eventData = eventData
//            self.setUpTimerLabel()
            if eventData.agendas == "1" {
                self.showTiles.append("Agenda".localized)
                self.tileImage.append("tile_agenda")
            }
            self.showTiles.append("Venue".localized)
            self.tileImage.append("tile_access")
            if eventData.is_floor_plan == "1" {
                self.showTiles.append("Event FloorPlan".localized)
                self.tileImage.append("tile_eventfloorplan")
            }
            if eventData.sponsors ==  "1"{
                self.showTiles.append("Partners".localized)
                self.tileImage.append("tile_partners")
            }
            if eventData.speakers == "1" {
                self.showTiles.append("Speakers".localized)
                self.tileImage.append("tile_speakers")
            }
            if eventData.gallery == "1" {
                self.showTiles.append("Gallery".localized)
                self.tileImage.append("tile_gallery")
            }
//            let isAllowed = Utilities.hasPermissionsForCheckIn()
            
            if eventData.checkin == "1" {
                self.showTiles.append("Check In".localized)
                self.tileImage.append("tile_checkIn")
            }
            if eventData.document == "1" {
                self.showTiles.append("Documents".localized)
                self.tileImage.append("tile_documents")
            }
            if eventData.chat == "1" {
//                self.showTiles.append("Delegates".localized)
                self.showTiles.append("Directory".localized)
                self.tileImage.append("tile_delegates")
            }
            if eventData.chat == "1" {
                self.showTiles.append("Main Contact".localized)
                self.tileImage.append("Contact")
            }
            
            if let surveyLink = eventData.eventSurvey, !surveyLink.isEmpty {
                self.showTiles.append("Survey".localized)
                self.tileImage.append("tile_survey")
            }
            if let pagesData = pages, pagesData.count > 0 {
                self.showTiles.append("More".localized)
                self.tileImage.append("tile_info")
            }
            
            self.menuCollection.reloadData()
        }
    }
    
    func setUpApp() {
        let colorValue: UIColor!
        if let appColor = self.eventData?.app_color {
            colorValue = UIColor.colorWithHexString(hex: appColor)
            UserDefaults.standard.set(appColor, forKey: "color_Value")
        }else{
            colorValue = UIColor.colorWithHexString(hex: "#00C3E7")
            UserDefaults.standard.set("#00C3E7", forKey: "color_Value")
        }
//        self.view.backgroundColor = colorValue
        customNavBar.backgroundColor = navigationBarColor()
//        bottomView.backgroundColor = navigationBarColor()
    }
    
    func setScrollView() {
        
        self.offSet = 0
        if !self.videoURL.isEmpty {
            
            videoPlayer.videoURL = URL(string: self.videoURL)
            videoPlayer.frame = CGRect.init(x: 0, y: imageScrollView.frame.origin.y, width: ScreenWidth, height: imageScrollView.frame.size.height)
            videoPlayer.shouldLoop = true
            videoPlayer.backgroundColor = UIColor.lightGray
            imageScrollView.addSubview(videoPlayer)
            
            videoPlayer.readyToPlayVideo = {
                self.videoPlayer.playVideo()
            }
            videoPlayer.volume = 0
            videoPlayer.startedVideo = {
                print("Video has started playing.")
            }
        } else {
            _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
            for (index, image) in headerImage.enumerated() {
                let image = image
                let imageView = UIImageView()
                imageView.contentMode = .scaleAspectFill
                imageView.clipsToBounds = true
                guard let url = URL(string: image) else { return }
                imageView.sd_setImage(with: url, placeholderImage: nil, options: .progressiveDownload, progress: nil, completed: nil)
                imageView.frame = CGRect.init(x: 0, y: imageScrollView.frame.origin.y, width: ScreenWidth, height: imageScrollView.frame.size.height)

                imageView.frame.origin.x = CGFloat(index) * self.view.bounds.size.width
                    imageScrollView.addSubview(imageView)
            }
        }
        
    }
    
    @objc func autoScroll() {
        let totalPossibleOffset = CGFloat(headerImage.count - 1) * self.view.bounds.size.width
        if offSet == totalPossibleOffset {
            offSet = 0 // come back to the first image after the last image
        }
        else {
            offSet += self.view.bounds.size.width
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.imageScrollView.contentOffset.x = CGFloat(self.offSet)
            }, completion: nil)
        }
    }

    @IBAction func notificationsButtonTapped(_ sender: UIButton) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
        let eventsData = MyDatabase.matchProgramData(tableName: "EventData", tableId: self.eventId!, columnName: "event_id") as? [EventData]

        destination.eventData = eventsData![0]
        destination.showMenuButton = false
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    
    func fetchGalleryCategories() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            _ = self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(eventId!)/galleries?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    self.hideHUD(forView: self.view)
                    DispatchQueue.main.async {
                        if let JSON = netResponse.responseDict as? [NSDictionary] {
                            var categories = [GalleryCategory]()
                            for category in JSON {
                                let galleryCategory = GalleryCategory.init(responseDict: category)
                                categories.append(galleryCategory)
                            }
                            let eventsData = MyDatabase.matchProgramData(tableName: "EventData", tableId: self.eventId!, columnName: "event_id") as? [EventData]
                            
                            let destination = self.storyboard?.instantiateViewController(withIdentifier: "GalleryCategoryViewController") as! GalleryCategoryViewController
                            destination.categories = categories
                            destination.eventData = eventsData![0]
                            destination.showMenuButton = false
                            self.navigationController?.pushViewController(destination, animated: true)
                        }
                    }
                }
            }
        }
    }

    
    
}

extension DashboardVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.showTiles.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! DashboardVCCell
        
        cell.tileText.text = self.showTiles[indexPath.row]
//            .uppercased()
        cell.tileText.textColor = navigationBarColor()
        
        let imageName = self.tileImage[indexPath.row]
        let originalImage = UIImage(named: imageName)
        let templateImage = originalImage!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        cell.tileImage.image = templateImage
        cell.tileImage.tintColor = navigationBarColor()

//        cell.tileImage.image = UIImage(named: imageName)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath)

        self.imageScrollView.frame = header.frame
        self.setScrollView()
        header.addSubview(self.imageScrollView)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellLength = (self.menuCollection.frame.width/3)
        return CGSize(width: cellLength, height: cellLength)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    //    - (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//    {
//    return 10; // This is the minimum inter item spacing, can be more
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let titleText = self.showTiles[indexPath.row]
        self.btnTapped(text: titleText)
    }
    
    func btnTapped(text: String) {
        let eventsData = MyDatabase.matchProgramData(tableName: "EventData", tableId: self.eventId!, columnName: "event_id") as? [EventData]
        switch text {
            
        case "Speakers".localized:
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "Spekers_VC") as! Spekers_VC
            destination.eventData = eventsData![0]
            let eventData = eventsData![0]
            destination.showMenuButton = false
            destination.app_token  = eventData.app_token!
            self.navigationController?.pushViewController(destination, animated: true)

        case "Partners".localized:
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "Delegates_Vc") as! Delegates_Vc
            destination.eventData = eventsData![0]
            destination.showMenuButton = false
            self.navigationController?.pushViewController(destination, animated: true)

        case "Event FloorPlan".localized:
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "MultiFloorPlanViewController") as! MultiFloorPlanViewController
            destination.eventData =   eventsData![0]
            destination.showMenuButton = false
            self.navigationController?.pushViewController(destination, animated: true)

        case "Gallery".localized:
            fetchGalleryCategories()
        case "Venue".localized:
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "AddressVC") as! AddressVC
            destination.eventData = eventsData![0]
            destination.showMenuButton = false
            self.navigationController?.pushViewController(destination, animated: true)

        case "Agenda".localized:
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "AgendaListViewController") as! AgendaListViewController
            destination.eventData = eventsData![0]
            destination.showMenuButton = false
            self.navigationController?.pushViewController(destination, animated: true)
            
        case "Check In".localized:
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "CheckInVc") as! CheckInVc
            destination.eventData = eventsData![0]
            destination.showMenuButton = false
            self.navigationController?.pushViewController(destination, animated: true)
          
        case "Directory".localized:
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "ConversationViewController") as! ConversationViewController
            destination.showMenuButton = false
            destination.eventData = eventsData![0]
            self.navigationController?.pushViewController(destination, animated: true)
        case "More".localized:
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "PagesViewController") as! PagesViewController
            destination.showMenuButton = false
            destination.pages = pages
            self.navigationController?.pushViewController(destination, animated: true)
        case "Documents".localized:
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "DocumentListViewController") as! DocumentListViewController
            destination.showMenuButton = false
            destination.eventData = eventsData![0]
            self.navigationController?.pushViewController(destination, animated: true)
        case "Survey".localized:
            if let surveyLink = eventsData![0].eventSurvey, !surveyLink.isEmpty, let url = URL(string: surveyLink) {
                if #available(iOS 11.0, *) {
                    let config: SFSafariViewController.Configuration = SFSafariViewController.Configuration.init()
                    config.entersReaderIfAvailable = true
                    let vc = SFSafariViewController(url: url, configuration: config)
                    present(vc, animated: true, completion: nil)
                } else {
                    let safariController = SFSafariViewController(url: url)
                    self.present(safariController, animated: true, completion: nil)
                }
            }
            
        case "Main Contact".localized :
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "MainContactVC") as! MainContactVC
            destination.showMenuButton = false
            destination.eventData = eventsData![0]
            self.navigationController?.pushViewController(destination, animated: true)
            
            
        default:
            break
            
            
        }
        
    }

    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        if position == FrontViewPosition.right {
            revealController.frontViewController.view.isUserInteractionEnabled = false
        } else {
            revealController.frontViewController.view.isUserInteractionEnabled = true
        }
    }
}





