//
//  MultiFloorPlanViewController.swift
//  ToolApp
//
//  Created by Phaninder on 04/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class MultiFloorPlanViewController: BaseClassVC {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var tableView: UITableView!

    var showMenuButton = true
    var eventId: String!
    var floorPlans = [NSDictionary]()
    var eventData: EventData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id!

        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        self.titleLabel.text = "Event FloorPlan".localized
        self.tableView.tableFooterView = UIView()
        fetchFloorPlans()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }

    func fetchFloorPlans() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            let urlString = "http://eu.aio-events.com/api/events/\(eventId!)/floor_plans?user_token=\(auth_Token)&api_token=\(apiToken)"
            _ = CallApi.getData(url: urlString, parameter: nil, type: .get) { (netResponse) -> (Void)
                in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        self.floorPlans = JSON
                        self.tableView.reloadData()
                    } else {
                        self.tableView.reloadData()
                    }
                    self.hideHUD(forView: self.view)
                } else {
                    self.hideHUD(forView: self.view)
                    self.tableView.reloadData()
                }
            }
        }
    }

}

extension MultiFloorPlanViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return floorPlans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let floorPlan = floorPlans[indexPath.row]
        cell.textLabel?.text = floorPlan.value(forKey: "name") as? String ?? "No Title"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let floorPlan = floorPlans[indexPath.row]
        guard let link = floorPlan.value(forKey: "image_url") as? String else {
            return
        }
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "Event_Web_VC") as! Event_Web_VC
        destination.link = link
        destination.titleString = floorPlan.value(forKey: "name") as! String
        destination.showMenuButton = false
        self.navigationController?.pushViewController(destination, animated: true)
    }
}
