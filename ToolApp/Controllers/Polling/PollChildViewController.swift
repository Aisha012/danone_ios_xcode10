//
//  PollChildViewController.swift
//  ToolApp
//
//  Created by Phaninder on 17/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PollChildViewController: BaseClassVC, IndicatorInfoProvider {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var questionLabel: UILabel!
    
    var pageIndex: Int!
    var poll: Poll!
    var selectedIndex: Int!
    var polls: [Poll]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        questionLabel.text = poll.question.question!
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let userInfo = ["data": pageIndex];
        NotificationCenter.default.post(name: .questionChanged, object: nil, userInfo:userInfo)

    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: String(pageIndex))
    }

}

extension PollChildViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return poll.options.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < poll.options.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: PollButtonsTableViewCell.cellIdentifier(), for: indexPath) as! PollButtonsTableViewCell
            cell.delegate = self
            
            cell.previousQuestionButton.isHidden = pageIndex == 0
            cell.nextQuestionButton.isHidden = pageIndex == polls.count - 1
            cell.submitButton.isEnabled = !poll.isSubmitted
            cell.submitButton.setTitleColor(poll.isSubmitted ? UIColor.lightGray : UIColor.black, for: .normal)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: PollOptionTableViewCell.cellIdentifier(), for: indexPath) as! PollOptionTableViewCell
        let option = poll.options[indexPath.row]
        var selected = false
        if poll.isSubmitted == true,
            let selectedOptionId = poll.selectedOptionId,
            String(selectedOptionId) == option.id {
            selectedIndex = indexPath.row
            selected = true
        }
        if let selectedRow = selectedIndex, selectedRow == indexPath.row {
            selected = true
        }
        cell.configureCell(option: option, isSelected: selected, isAlreadyAnswered: poll.isSubmitted)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row < poll.options.count else {
            return 60.0
        }
        tableView.estimatedRowHeight = 150.0
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        guard !poll.isSubmitted else {
            return nil
        }
        selectedIndex = indexPath.row
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.reloadData()
    }
}

extension PollChildViewController: PollButtonsTableViewCellDelegate {
    
    func submitButtonTapped() {
        self.poll.isSubmitted = true
        self.poll.selectedOptionId = Int(poll.options[selectedIndex].id)
        self.tableView.reloadData()
        let userInfo = ["questionId": poll.question.id, "answerId": poll.options[selectedIndex].id];
        NotificationCenter.default.post(name: .questionSubmitted, object: nil, userInfo:userInfo)

    }
    
    func previousButtonTapped() {
        let userInfo = ["data": pageIndex - 1];
        NotificationCenter.default.post(name: .directionButtonTapped, object: nil, userInfo:userInfo)

    }
    
    func nextButtonTapped() {
        let userInfo = ["data": pageIndex + 1];
        NotificationCenter.default.post(name: .directionButtonTapped, object: nil, userInfo:userInfo)

    }
    
}
