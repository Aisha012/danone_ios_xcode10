//
//  FilterViewController.swift
//  ToolApp
//
//  Created by Phaninder on 06/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

protocol FilterViewControllerDelegate {

    func filterApplied(_ category: String, room: String, date: String)
}

class FilterViewController: BaseClassVC {

    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    var pagerViewController: FilterCustomPagerViewController!
    var showMenuButton = true
    var completeModal = [CompleteAgendaModal]()
    var eventId: String!
    var selectedCategoryId = ""
    var selectedRoomId = ""
    var selectedDate = ""
    var filterDelegate: FilterViewControllerDelegate?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
        menuButton.setImage(nil, for: .normal)
        backImage.image = UIImage(named: "back.png")
        self.titleLabel.text = "Filter".localized

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(categorySelected(_:)),
                                               name: .categorySelected,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(roomSelected(_:)),
                                               name: .roomSelected,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dateSelected(_:)),
                                               name: .dateSelected,
                                               object: nil)


    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .categorySelected, object: nil)
        NotificationCenter.default.removeObserver(self, name: .roomSelected, object: nil)
        NotificationCenter.default.removeObserver(self, name: .dateSelected, object: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FilterPager" {
            pagerViewController = segue.destination as! FilterCustomPagerViewController
            pagerViewController.eventId = self.eventId
            pagerViewController.completeModal = self.completeModal
        }
    }

    @IBAction func doneButtonTapped(_ sender: UIButton) {
        filterDelegate?.filterApplied(selectedCategoryId, room: selectedRoomId, date: selectedDate)
        navigateBack()
    }
    
    @objc func categorySelected(_ notification: NSNotification) {
        if let categoryId = notification.userInfo?["data"] as? String {
            selectedCategoryId = categoryId
        }
    }

    @objc func roomSelected(_ notification: NSNotification) {
        if let roomId = notification.userInfo?["data"] as? String {
            selectedRoomId = roomId
        }
    }

    @objc func dateSelected(_ notification: NSNotification) {
        if let date = notification.userInfo?["data"] as? String {
            var selDate = date
            if date == "Today" {
                let todayDate = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                selDate = dateFormatter.string(from: todayDate)
            }
            selectedDate = selDate
        }
    }


}
