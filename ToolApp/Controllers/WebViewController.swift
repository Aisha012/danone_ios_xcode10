//
//  WebViewController.swift
//  ToolApp
//
//  Created by Phaninder on 28/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

protocol LinkedInLoginDelegate {
    func successfullyFetchedAccessToken(_ token: String)
}

class WebViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    let linkedInKey = "81c4hmxjy1ojbf"
    let linkedInSecret = "gc6fL104hAinZb27"
    let authorizationEndPoint = "https://www.linkedin.com/oauth/v2/authorization"
    let accessTokenEndPoint = "https://www.linkedin.com/oauth/v2/accessToken"
    
    var linkedInDelegate: LinkedInLoginDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.startAuthorization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func startAuthorization() {
        let responseType = "code"
//        let redirectURL = "https://com.appcoda.linkedin.oauth/oauth".stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())!
        let redirectURL = "https://com.appcoda.linkedin.oauth/oauth".addingPercentEncoding(withAllowedCharacters: .alphanumerics)!

        let state = "linkedin\(Int(NSDate().timeIntervalSince1970))"
        
        let scope = "r_basicprofile,r_emailaddress"
        
        var authorizationURL = "\(authorizationEndPoint)?"
        authorizationURL += "response_type=\(responseType)&"
        authorizationURL += "client_id=\(linkedInKey)&"
        authorizationURL += "redirect_uri=\(redirectURL)&"
        authorizationURL += "state=\(state)&"
        authorizationURL += "scope=\(scope)"
        
        // logout already logined user or revoke tokens
        logout()
        
        // Create a URL request and load it in the web view.
//        let request = NSURLRequest(URL: NSURL(string: authorizationURL)!)
        let request = URLRequest(url: URL(string: authorizationURL)!)

        webView.loadRequest(request)
    }
    
    func logout(){
        let revokeUrl = "https://api.linkedin.com/uas/oauth/invalidateToken"
        let request = URLRequest(url: URL(string: revokeUrl)!)
        webView.loadRequest(request)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        let url = request.url!
        if url.host == "com.appcoda.linkedin.oauth" {
            if url.absoluteString.range(of: "code") != nil {
                let urlParts = url.absoluteString.components(separatedBy: "?")
                let code = urlParts[1].components(separatedBy: "=")[1]
                
                requestForAccessToken(authorizationCode: code)
            }
            
        } else if url.path.contains("login-cancel") {
            navigationController?.popViewController(animated: true)
        }
        
        return true
    }

    func requestForAccessToken(authorizationCode: String) {
        let grantType = "authorization_code"
        let redirectURL = "https://com.appcoda.linkedin.oauth/oauth".addingPercentEncoding(withAllowedCharacters: .alphanumerics)!
        
        
        // Set the POST parameters.
        var postParams = "grant_type=\(grantType)&"
        postParams += "code=\(authorizationCode)&"
        postParams += "redirect_uri=\(redirectURL)&"
        postParams += "client_id=\(linkedInKey)&"
        postParams += "client_secret=\(linkedInSecret)"
        
        
        // Convert the POST parameters into a NSData object.
        let postData = postParams.data(using: String.Encoding.utf8)
        
        // Initialize a mutable URL request object using the access token endpoint URL string.
        var request = URLRequest(url: URL(string: accessTokenEndPoint)!)
        
        // Indicate that we're about to make a POST request.
        request.httpMethod = "POST"
        
        // Set the HTTP body using the postData object created above.
        request.httpBody = postData
        // Add the required HTTP header field.
        request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")
        
        // Initialize a NSURLSession object.
        let session = URLSession(configuration: .default)
        
        // Make the request.
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            // Get the HTTP status code of the request.
            let statusCode = (response as! HTTPURLResponse).statusCode
            
            if statusCode == 200 {
                // Convert the received JSON data into a dictionary.
                do {
                    let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    let dataDict = dataDictionary as! [String: AnyObject]
                    print("dataDictionary\(dataDict)")
                    let accessToken = dataDict["access_token"] as! String
                    print("START sentData")
                    DispatchQueue.main.async {
                        self.linkedInDelegate?.successfullyFetchedAccessToken(accessToken)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                catch {
                    print("Could not convert JSON data into a dictionary.")
                }
            }else{
                print("cancel clicked")
            }
        }
        
        task.resume()
    }
    
}
