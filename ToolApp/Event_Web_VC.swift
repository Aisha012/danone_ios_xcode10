//
//  Event_Web_VC.swift
//  ToolApp
//
//  Created by Prashant Tiwari on 06/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit


class Event_Web_VC: BaseClassVC {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var web_View: UIWebView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!

    var eventData: EventData!
    var showMenuButton = true
    var link: String!
    var titleString = "Event Floorplan"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())

        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        self.titleLabel.text = titleString
//        setUpTimerLabel()
        
        guard let eventData = eventData else {
            if let url = URL(string: link) {
                let request = URLRequest(url: url);
                self.web_View.loadRequest(request)
                web_View.scalesPageToFit = true;
            }
            return
        }
        
        if let urlVal = URL(string: eventData.floor_plan!){
            let request = URLRequest(url: urlVal);
            self.web_View.loadRequest(request)
            web_View.scalesPageToFit = true;
        }else{
            self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }

    func setUpTimerLabel() {
        let timer = Timer.scheduledTimer(timeInterval: 1,
                                         target: self,
                                         selector: #selector(self.updateTime),
                                         userInfo: nil,
                                         repeats: true)
        RunLoop.current.add(timer, forMode: .common)
    }
    
    @objc func updateTime() {
        self.titleLabel.text = getElapsedTimefromDate(startTime: eventData?.start_time, endTime: eventData?.end_time)
    }

//    func setNavigationBar(){
//        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
//        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
//        self.navigationItem.setHidesBackButton(false, animated:true);
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
//            statusBar.backgroundColor   =   UIColor.white
//        }
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension Event_Web_VC: UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.showHUD(forView: self.view, excludeViews: [])
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.hideHUD(forView: self.view)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.hideHUD(forView: self.view)
    }
}
