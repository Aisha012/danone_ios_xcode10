//
//  MainContactVC.swift
//  Danone
//
//  Created by Renendra Saxena on 05/01/19.
//  Copyright © 2019 Zaman Meraj. All rights reserved.
//

import UIKit
import Alamofire

class MainContactVC: BaseClassVC {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var tableView: UITableView!

    var showMenuButton = true
    var userData: LoginData!
    var eventData: EventData!
    var request: DataRequest!
    var contactPagination = ContactListPagination()
    var isFirstTime = true
    var isFetchingData = false
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        let loginData = MyDatabase.getProgramData(tableName: "LoginData") as? [LoginData]
        userData = loginData![0]
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
      
        tableView.tableFooterView = UIView()
  
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        titleLabel.text = "Main Contact".localized
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.isFirstTime = true
        fetchContactListing()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc override func navigateBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func refreshData(_ sender: Any) {
        self.contactPagination.paginationType = .reload
        self.contactPagination.currentPageNumber = 1
        fetchContactListing()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    func showViewsAccordingly() {
        if contactPagination.contactList.count == 0 {
            tableView.isHidden = true
//            emptyPlaceholderLabel.isHidden = false
        } else {
            tableView.isHidden = false
//            emptyPlaceholderLabel.isHidden = true
        }
        tableView.reloadData()
    }
    
//    MARK: Api call
    
    func fetchContactListing(){

        let user_token = userData.authenticate_user
        if contactPagination.paginationType != .old {
            self.contactPagination.currentPageNumber = 1
            if isFirstTime == true {
                self.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        isFetchingData = true
        
        
        request = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(eventData.event_id!)/contact_people?user_token=\(user_token!)&api_token=\(apiToken)", parameter: nil, type: .get) { (netResponse) -> (Void) in
            self.isFetchingData = false
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                
                if let JSON = netResponse.responseDict as? [NSDictionary] {
                    let newPagination = ContactListPagination(array: JSON)
                    self.contactPagination.appendDataFromObject(newPagination)
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    
                    self.tableView.reloadData()
                    DispatchQueue.main.async {
                        if self.contactPagination.paginationType != .old && self.isFirstTime == true {
                            self.hideHUD(forView: self.view)
                            self.isFirstTime = false
                        } else {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                        self.showViewsAccordingly()
                    }
                } else {
                    self.hideHUD(forView: self.view)
                }
            }
        }
    }
}

extension MainContactVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactPagination.contactList.count > 0 ? contactPagination.contactList.count + 1 : 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row < contactPagination.contactList.count else {
            return contactPagination.hasMoreToLoad ? 50.0 : 0.0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row < contactPagination.contactList.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell" , for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
  
        let cell : MainContactTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! MainContactTableViewCell
         cell.configureCell(contact: contactPagination.contactList[indexPath.row ])
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
    if indexPath.row == self.contactPagination.contactList.count && self.contactPagination.hasMoreToLoad && !isFetchingData {
        self.contactPagination.paginationType = .old
    fetchContactListing()
    }
    }
  
}
