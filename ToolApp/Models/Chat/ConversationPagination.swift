//
//  ConversationPagination.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class ConversationListPagination {
    
    var conversations: [Conversation] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool = false
    
    init() {
        conversations = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    init(array: [NSDictionary]) {
        for dict in array {
            let convo = Conversation(responseDict: dict)
            self.conversations.append(convo)
        }
        
        hasMoreToLoad = self.conversations.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: ConversationListPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.conversations = [];
            self.conversations = newPaginationObject.conversations
        case .old:
            self.conversations += newPaginationObject.conversations
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}

