//
//  Utilities.swift
//  ToolApp
//
//  Created by Phaninder on 16/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation
import Reachability

class Utilities: NSObject {
    
    static let shared = Utilities()
    var reachability: Reachability?

    class func logoutUser() {
        MyDatabase.clearAllData()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.fetchedEventsData = true
        DispatchQueue.main.async(execute: {
            UserDefaults.standard.set("userNotRegister", forKey: "Registered")
            UserDefaults.standard.set("", forKey: "EventId")
            UserDefaults.standard.set("", forKey: "UUID")
            UserDefaults.standard.set("", forKey: "Auth_Token")
            UserDefaults.standard.set("", forKey: "DeviceToken")
            UserDefaults.standard.set("113cf28b-728f-4696-8aa7-8f4365f6c475", forKey: "API_TOKEN")
            UserDefaults.standard.synchronize()
        })
    }
    
    class func getDateFromString(_ dateString: String?) -> Date? {
        if let date = dateString, !date.isEmpty {
            let index = date.index(date.startIndex, offsetBy: 16)
            let myDateString = String(date[..<index])
            let dateFormatter = DateFormatter()

            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
            if let createdDate = dateFormatter.date(from: myDateString) {
                return createdDate
            }
        }
        return nil
    }
    
    func isNetworkReachable() -> Bool {
        guard let reachability = reachability else {
            return false
        }
        return reachability.connection != .none
    }

    func setupReachability() {
        self.initializeReachability()
    }

    fileprivate func initializeReachability() {
        let reachability = Reachability()
        self.reachability = reachability
        
        do {
            try self.reachability!.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }

    func registerDeviceToken() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        let reg = UserDefaults.standard.object(forKey: "Registered") as? String
        let deviceToken = UserDefaults.standard.object(forKey: "DeviceToken") as? String
        let userId = UserDefaults.standard.object(forKey: "UUID") as? String
        
        if let auth_Token = auth_Token, let deviceToken = deviceToken, let userId = userId, let loggedIn = reg, loggedIn == "userRegister" {
           // let device = ["token" : deviceToken, "user_id" : userId, "api_token": apiToken,  "user_token": auth_Token, "plateform": "ios"]
            
            let param: NSDictionary = ["token" : deviceToken,
                                    "user_id" : userId,
                                     "api_token": apiToken,
                                      "user_token": auth_Token,
                                       "plateform": "ios"]
            print("params : \n", param)
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/devices", parameter: param as? Dictionary<String, Any>, type: .post) { (netResponse) -> (Void) in
                
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let _ = netResponse.responseDict as? NSDictionary {

                    }
                }else {

                }
            }
        }
    }

    class func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    class func convertDate(getDate: String?) -> String {
        guard let stringDate = getDate else {
            return "---"
        }
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: stringDate)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "hh:mm a"
        let myDate = dateFormatter2.string(from: date!)
        return myDate
    }
    
    class func hasPermissionsForCheckIn() -> Bool {
        let acceptedRoles = ["admin", "customer", "moderator", "staff"]
        let loginData = MyDatabase.getProgramData(tableName: "LoginData") as? [LoginData]
        if let loginDataArray = loginData, let role = loginDataArray[0].role {
            return acceptedRoles.contains(role)
        }
        return false
    }

}
