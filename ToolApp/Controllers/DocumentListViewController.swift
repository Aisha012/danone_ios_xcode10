//
//  DocumentListViewController.swift
//  ToolApp
//
//  Created by Phaninder on 26/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import QuickLook
import SafariServices

class DocumentListViewController: BaseClassVC {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!

    var showMenuButton = true
    var documents = [Document]()
    var eventId: String!
    var eventData: EventData!
    var quickLookController = QLPreviewController()
    var docIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id
        quickLookController.dataSource = self
        
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        self.titleLabel.text = "Documents".localized
        emptyPlaceholderLabel.text = "No Documents".localized
        fetchDocuments()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func pushToWebViewWithURL(_ url: String) {
        if let url = URL(string: url) {
            if #available(iOS 11.0, *) {
                let config: SFSafariViewController.Configuration = SFSafariViewController.Configuration.init()
                config.entersReaderIfAvailable = true
                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true, completion: nil)
            } else {
                let safariController = SFSafariViewController(url: url)
                self.present(safariController, animated: true, completion: nil)
            }
        }
    }

    
    func fetchDocuments(){
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            let urlString = "http://eu.aio-events.com/api/events/\(eventId!)/documents?user_token=\(auth_Token)&api_token=\(apiToken)"
            _ = CallApi.getData(url: urlString, parameter: nil, type: .get) { (netResponse) -> (Void)
                in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        for docDictionary in JSON {
                            let doc = Document(responseDict: docDictionary)
                            self.documents.append(doc)
                        }
                        self.showDataAccordingly()
                    } else {
                        self.showDataAccordingly()
                    }
                    self.hideHUD(forView: self.view)
                } else {
                    self.hideHUD(forView: self.view)
                    self.showDataAccordingly()
                }
            }
        }
    }

    func showDataAccordingly() {
        DispatchQueue.main.async {
            self.tableView.isHidden = self.documents.isEmpty
            self.emptyPlaceholderLabel.isHidden = !self.documents.isEmpty
            self.tableView.reloadData()
        }
    }
    
}

extension DocumentListViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DocumentTableViewCell.cellIdentifier(), for: indexPath) as! DocumentTableViewCell
        cell.configureCell(doc: documents[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 86.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        docIndex = indexPath.row
        pushToWebViewWithURL(documents[docIndex].file)
    }
    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//    }
}

extension DocumentListViewController: QLPreviewControllerDataSource {
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return documents[docIndex]
    }
    
}
