//
//  AgendaLIstModal.swift
//  ToolApp
//
//  Created by Zaman Meraj on 06/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import Foundation

struct AgendaListModal {
    var agendaListId: String?
    var title: String?
    var description: String?
    var time_range: String?
    var start_date: String?
    var end_date: String?
    var event_id: String?
    var category: CategoryModal?
    var roomModal: RoomModal?
    var speakersModal = [SpeakersModal]()
    var breakOutSessions = [BreakOutSession]()
    var isAddedToCalendar = false
    var hidePooling = false
    var hideFeedback = false
    var hidePolling = false
    var feedbackTitle = ""
    var notes = ""
    var categorySortTitle = ""
    
    init(responseDict: NSDictionary, eventId: String) {
        self.agendaListId = "\(responseDict.value(forKey: "id") as! Int)"
        self.title        = responseDict.value(forKey: "title") as? String
        self.description  = responseDict.value(forKey: "description") as? String
        self.time_range   = responseDict.value(forKey: "time_range") as? String
        self.start_date   = responseDict.value(forKey: "start_date") as? String
        self.end_date     = responseDict.value(forKey: "end_date") as? String
        self.event_id     = eventId
        self.hidePooling = responseDict.value(forKey: "hide_pooling_in_app") as? Bool ?? true
        self.hideFeedback = responseDict.value(forKey: "hide_feedback_in_app") as? Bool ?? true
        self.hidePolling = responseDict.value(forKey: "hide_polling_in_app") as? Bool ?? true
        self.feedbackTitle = responseDict.value(forKey: "feedback_title") as? String ?? "Session feedback"
        self.notes = responseDict.value(forKey: "notes") as? String ?? ""
        self.categorySortTitle = responseDict.value(forKey: "category_sort_caption") as? String ?? ""
        
         if let category = responseDict.value(forKey: "category") as? NSDictionary {
            let categoryModal = CategoryModal(responseDict: category, agendaId: self.agendaListId!)
            self.category = categoryModal
        }
        
        if let room = responseDict.value(forKey: "room") as? NSDictionary {
            let roomModal = RoomModal(responseDict: room, agendaId: self.agendaListId!)
            self.roomModal  =   roomModal
        }
        
        if let speakers = responseDict.value(forKey: "speakers") as? [NSDictionary] {
            
            for speaker in speakers {
                let speakerModal = SpeakersModal(responseDict: speaker, agendaId: self.agendaListId!)
                self.speakersModal.append(speakerModal)
            }
            
        }
        
        if let breakoutArray = responseDict.value(forKey: "breakout_sessions") as? [NSDictionary] {
            
            for breakoutSession in breakoutArray {
                let breakout = BreakOutSession(responseDict: breakoutSession)
                self.breakOutSessions.append(breakout)
            }
            
        }

    }
    
}
