//
//  AgendaListingVc.swift
//  ToolApp
//
//  Created by mayank on 28/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit

class AgendaListingVc: BaseClassVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var customNavBar: UIView!

    var Agenda = [NSDictionary]()
    var headerArray =  [String]()
//    var categoryModal: CategoryModal?
//    var roomModal: RoomModal?
//    var speakersModal = [SpeakersModal]()
//    var agentsListMdal = [AgendaListModal]()
    var eventId: String?
    var eventData: EventData!
    var pageIndex = 1
    var date = [String]()
    var completeModal = [CompleteAgendaModal]()
    var showMenuButton = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        
        self.titleLabel.text = "Agenda".localized

        self.setTableLayout()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)

        self.tableView.estimatedRowHeight   =   150
        self.tableView.rowHeight            =   UITableView.automaticDimension
        self.eventId = eventData.event_id!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
        fetchAgendas()
    }
    
    func setTableLayout(){
        tableView.dataSource = self
        tableView.delegate   = self
        let nib  = UINib(nibName : "AgendaCell", bundle : nil)
        tableView.register(nib, forCellReuseIdentifier: "AgendaCell")
    
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CalendarNav" {
            let calendarVC = segue.destination as! CalendarViewController
            calendarVC.completeModal = completeModal
            calendarVC.eventId = self.eventId
        }
    }

}

extension AgendaListingVc: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let modalListing = self.completeModal[section]
        return modalListing.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "AgendaCell", for: indexPath) as! AgendaCell
        let sectionNo = indexPath.section
        let myList = self.completeModal[sectionNo].list[indexPath.row]
        cell.agenda_des.text = myList.description
        cell.agendaName.text = myList.title
        
        let startDate = AgendaListingVc.convertDate(getDate: myList.start_date!)
        let endDate = AgendaListingVc.convertDate(getDate: myList.end_date!)
        
        cell.mrngTime.text = startDate
        cell.eveTime.text  = endDate
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.completeModal.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHead = UIView(frame: CGRect(x: 0, y: 0, width : self.tableView.frame.size.width, height : self.view.frame.size.height*0.5))
        /* Create custom view to display section header... */
        let label = UILabel(frame: CGRect(x :10, y : viewHead.frame.size.height*0.5/7.5, width : tableView.frame.size.width, height :  18))
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.black
        let myDate = DateExtension.changeDateFormat(date: self.completeModal[section].date!, getDateFormat: "yyyy-MM-dd", desiredDateFormat: "EEE, MMM dd yyyy")
            label.text = myDate
        viewHead.addSubview(label)
        viewHead.backgroundColor = UIColor.white
        return viewHead
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.tableView.frame.size.height*0.08
    }
    
   static func convertDate(getDate: String) -> String {
        
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: getDate)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "HH:mm a"
        let myDate = dateFormatter2.string(from: date!)
        return myDate
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sectionNo = indexPath.section
        let value = self.completeModal[sectionNo].list[indexPath.row]
        
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "AgendaSubDetailVc") as! AgendaSubDetailVc
        destination.lisModal      = value
        destination.eventId       = self.eventId
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
}

extension AgendaListingVc { 
    
    func fetchAgendas() {
        
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            _ = self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agenda_list?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    self.hideHUD(forView: self.view)
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        self.completeModal.removeAll()
                        for jsonValue in JSON {
                           let completeModal = CompleteAgendaModal(responseDic: jsonValue, eventId: self.eventId!)
                            self.completeModal.append(completeModal)
                            
                        }
                        if self.completeModal.count <= 0{
                            self.view.makeToast("There is no agenda listing".localized)
                        }else{
                            self.hideHUD(forView: self.view)
                            self.tableView.reloadData()
                        }
                        
                    }
                }
            }
        }
    }
}
