//
//  Constants.swift
//  ToolApp
//
//  Created by Phaninder on 16/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

//MARK: - Dimension Constants -

let ScreenWidth = UIScreen.main.bounds.width
let ScreenHeight = UIScreen.main.bounds.height

//let apiToken = UserDefaults.standard.value(forKey: "API_TOKEN") as? String ?? "113cf28b-728f-4696-8aa7-8f4365f6c475"
let apiToken = UserDefaults.standard.value(forKey: "API_TOKEN") as? String ?? "113cf28b-728f-4696-8aa7-8f4365f6c475"
enum PaginationType: Int {
    case new, old, reload
}

let AppName = "AIO Events".localized
let BadgeNumber = "badge"
let OtherPictures = "Other Pictures"

extension Notification.Name {
    static let categorySelected = Notification.Name("categorySelected")
    static let roomSelected = Notification.Name("roomSelected")
    static let dateSelected = Notification.Name("dateSelected")
    static let searchChanged = Notification.Name("searchChanged")
    static let fetchSearchData = Notification.Name("fetchSearchData")
    static let questionChanged = Notification.Name("questionChanged")
    static let directionButtonTapped = Notification.Name("directionButtonTapped")
    static let questionSubmitted = Notification.Name("questionSubmitted")
    static let notificationCountChanged = Notification.Name("notificationCountChanged")
}

