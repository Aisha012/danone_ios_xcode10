//
//  Document.swift
//  ToolApp
//
//  Created by Phaninder on 26/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation
import QuickLook

class Document: NSObject, QLPreviewItem {
    
    let id: Int!
    let caption: String!
    let file: String!
    let fileType: String!
    let previewItemURL: URL?
    let previewItemTitle: String?
    
    
    init(responseDict: NSDictionary) {
        self.id = responseDict.value(forKey: "id") as? Int ?? 0
        
        self.caption = responseDict.value(forKey: "caption") as? String ?? ""
        self.file = responseDict.value(forKey: "file") as? String ?? ""
        self.previewItemURL = URL(string: self.file)
        self.previewItemTitle = caption
        self.fileType = responseDict.value(forKey: "file_type") as? String ?? ""
    }
    

}
