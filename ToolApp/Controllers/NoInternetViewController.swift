//
//  NoInternetViewController.swift
//  ToolApp
//
//  Created by Phaninder on 26/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class NoInternetViewController: BaseClassVC {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    
    var currentUser: LoginData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let loginData = MyDatabase.getProgramData(tableName: "LoginData") as? [LoginData]
        
        if let users = loginData, users.count > 0 {
            currentUser = users[0]
            messageLabel.text = "Looks like you have an issue with Internet. Do you want to visit the profile page in the mean while?".localized
        } else {
            messageLabel.text = "Looks like you have an issue with Internet. Please try again after sometime".localized
            stackView.removeArrangedSubview(profileButton)
        }

    }

    @IBAction func refreshButtonTapped(_ sender: UIButton) {
        
        guard Utilities.shared.isNetworkReachable() else {
            return
        }
        let reg   = UserDefaults.standard.object(forKey: "Registered") as? String
        let window = (UIApplication.shared.delegate as! AppDelegate).window
        if reg != "userRegister" || reg == nil{
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "RevealViewController") as? SWRevealViewController
            window?.rootViewController = controller
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let frontviewcontroller = storyboard.instantiateViewController(withIdentifier: "EventListVc") as! EventListVc
            let objNavigationController : UINavigationController    =   UINavigationController.init(rootViewController: frontviewcontroller)
            let rearViewController = storyboard.instantiateViewController(withIdentifier: "menuViewController") as? menuViewController
            let mainRevealController = SWRevealViewController()
            mainRevealController.frontViewController = objNavigationController
            mainRevealController.rearViewController = rearViewController
            window?.rootViewController = mainRevealController
            window?.makeKeyAndVisible()
        }

    }
    
    @IBAction func profileButtonTapped(_ sender: UIButton) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        viewController.user  = currentUser
        viewController.showMenuButton = false
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
