//
//  Category.swift
//  ToolApp
//
//  Created by Phaninder on 05/05/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class GalleryCategory {
    
    let id: Int!
    let name: String!
    var pictures = [String]()
    
    init(responseDict: NSDictionary) {
        self.id = responseDict.value(forKey: "id") as? Int ?? 0
        self.name = responseDict.value(forKey: "name") as? String ?? ""
        self.pictures = responseDict.value(forKey: "pictures") as? [String] ?? [String]()
    }
}

