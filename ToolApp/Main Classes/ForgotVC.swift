//
//  ForgotVC.swift
//  ToolApp
//
//  Created by mayank on 06/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import JSSAlertView

class ForgotVC: BaseClassVC {
    
    @IBOutlet weak var mailField: UITextField!
    var APIToken = "113cf28b-728f-4696-8aa7-8f4365f6c475"
    var eventOptions = [EventOption]()
    var shouldShowEventOptions = false

    
    @IBOutlet weak var optionTableView: UITableView!
    @IBOutlet weak var optionHolderView: UIView!
    @IBOutlet weak var optionHolderViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var optionHolderViewVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var blurView: UIVisualEffectView!

    override func viewDidLoad() {
        super.viewDidLoad()
        optionTableView.dataSource = self
        optionTableView.delegate = self

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        self.mailField.layer.borderWidth  =   0.5
        self.mailField.layer.borderColor  =   UIColor.white.cgColor

        self.mailField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.colorWithHexString(hex: "F3903F")])
        self.mailField.setLeftPaddingPoints(10)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resetBtn(_ sender: Any) {
        view.endEditing(true)
        if let text = self.mailField.text{
            if text.isEmpty {
                _ = JSSAlertView().warning(
                    self,
                    title: AppName,
                    text: "Email Field can not be blank.".localized,
                    buttonText: "Ok".localized)
            }else{
                self.AIOResetPassword()
            }
        }else{
            _ = JSSAlertView().warning(
                self,
                title: AppName,
                text: "Email Field can not be blank.".localized,
                buttonText: "Ok".localized)
        }
        
    }

    func resetPassword() {
        
        self.showHUD(forView: self.view, excludeViews: [])
        _ = CallApi.getData(url: "http://eu.aio-events.com/api/users/request_password_reset?api_token=\(APIToken)&email=\(self.mailField.text ?? "")", parameter: nil, type: .get) { (netResponse) -> (Void) in
            self.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    var message = "A new password reset link has been sent to your email.".localized
                    if let JSON = netResponse.responseDict as? NSDictionary, let msg = JSON.value(forKey: "message") as? String {
                        message = msg
                    }
                    let alert = JSSAlertView().success(
                        self,
                        title: AppName,
                        text: message,
                        buttonText: "OK")
                    alert.addAction {
                        self.navigateBack()
                    }
                } else {
                    var message = "Something didn't go as expected.".localized
                    if let JSON = netResponse.responseDict as? NSDictionary, let msg = JSON.value(forKey: "message") as? String {
                        message = msg
                    }
                    JSSAlertView().danger(
                        self,
                        title: "Error!".localized,
                        text: message,
                        buttonText: "Ok".localized)
                }
            }
        }
    }
    
    func AIOResetPassword() {
        
        self.showHUD(forView: self.view, excludeViews: [])
        _ = CallApi.getData(url: "http://eu.aio-events.com/api/aio/request_password_reset?email=\(self.mailField.text ?? "")", parameter: nil, type: .get) { (netResponse) -> (Void) in
            self.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    var message = "A new password reset link has been sent to your email."
                    if let JSON = netResponse.responseDict as? NSDictionary, let msg = JSON.value(forKey: "message") as? String {
                        message = msg
                    }
                    let alert = JSSAlertView().success(
                        self,
                        title: "Tool App",
                        text: message,
                        buttonText: "OK")
                    alert.addAction {
                        self.navigateBack()
                    }
                    
                } else {
                    self.hideHUD(forView: self.view)
                    if netResponse.statusCode == 300, let JSON = netResponse.responseDict as? [NSDictionary], JSON.count > 0 {
                        self.eventOptions.removeAll()
                        for eventOpt in JSON {
                            let eventOption = EventOption(responseDict: eventOpt)
                            self.eventOptions.append(eventOption)
                        }
                        DispatchQueue.main.async {
                            self.showBlurView()
                        }
                    } else {
                        var message = "Something went wrong. Please try again later."
                        if let JSON = netResponse.responseDict as? NSDictionary, let msg = JSON.value(forKey: "message") as? String {
                            message = msg
                        }
                        JSSAlertView().danger(
                            self,
                            title: "Error!",
                            text: message,
                            buttonText: "Ok")

                    }

                }
            }
        }
    }

    func hideBlurView() {
        
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.9, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.optionHolderViewVerticalConstraint.constant = ScreenHeight + (self.optionHolderViewHeightConstraint.constant / 2)
            self.blurView.alpha = 0
            
            self.view.layoutIfNeeded()
        }) { (completed) in
            if completed {
                self.blurView.isHidden = true
                self.blurView.alpha = 1
                self.optionHolderView.isHidden = true
            }
        }
    }
    
    func showBlurView() {
        optionHolderViewHeightConstraint.constant = heightForOptionsView()
        optionTableView.isUserInteractionEnabled = true
        
        optionTableView.reloadData()
        view.layoutIfNeeded()
        optionHolderViewVerticalConstraint.constant = -(ScreenHeight / 2) - (optionHolderViewHeightConstraint.constant / 2)
        self.optionHolderView.isHidden = false
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.optionHolderViewVerticalConstraint.constant = 0
            self.blurView.isHidden = false
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }

    
    func heightForOptionsView() -> CGFloat {
        return CGFloat(41.0 + (75.0 * Double(eventOptions.count)))
    }

}
extension UITextField{
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}

extension ForgotVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EventOptionTableViewCell.cellIdentifier(), for: indexPath) as! EventOptionTableViewCell
        cell.nameLabel.text = eventOptions[indexPath.row].name
        cell.eventImageView.sd_setShowActivityIndicatorView(true)
        cell.eventImageView.sd_setIndicatorStyle(.gray)
        
        cell.eventImageView.sd_setImage(with: URL(string: eventOptions[indexPath.row].logo), placeholderImage: UIImage(named: "gallery_placeholder"), options: .progressiveDownload, completed: nil)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        APIToken = self.eventOptions[indexPath.row].apiToken
        UserDefaults.standard.set(APIToken, forKey: "API_TOKEN")
        UserDefaults.standard.synchronize()
        hideBlurView()
        resetPassword()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
    
}
