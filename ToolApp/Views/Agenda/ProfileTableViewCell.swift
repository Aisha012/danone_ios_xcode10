//
//  ProfileTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 09/05/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var cellImageView: UIImageView!
    
    override func awakeFromNib() {
        holderView.layer.shadowOffset = CGSize(width: 0, height: 2)
        holderView.layer.shadowColor = UIColor.black.cgColor
        holderView.layer.shadowRadius = 4
        holderView.layer.cornerRadius = 5
        holderView.layer.shadowOpacity = 0.3
        holderView.layer.masksToBounds = false
        holderView.layer.rasterizationScale = UIScreen.main.scale
        holderView.clipsToBounds = false
    }
    
    class func cellIdentifier() -> String {
        return "ProfileTableViewCell"
    }
    
    func configureCellWithText() {
        
    }
    
    
}
