//
//  Spekers_VC.swift
//  ToolApp
//
//  Created by Prashant Tiwari on 30/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import SafariServices

class Spekers_VC: BaseClassVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!

    var eventId: String?
    var eventData: EventData!
    var app_token = String()
    var pageIndex =  1
    var showMenuButton = true
    var speakerPagination = SpeakerPagination()
    private let refreshControl = UIRefreshControl()
    var isFirstTime = true
    var isFetchingData = false

    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id!
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        self.titleLabel.text = "Speakers".localized
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        tableView.tableFooterView = UIView()
        fetchSpeakers()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func refreshData(_ sender: Any) {
        self.speakerPagination.paginationType = .reload
        self.speakerPagination.currentPageNumber = 1
        fetchSpeakers()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    func pushToWebViewWithURL(_ url: String) {
        if let url = URL(string: url) {
            if #available(iOS 11.0, *) {
                let config: SFSafariViewController.Configuration = SFSafariViewController.Configuration.init()
                config.entersReaderIfAvailable = true
                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true, completion: nil)
            } else {
                let safariController = SFSafariViewController(url: url)
                self.present(safariController, animated: true, completion: nil)
            }
        }
    }

    func showViewsAccordingly() {
        if speakerPagination.speakers.count == 0 {
            tableView.isHidden = true
            emptyPlaceholderLabel.isHidden = false
        } else {
            tableView.isHidden = false
            emptyPlaceholderLabel.isHidden = true
        }
        tableView.reloadData()
    }

    func fetchSpeakers(){
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            if speakerPagination.paginationType != .old {
                self.speakerPagination.currentPageNumber = 1
                if isFirstTime == true {
                    self.showHUD(forView: self.view, excludeViews: [])
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                }
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            isFetchingData = true
            
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/speakers?user_token=\(auth_Token)&api_token=\(apiToken)&page=\(speakerPagination.currentPageNumber))", parameter: nil, type: .get) { (netResponse) -> (Void) in
                self.isFetchingData = false
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        let newPagination = SpeakerPagination(array: JSON)
                        self.speakerPagination.appendDataFromObject(newPagination)
                        DispatchQueue.main.async {
                            if self.speakerPagination.paginationType != .old && self.isFirstTime == true {
                                self.hideHUD(forView: self.view)
                                self.isFirstTime = false
                                self.showViewsAccordingly()
                            } else {
                                self.showViewsAccordingly()
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                        }
                    } else {
                        self.hideHUD(forView: self.view)
                    }
                }
            }
        }
    }
    
    func upVoteSpeaker(speakerId: String, atIndex index: Int) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])

            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/3/upvote/\(speakerId)?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .post) { (netResponse) -> (Void) in
                
                self.hideHUD(forView: self.view)
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    if let _ = netResponse.responseDict as? NSDictionary {
                        self.speakerPagination.speakers[index].hasVoted = true
                        self.speakerPagination.speakers[index].votes += 1
                        DispatchQueue.main.async {
                            self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
                        }
                    }
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }
        }
    }

    func downVoteSpeaker(speakerId: String, atIndex index: Int) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String

        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/3/downvote/\(speakerId)?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .post) { (netResponse) -> (Void) in
                
                self.hideHUD(forView: self.view)

                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let _ = netResponse.responseDict as? NSDictionary {
                        self.speakerPagination.speakers[index].votes -= 1
                        self.speakerPagination.speakers[index].hasVoted = false
                        DispatchQueue.main.async {
                            self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
                        }
                    }
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }
        }
    }

}

extension Spekers_VC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard speakerPagination.speakers.count > 0  else {
            return 0
        }
        return speakerPagination.speakers.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < speakerPagination.speakers.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: SpeakersTableViewCell.cellIdentifier(), for: indexPath) as! SpeakersTableViewCell
        cell.configureCell(speaker: speakerPagination.speakers[indexPath.row], index: indexPath.row)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row < speakerPagination.speakers.count else {
            return speakerPagination.hasMoreToLoad ? 50.0 : 0.0
        }
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.speakerPagination.speakers.count && self.speakerPagination.hasMoreToLoad && !isFetchingData {
            self.speakerPagination.paginationType = .old
            fetchSpeakers()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let value = self.speakerPagination.speakers[indexPath.row]
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "SpeakerDetailViewController") as! SpeakerDetailViewController
        destination.speaker = value
        destination.showUpvoteOption = false
        destination.eventId       = self.eventId
        self.navigationController?.pushViewController(destination, animated: true)        
    }
    
}

extension Spekers_VC: SpeakersTableViewCellDelegate {
    
    func facebookButtonTappedAtIndex(_ index: Int) {
        if let fbLink = speakerPagination.speakers[index].facebookLink,
            !fbLink.isEmpty {
            pushToWebViewWithURL(fbLink)
        }
    }
    
    func linkedInButtonTappedAtIndex(_ index: Int) {
        if let linkedInLink = speakerPagination.speakers[index].linkedInLink,
            !linkedInLink.isEmpty {
            pushToWebViewWithURL(linkedInLink)
        }
    }
    
    func googleButtonTappedAtIndex(_ index: Int) {
        if let googleLink = speakerPagination.speakers[index].googleLink,
            !googleLink.isEmpty {
            pushToWebViewWithURL(googleLink)
        }
    }
    
    func twitterButtonTappedAtIndex(_ index: Int) {
        if let twitterLink = speakerPagination.speakers[index].twitterLink,
            !twitterLink.isEmpty {
            pushToWebViewWithURL(twitterLink)
        }
    }
    
    func likeButtonTapped(_ index: Int) {
        let speaker = speakerPagination.speakers[index]
        let speakerId = speaker.speaker_id ?? "0"
        if speaker.hasVoted {
            downVoteSpeaker(speakerId: speakerId, atIndex: index)
        } else {
            upVoteSpeaker(speakerId: speakerId, atIndex: index)
        }
    }

}
