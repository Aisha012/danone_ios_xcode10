//
//  SponsorPagination.swift
//  ToolApp
//
//  Created by Phaninder on 19/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class SponsorPagination {
    
    var sponsors: [Sponsor] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool = false
    
    init() {
        sponsors = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    init(array: [NSDictionary]) {
        for dict in array {
            let sponsor = Sponsor(responseDict: dict)
            self.sponsors.append(sponsor)
        }
        
        hasMoreToLoad = self.sponsors.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: SponsorPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.sponsors = [];
            self.sponsors = newPaginationObject.sponsors
        case .old:
            self.sponsors += newPaginationObject.sponsors
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
}

}
