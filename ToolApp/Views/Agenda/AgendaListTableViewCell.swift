//
//  AgendaListTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 08/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

protocol AgendaListTableViewCellDelegate {
    
    func scheduleButtonTapped(atIndex index: IndexPath)
    
}

class AgendaListTableViewCell: UITableViewCell {

    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var agendaNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var scheduleButton: UIButton!
    @IBOutlet weak var markerImageView: UIImageView!
    @IBOutlet weak var timeImagerView: UIImageView!
    
    var delegate: AgendaListTableViewCellDelegate?
    
    var indexPath: IndexPath!
    
    class func cellIdentifier() -> String {
        return "AgendaListTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.holderView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.holderView.layer.shadowColor = UIColor.black.cgColor
        self.holderView.layer.shadowRadius = 4
        self.holderView.layer.cornerRadius = 5
        self.holderView.layer.shadowOpacity = 0.3
        self.holderView.layer.masksToBounds = false
        self.holderView.layer.rasterizationScale = UIScreen.main.scale
        self.holderView.clipsToBounds = false
    }

    func configureCell(agenda: AgendaListModal, atIndex index: IndexPath) {
        
        self.indexPath = index
        let startTime = Utilities.convertDate(getDate: agenda.start_date)
        let endTime = Utilities.convertDate(getDate: agenda.end_date)
        
        if let room = agenda.roomModal {
            markerImageView.isHidden = false
            locationLabel.isHidden = false
            locationLabel.text = room.name ?? ""
        } else {
            markerImageView.isHidden = true
            locationLabel.isHidden = true
        }
        agendaNameLabel.text = agenda.title ?? ""
        let title = agenda.isAddedToCalendar ? "x Remove from Calendar".localized : "+ Add to Calendar".localized
        timeLabel.text = startTime + " - " + endTime
        scheduleButton.setTitle(title, for: .normal)
    }
    
    @IBAction func addScheduleButtonTapped(_ sender: UIButton) {
        delegate?.scheduleButtonTapped(atIndex: self.indexPath)
    }
    
}
