//
//  GalleryFile.swift
//  ToolApp
//
//  Created by Phaninder on 20/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation
import Photos

class GalleryFile {
    
    let thumb: String!
    let banner: String!
    let id: Int!
    let caption: String!
    
    init(responseDict: NSDictionary) {
        self.id = responseDict.value(forKey: "id") as? Int ?? 0
        self.caption = responseDict.value(forKey: "caption") as? String ?? ""
        self.banner = responseDict.value(forKey: "banner") as? String ?? ""
        self.thumb = responseDict.value(forKey: "thumb") as? String ?? ""
    }
    
    
}
