//
//  AgendaSubDetailVc.swift
//  ToolApp
//
//  Created by mayank on 06/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import SafariServices
import AVFoundation
import EventKit
import JSSAlertView

class AgendaSubDetailVc: BaseClassVC {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var tableView: UITableView!
        
    var lisModal: AgendaListModal?
    var eventId: String?
    let store = EKEventStore()
    var shouldClearData = false
    var agendaId: String!
    var canAccessCalendar = false
    var selectedEvent: EKEvent?
    var buttonTableViewCellHeight: CGFloat = 408.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        self.titleLabel.text = "Agenda Details".localized
        menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
        menuButton.setImage(nil, for: .normal)
        backImage.image = UIImage(named: "back.png")
        if let _ = agendaId {
            fetchAgenda()
        } else {
            shouldAddToCalendar()
        }
        self.tableView.estimatedRowHeight = 80.0
        canAccessCalendar = EKEventStore.authorizationStatus(for: .event) == .authorized
        if canAccessCalendar == false {
            store.requestAccess(to: .event) { (success, error) in
                
                if error == nil {
                    self.canAccessCalendar = true
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                } else {
                    //we have error in getting access to device calnedar
                    print("error = \(String(describing: error?.localizedDescription))")
                }
            }
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }

    func pushToWebViewWithURL(_ url: String) {
        if let url = URL(string: url) {
            if #available(iOS 11.0, *) {
                let config: SFSafariViewController.Configuration = SFSafariViewController.Configuration.init()
                config.entersReaderIfAvailable = true
                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true, completion: nil)
            } else {
                let safariController = SFSafariViewController(url: url)
                self.present(safariController, animated: true, completion: nil)
            }
        }
    }
    
    func shouldAddToCalendar() {
        if canAccessCalendar,
            let title = self.lisModal!.title,
            let startDate = Utilities.getDateFromString(self.lisModal!.start_date),
            let endDate = Utilities.getDateFromString(self.lisModal!.end_date) {
            
            let predicate = store.predicateForEvents(withStart: startDate, end: endDate, calendars: nil)
            let existingEvents = store.events(matching: predicate)
            for singleEvent in existingEvents {
                if singleEvent.title == title && singleEvent.startDate == startDate {
                    self.lisModal?.isAddedToCalendar = true
                }
            }
        }
    }
    
    func createEventInTheCalendar(with title:String, forDate eventStartDate:Date, toDate eventEndDate:Date) {
        
        store.requestAccess(to: .event) { (success, error) in
            if  error == nil {
                self.canAccessCalendar = true
                let event = EKEvent.init(eventStore: self.store)
                event.title = title
                event.calendar = self.store.defaultCalendarForNewEvents // this will return deafult calendar from device calendars
                event.startDate = eventStartDate
                event.endDate = eventEndDate
                
                let alarm = EKAlarm.init(absoluteDate: Date.init(timeInterval: -3600, since: event.startDate))
                event.addAlarm(alarm)
                
                do {
                    try self.store.save(event, span: .thisEvent)
                    DispatchQueue.main.async {
                        let alert = JSSAlertView().success(
                            self,
                            title: AppName,
                            text: "Added the event to the calendar".localized,
                            buttonText: "Ok".localized)
                        alert.addAction {
                            self.lisModal?.isAddedToCalendar = true
                            self.tableView.reloadData()
                        }
                    }

                } catch let error as NSError {
                    print("failed to save event with error : \(error)")
                }
                
            } else {
                //we have error in getting access to device calnedar
                print("error = \(String(describing: error?.localizedDescription))")
            }
        }
    }

    func sendFeedback(rating: CGFloat, comment: String) {
        let agendaId = String((lisModal?.agendaListId)!)
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            let param: NSDictionary = ["rating" : String(describing: rating),
                                       "comments" : comment]

            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/\(agendaId)/feedback?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: param as? Dictionary<String, Any>, type: .post) { (netResponse) -> (Void) in
                
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let _ = netResponse.responseDict as? NSDictionary {
                        let alert = JSSAlertView().success(
                            self,
                            title: AppName,
                            text: "Thank you for the feedback".localized,
                            buttonText: "Ok".localized)
                        alert.addAction {
                            self.shouldClearData =  true
                            self.tableView.reloadData()
                        }
                    }
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }
        }
    }

    func upVoteSpeaker(speakerId: String, atIndex index: Int) {
        let agendaId = String((lisModal?.agendaListId)!)

        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String

        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/\(agendaId)/upvote/\(speakerId)?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .post) { (netResponse) -> (Void) in
                
                self.hideHUD(forView: self.view)
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    if let _ = netResponse.responseDict as? NSDictionary {
                        self.lisModal?.speakersModal[index].hasVoted = true
                        self.lisModal?.speakersModal[index].votes += 1
                        DispatchQueue.main.async {
                            self.tableView.reloadRows(at: [IndexPath.init(row: index + 4, section: 0)], with: UITableView.RowAnimation.none)
                        }
                    }
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }
        }
    }
    
    func downVoteSpeaker(speakerId: String, atIndex index: Int) {
        let agendaId = String((lisModal?.agendaListId)!)

        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/\(agendaId)/downvote/\(speakerId)?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .post) { (netResponse) -> (Void) in
                
                self.hideHUD(forView: self.view)
                
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let _ = netResponse.responseDict as? NSDictionary {
                        self.lisModal?.speakersModal[index].hasVoted = true
                        self.lisModal?.speakersModal[index].votes += 1
                        DispatchQueue.main.async {
                            self.tableView.reloadRows(at: [IndexPath.init(row: index + 4, section: 0)], with: UITableView.RowAnimation.none)
                        }
                    }
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }
        }
    }

    func fetchAgenda() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            _ = self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(eventId!)/agendas/\(agendaId!)?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    self.hideHUD(forView: self.view)
                    if let JSON = netResponse.responseDict as? NSDictionary {
                        self.lisModal = AgendaListModal(responseDict: JSON, eventId: self.eventId!)
                        DispatchQueue.main.async {
                            self.shouldAddToCalendar()
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    func removeEventFromCalendar(with title:String, forDate eventStartDate:Date, toDate eventEndDate:Date) {
        
        store.requestAccess(to: .event) { (success, error) in
            if  error == nil {
                self.canAccessCalendar = true
                let predicate = self.store.predicateForEvents(withStart: eventStartDate, end: eventEndDate, calendars: nil)
                let existingEvents = self.store.events(matching: predicate)
                for singleEvent in existingEvents {
                    if singleEvent.title == title && singleEvent.startDate == eventStartDate {
                        do {
                            try self.store.remove(singleEvent, span: .thisEvent)
                            DispatchQueue.main.async {
                                let alert = JSSAlertView().success(
                                    self,
                                    title: AppName,
                                    text: "Removed the event from the calendar".localized,
                                    buttonText: "Ok".localized)
                                alert.addAction {
                                    self.lisModal?.isAddedToCalendar = false
                                    self.tableView.reloadData()
                                }
                            }
                            
                        } catch let error as NSError {
                            print("failed to remove event with error : \(error)")
                        }
                    }
                }
            } else {
                //we have error in getting access to device calnedar
                print("error = \(String(describing: error?.localizedDescription))")
            }
        }
    }

    func fetchPolls() {
        let agendaId = String((lisModal?.agendaListId)!)

        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            _ = self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(eventId!)/agendas/\(agendaId)/survey_questions?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    self.hideHUD(forView: self.view)
                    if let JSON = netResponse.responseDict as? NSDictionary, let pollsArray = JSON["polls"] as? [NSDictionary] {
                        var polls = [Poll]()
                        for pollDictionary in pollsArray {
                            let poll = Poll(responseDict: pollDictionary)
                            polls.append(poll)
                        }
                        if polls.count > 0 {
                            DispatchQueue.main.async {
                                let destination  =  self.storyboard?.instantiateViewController(withIdentifier: "PollParentViewController") as! PollParentViewController
                                
                                destination.eventId  =  self.eventId
                                destination.agendaId = self.lisModal?.agendaListId ?? ""
                                destination.polls = polls
                                destination.agendaName = self.lisModal?.title ?? ""
                                self.navigationController?.pushViewController(destination, animated: true)
                            }
                        } else {
                            self.view.makeToast("No polls to display".localized, duration: 2.0, position: CSToastPositionCenter)
                        }
                    }
                }
            }
        }
    }
    
}

extension AgendaSubDetailVc: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let listModal = lisModal else {
            return 0
        }
        return 8 + (listModal.speakersModal.count) + (listModal.breakOutSessions.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AgendaDetailTableViewCell.cellIdentifier(), for: indexPath) as! AgendaDetailTableViewCell
            cell.configureCell(agenda: lisModal!)
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AgendaLocationTableViewCell.cellIdentifier(), for: indexPath) as! AgendaLocationTableViewCell
            cell.locationNameLabel.text = lisModal?.roomModal?.name
            return cell

        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AgendaSessionTableViewCell.cellIdentifier(), for: indexPath) as! AgendaSessionTableViewCell
            cell.sessionTypeLabel.text = lisModal?.category?.name
            return cell
        } else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AgendaSpeakersTitleTableViewCell.cellIdentifier(), for: indexPath) as! AgendaSpeakersTitleTableViewCell
            return cell
        } else if indexPath.row == 4 + (lisModal?.speakersModal.count)! {
            let cell = tableView.dequeueReusableCell(withIdentifier: BreakOutHeaderTableViewCell.cellIdentifier(), for: indexPath) as! BreakOutHeaderTableViewCell
            return cell
        } else if indexPath.row == 5 + (lisModal?.speakersModal.count)! + (lisModal?.breakOutSessions.count)! {
            let cell = tableView.dequeueReusableCell(withIdentifier: AgendaButtonsTableViewCell.cellIdentifier(), for: indexPath) as! AgendaButtonsTableViewCell

            if self.lisModal!.isAddedToCalendar == true {
                cell.attendButton.setTitle("Remove from Calendar".localized, for: .normal)
            } else {
                cell.attendButton.setTitle("Add to Calendar".localized, for: .normal)
            }

            buttonTableViewCellHeight = cell.configureCellWithColor(navigationBarColor(), agenda: lisModal!)
            if shouldClearData == true {
                cell.descriptionTextView.text = ""
                cell.ratingView.value = 2
                shouldClearData = false
            }
            cell.delegate = self
            return cell
        } else if indexPath.row == 6 + (lisModal?.speakersModal.count)! + (lisModal?.breakOutSessions.count)! {
            let cell = tableView.dequeueReusableCell(withIdentifier: AgendaDescriptionTableViewCell.cellIdentifier(), for: indexPath) as! AgendaDescriptionTableViewCell
            cell.descriptionLabel.text = lisModal?.description ?? ""
            return cell
        } else if indexPath.row == 7 + (lisModal?.speakersModal.count)! + (lisModal?.breakOutSessions.count)! {
            let cell = tableView.dequeueReusableCell(withIdentifier: AgendaNotesTableViewCell.cellIdentifier(), for: indexPath) as! AgendaNotesTableViewCell
//            cell.notesTextView.layer.cornerRadius = 3.0
//            cell.notesTextView.layer.borderColor = UIColor.darkGray.cgColor
//            cell.notesTextView.layer.borderWidth = 1.0
            cell.submitButton.backgroundColor = navigationBarColor()
            let notes = lisModal?.notes ?? ""
//            cell.notesTextView.text = notes
            cell.submitButton.setTitle(notes == "" ? "Add Notes" : "View Notes", for: .normal)
            
            cell.delegate = self
            return cell

        } else if indexPath.row < 4 + (lisModal?.speakersModal.count)! {
            let cell = tableView.dequeueReusableCell(withIdentifier: SpeakersTableViewCell.cellIdentifier(), for: indexPath) as! SpeakersTableViewCell
            cell.configureCell(speaker: (lisModal?.speakersModal[indexPath.row - 4])!, index: indexPath.row - 4)
            cell.delegate = self
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: BreakOutTableViewCell.cellIdentifier(), for: indexPath) as! BreakOutTableViewCell
            cell.configureCell((lisModal?.breakOutSessions[indexPath.row - 5 - (lisModal?.speakersModal.count)!])!)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 129.0
        } else if indexPath.row == 1 {
            if let listModel = lisModal, let roomModal = listModel.roomModal, let name = roomModal.name, !name.isEmpty {
                return 82.0
            } else {
                return 0
            }
        } else if indexPath.row == 2 {
            if let listModel = lisModal, let category = listModel.category, let name = category.name, !name.isEmpty {
                return 96.0
            } else {
                return 0
            }
        } else if indexPath.row == 3 {
            if let listModel = lisModal, listModel.speakersModal.count > 0 {
                return 44.0
            } else {
                return 0
            }
        } else if indexPath.row == 4 + (lisModal?.speakersModal.count)! {
            if let listModel = lisModal, listModel.breakOutSessions.count > 0 {
                return 65.0
            } else {
                return 0
            }
        } else if indexPath.row < 4 + (lisModal?.speakersModal.count)! {
            return 100.0
        } else if indexPath.row == 5 + (lisModal?.speakersModal.count)! + (lisModal?.breakOutSessions.count)! {
            return buttonTableViewCellHeight
        } else if indexPath.row == 6 + (lisModal?.speakersModal.count)! + (lisModal?.breakOutSessions.count)! {
            return UITableView.automaticDimension
        } else if indexPath.row == 7 + (lisModal?.speakersModal.count)! + (lisModal?.breakOutSessions.count)! {
            return 65.0
        } else {
            return UITableView.automaticDimension
        }
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 3 && indexPath.row < 4 + (lisModal?.speakersModal.count)! {
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "SpeakerDetailViewController") as! SpeakerDetailViewController
            destination.speaker = lisModal?.speakersModal[indexPath.row - 4]
            destination.controllerDelegate = self
            destination.eventId       = self.eventId
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    
}

extension AgendaSubDetailVc: SpeakerDetailViewControllerDelegate {
    
    func likedUnlikedButtonTapped(speaker: SpeakersModal) {
        if let agendaModal = lisModal {
            let index = agendaModal.speakersModal.index(where: { (speakerModal) -> Bool in
                return speakerModal.speaker_id == speaker.speaker_id
            })
            if let i = index {
                lisModal!.speakersModal[i].hasVoted = speaker.hasVoted
                lisModal!.speakersModal[i].votes = speaker.votes
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            
        }
    }
    
}

extension AgendaSubDetailVc: AgendaButtonsTableViewCellDelegate {
    
    func seeQuestionButtonTapped() {
        let destination  =  self.storyboard?.instantiateViewController(withIdentifier: "QuestionListVc") as!QuestionListVc
        destination.eventId  =  self.eventId
        destination.agendaModel = self.lisModal
        self.navigationController?.pushViewController(destination, animated: true)
    }

    func joinPollButtonTapped() {
        fetchPolls()
    }
    
    func askQuestionButtonTapped() {
        let destination  =  self.storyboard?.instantiateViewController(withIdentifier: "AskQuestionVC") as! AskQuestionVC
        destination.eventId  =  self.eventId
        destination.agendaModel = self.lisModal
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func attendButtonTapped() {
        
        if let agendaModel = lisModal, let title = agendaModel.title,
            let startDate = Utilities.getDateFromString(agendaModel.start_date),
            let endDate = Utilities.getDateFromString(agendaModel.end_date) {
            if agendaModel.isAddedToCalendar {
                removeEventFromCalendar(with: title, forDate: startDate, toDate: endDate)
            } else {
                createEventInTheCalendar(with: title, forDate: startDate, toDate: endDate)
            }
        }
    }
    
    func submitFeedbackButtonTapped(_ rating: CGFloat, feedback: String) {
        let trimmedFeedbackString = feedback.components(separatedBy: .whitespacesAndNewlines).joined(separator: "")
        
        guard !trimmedFeedbackString.isEmpty else {
            return
        }
        self.view.endEditing(true)
        sendFeedback(rating: rating, comment: feedback)
    }
}

extension AgendaSubDetailVc: SpeakersTableViewCellDelegate {
    
    func facebookButtonTappedAtIndex(_ index: Int) {
        if let fbLink = lisModal?.speakersModal[index].facebookLink,
            !fbLink.isEmpty {
            pushToWebViewWithURL(fbLink)
        }
    }
    
    func linkedInButtonTappedAtIndex(_ index: Int) {
        if let linkedInLink = lisModal?.speakersModal[index].linkedInLink,
            !linkedInLink.isEmpty {
            pushToWebViewWithURL(linkedInLink)
        }
    }
    
    func googleButtonTappedAtIndex(_ index: Int) {
        if let googleLink = lisModal?.speakersModal[index].googleLink,
            !googleLink.isEmpty {
            pushToWebViewWithURL(googleLink)
        }
    }
    
    func twitterButtonTappedAtIndex(_ index: Int) {
        if let twitterLink = lisModal?.speakersModal[index].twitterLink,
            !twitterLink.isEmpty {
            pushToWebViewWithURL(twitterLink)
        }
    }
    
    func likeButtonTapped(_ index: Int) {
        let speaker = lisModal?.speakersModal[index]
        let speakerId = speaker!.speaker_id ?? "0"
        if speaker!.hasVoted {
            downVoteSpeaker(speakerId: speakerId, atIndex: index)
        } else {
            upVoteSpeaker(speakerId: speakerId, atIndex: index)
        }
    }
}

extension AgendaSubDetailVc: AgendaNotesTableViewCellDelegate {
    
    func submitNotes(_ notes: String) {
        let destination  =  self.storyboard?.instantiateViewController(withIdentifier: "NotesViewController") as! NotesViewController
        
        destination.eventId  = self.eventId
        destination.agendaId = self.lisModal?.agendaListId ?? ""
        destination.showMenuButton = false
        destination.lisModal = self.lisModal
        destination.notesDelegate = self
        self.navigationController?.pushViewController(destination, animated: true)

    }
}
extension AgendaSubDetailVc: NotesViewControllerDelegate {

    func notesUpdatedWithText(_ notes: String) {
        self.lisModal?.notes = notes
        self.tableView.reloadData()
    }
    
}


