//
//  AgendaFilterTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder Kumar on 08/05/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

protocol AgendaFilterTableViewCellDelegate {
    func sortByCategory()
    func filterByDate()
}

class AgendaFilterTableViewCell: UITableViewCell {

    
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var dateButton: UIButton!
    
    var delegate: AgendaFilterTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "AgendaFilterTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.categoryButton.layer.cornerRadius = 5
        self.categoryButton.layer.borderColor = UIColor.gray.cgColor
        self.categoryButton.layer.borderWidth = 1.0

        
        self.dateButton.layer.cornerRadius = 5
        self.dateButton.layer.borderColor = UIColor.gray.cgColor
        self.dateButton.layer.borderWidth = 1.0

    }

    func configureCell() {
        
    }
    
    @IBAction func sortByCategoryButtonTapped(_ sender: UIButton) {
        delegate?.sortByCategory()
    }
    
    @IBAction func filterByDateButtonTapped(_ sender: UIButton) {
        delegate?.filterByDate()
    }
    
    
    
}
