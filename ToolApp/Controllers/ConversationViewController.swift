//
//  ConversationViewController.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class ConversationViewController: BaseClassVC , UIActionSheetDelegate {

    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var crossIcon: UIImageView!
    @IBOutlet weak var popUpView: UIView!

    
    
    var pagerViewController: ConversationCustomPagerViewController!
    var showMenuButton = true
    var eventId: String?
    var eventData: EventData!
    var showTextField = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        titleLabel.text = "Directory".localized
        let paddingView = UIView(frame: CGRect.init(x: 0, y: 0, width: 8, height: 30))
        searchTextField.leftView = paddingView
        searchTextField.leftViewMode = .always
        searchViewLeftConstraint.constant = ScreenWidth - 99.0
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        searchView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ChatPager" {
            pagerViewController = segue.destination as! ConversationCustomPagerViewController
            pagerViewController.eventData = self.eventData
        }
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        showTextField = !showTextField
        animateTextField()
    }
    
    func animateTextField() {
        if showTextField {
            searchTextField.becomeFirstResponder()
        } else {
            searchTextField.text = ""
            searchTextField.resignFirstResponder()
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {

            self.searchViewLeftConstraint.constant = self.showTextField ? 8 : self.searchTextField.width - 57.0
            self.searchIcon.isHidden = self.showTextField
            self.crossIcon.isHidden = !self.showTextField
            self.searchTextField.isHidden = !self.showTextField
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }

    @IBAction func valueChangedInSearchField(_ sender: Any) {
        let userInfo = ["data": searchTextField.text ?? ""];
        NotificationCenter.default.post(name: .searchChanged, object: nil, userInfo:userInfo)

//        if let req = request {
//            req.cancel()
//        }
//        self.participantPagination.paginationType = .reload
//        self.participantPagination.currentPageNumber = 1
//        fetchParticipantList()
    }
    @IBAction func filterBtnTapped(_ sender: Any) {
           let alert = UIAlertController(title: "Filter", message: "Please Select an Option", preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        
        alert.addAction(UIAlertAction(title: "Country", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
        }))

        alert.addAction(UIAlertAction(title: "Region", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    

}
