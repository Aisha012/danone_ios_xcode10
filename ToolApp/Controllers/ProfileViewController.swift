//
//  ProfileViewController.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import Alamofire

class ProfileViewController: BaseClassVC {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var QRCodeImageView: UIImageView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageHolderView: UIView!
    @IBOutlet weak var qrCodeHolderView: UIView!
    
    var showMenuButton = true
    var user: LoginData!
    var chatUser: ChatUser!
    var profileImage: UIImage!
    var isImageSelected = false
    var name = ""
    var role = ""
    var email = ""
    var phone = ""
    var position = ""
    var company = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())

        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        if let _ = chatUser {
            populateChatUserDetails()
        } else {
            populateUserDetails()
        }
        tableView.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()

        self.navigationController?.navigationBar.isHidden = true
        

        applyShadowToView(holderView: imageHolderView)
        applyShadowToView(holderView: qrCodeHolderView)

        
    }

    func applyShadowToView(holderView: UIView) {
        holderView.layer.shadowOffset = CGSize(width: 0, height: 2)
        holderView.layer.shadowColor = UIColor.black.cgColor
        holderView.layer.shadowRadius = 4
        holderView.layer.cornerRadius = 5
        holderView.layer.shadowOpacity = 0.3
        holderView.layer.masksToBounds = false
        holderView.layer.rasterizationScale = UIScreen.main.scale
        holderView.clipsToBounds = false
    }
    
    func populateUserDetails() {
        let avatarURL = user.avatar ?? ""
        profileImageView.sd_setShowActivityIndicatorView(true)
        profileImageView.sd_setIndicatorStyle(.gray)
        profileImageView.sd_setImage(with: URL(string: avatarURL), placeholderImage: UIImage(named: "profile.png"), options: .progressiveDownload, completed: nil)

        name = user.first_name! + " " + user.last_name!
        role = user.role ?? "---"
        email = user.email ?? "---"
        phone = user.mobile ?? ""
        position = user.position ?? ""
        company = user.company ?? ""

        if phone.isEmpty {
            phone = "NA"
        }
        if position.isEmpty {
            position = "NA"
        }
        if company.isEmpty {
            company = "NA"
        }

        tableView.reloadData()
        if let qrString = user.qrcode, let url = URL(string: qrString) {
            do {
                let data = try Data.init(contentsOf: url)
                let image = UIImage.init(data: data)
                QRCodeImageView.image = image
            } catch {
                print(error)
            }
        }
    }
    
    func populateChatUserDetails() {
        let avatarURL = chatUser.avatar ?? ""
        profileImageView.sd_setShowActivityIndicatorView(true)
        profileImageView.sd_setIndicatorStyle(.gray)
        profileImageView.sd_setImage(with: URL(string: avatarURL), placeholderImage: UIImage(named: "profile.png"), options: .progressiveDownload, completed: nil)
        
        name = chatUser.firstName! + " " + chatUser.lastName!
        role = chatUser.role ?? ""
        email = chatUser.email ?? ""
        phone = chatUser.phone ?? ""
        position = chatUser.position ?? ""
        company = chatUser.company ?? ""

        if phone.isEmpty {
            phone = "NA"
        }
        if position.isEmpty {
            position = "NA"
        }
        if company.isEmpty {
            company = "NA"
        }

        tableView.reloadData()
        if let qrString = chatUser.qrCode, let url = URL(string: qrString) {
            do {
                let data = try Data.init(contentsOf: url)
                let image = UIImage.init(data: data)
                QRCodeImageView.image = image
            } catch {
                print(error)
            }
        }
    }
    
    func updateUserWithImage() {
        
        let imageData = profileImage.jpegData(compressionQuality: 0.5) 
        

        let userId = user.user_id!
        let url = "http://eu.aio-events.com/api/users/\(userId)" /* your API url */
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        self.showHUD(forView: self.view, excludeViews: [])
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String

        let params = ["user_token": auth_Token!,
                      "api_token": apiToken]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData {
                multipartFormData.append(data, withName: "avatar", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .put, headers: headers) { (result) in
            self.hideHUD(forView: self.view)
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let _ = response.error{
                        return
                    }
                    let json = try? JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                    if let jsonDict = json as? NSDictionary {
                        let loginModal = LoginModal(responseDict: jsonDict)
                        MyDatabase.saveLoginDataLocally(loginModal: loginModal)

                    }
                }
            case .failure(let error):
                self.hideHUD(forView: self.view)
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }

    @IBAction func showImageOptionsTapped(_ sender: UIButton) {

        guard let _ = self.user else {
            return
        }
        
        let alert = UIAlertController(title: "Choose Image".localized, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { _ in
            self.camera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery".localized, style: .default, handler: { _ in
            self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
        

        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func shareButtonTapped(_ sender: UIButton) {

        var phoneNumber = ""
        var position = ""
        var company = ""
        if let chatUser = chatUser {
            phoneNumber = chatUser.phone
            position = chatUser.position
            company = chatUser.company
        } else {
            phoneNumber = user.mobile ?? ""
            position = user.position ?? ""
            company = user.company ?? ""
        }

        var text = "Name - ".localized + "\(name)\n"
        text += "Email - ".localized + "\(email)\n"
        if !phoneNumber.isEmpty {
            text += "Phone Number - ".localized + "\(phoneNumber)\n"
        }
        if !company.isEmpty {
            text += "Company - ".localized + "\(company)\n"
        }
        if !position.isEmpty {
            text += "Position - ".localized + "\(position)"
        }
        
        
        if let image = profileImageView.image {
            let activityController = UIActivityViewController(activityItems: [text, image], applicationActivities: nil)
            self.present(activityController, animated: true, completion: nil)
        }

    }
    
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = UIImagePickerController.InfoKey.self as! UIImage
        
//        info[UIImagePickerControllerOriginalImage] as! UIImage
        
        
        self.profileImage = image
        profileImageView.image = image
        isImageSelected = true
        dismiss(animated:true, completion: nil)
        updateUserWithImage()
    }
    
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCell.cellIdentifier(), for: indexPath) as! ProfileTableViewCell
        var value = ""
        var imageName = ""
        switch indexPath.row {
        case 0:
            value = name
            imageName = "user-avatar.png"
        case 1:
            value = role
            imageName = "role.png"
        case 2:
            value = email
            imageName = "mail-send.png"
        case 3:
            value = position
            imageName = "position"
        case 4:
            value = company
            imageName = "organization.png"
        case 5:
            value = phone
            imageName = "phone-call.png"
        default:
            break
        }
        let color: UIColor!
        if let color_Value  =  UserDefaults.standard.value(forKey: "color_Value") as? String {
            color = UIColor.colorWithHexString(hex: color_Value)
        }else{
            color = UIColor.init(red: 0.0/255.0, green: 195.0/255.0, blue: 231/255.0, alpha: 1)
        }
        cell.cellImageView.backgroundColor = color
        cell.nameLabel.text = value
        cell.cellImageView.image = UIImage(named: imageName)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 1 ? 0 : 70.0
    }
    
}


