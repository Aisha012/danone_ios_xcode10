//
//  PollOptionTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 17/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class PollOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var radioButtonOuterView: UIView!
    @IBOutlet weak var radioButtonInnerView: UIView!
    
    class func cellIdentifier() -> String {
        return "PollOptionTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.holderView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.holderView.layer.shadowColor = UIColor.black.cgColor
        self.holderView.layer.shadowRadius = 4
        self.holderView.layer.cornerRadius = 5
        self.holderView.layer.shadowOpacity = 0.3
        self.holderView.layer.masksToBounds = false
        self.holderView.layer.rasterizationScale = UIScreen.main.scale
        self.holderView.clipsToBounds = false
        
        self.radioButtonInnerView.layer.cornerRadius = 10.0
        self.radioButtonOuterView.layer.cornerRadius = 15.0
        self.radioButtonOuterView.backgroundColor = UIColor.clear
//        self.radioButtonInnerView.backgroundColor = UIColor.clear
        self.radioButtonOuterView.layer.borderWidth = 1.5
//        self.radioButtonOuterView.layer.borderColor = UIColor.black.cgColor
    }

    func configureCell(option: PollOption, isSelected: Bool, isAlreadyAnswered: Bool) {
        self.answerLabel.text = option.option
//        var selectedColor = UIColor.init(red: 0.0/255.0, green: 195.0/255.0, blue: 231/255.0, alpha: 1)
//
//        if let color_Value  =  UserDefaults.standard.value(forKey: "color_Value") as? String {
//            selectedColor =  UIColor.colorWithHexString(hex: color_Value)
//        }

        self.holderView.backgroundColor = UIColor.white
        if isAlreadyAnswered {
            radioButtonOuterView.layer.borderColor = UIColor.lightGray.cgColor
            radioButtonInnerView.backgroundColor = isSelected ? UIColor.lightGray : UIColor.clear
            self.answerLabel.textColor = UIColor.lightGray
        } else {
            radioButtonOuterView.layer.borderColor = UIColor.black.cgColor
            radioButtonInnerView.backgroundColor = isSelected ? UIColor.black : UIColor.clear
            self.answerLabel.textColor = UIColor.black
        }
    }

}
