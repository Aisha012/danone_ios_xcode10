//
//  AgendaDetailTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 18/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class AgendaDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var timingLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "AgendaDetailTableViewCell"
    }
    
    func configureCell(agenda: AgendaListModal) {
        titleLabel.text = agenda.title!
        let startDate1 = agenda.start_date ?? "2018-02-22T10:46:00.000Z"
        let index = startDate1.index(startDate1.startIndex, offsetBy: 16)
        let myDateString = String(startDate1[..<index])
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"

        let dayDate = DateExtension.changeDateFormat(date: myDateString, getDateFormat: "yyyy-MM-dd'T'HH:mm", desiredDateFormat: "EEE, dd yyyy")
        dayLabel.text = dayDate
        
        let startDate = AgendaListingVc.convertDate(getDate: (agenda.start_date)!)
        let endDate  = AgendaListingVc.convertDate(getDate: (agenda.end_date)!)
        
        timingLabel.text = startDate + " - " + endDate
    }
    
}
