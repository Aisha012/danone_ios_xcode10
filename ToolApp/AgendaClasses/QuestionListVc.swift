//
//  QuestionListVc.swift
//  CCBT Unified
//
//  Created by devlopment on 07/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import Starscream
import ActionCableClient
import Alamofire

class QuestionListVc: BaseClassVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!

    var showTextField = false
    var questionPagination = QuestionPagination()
    var eventId: String?
    var agendaModel: AgendaListModal?
    let client = ActionCableClient(url: URL(string: "ws://yourevent2go.com/cable?user_token=\(UserDefaults.standard.value(forKey: "Auth_Token") as? String ?? "")")!)
    var roomChannel: Channel!
    var request: DataRequest!
    var isFirstTime = true
    var isFetchingData = false

    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
        menuButton.setImage(nil, for: .normal)
        backImage.image = UIImage(named: "back.png")
        tableView.tableFooterView = UIView()
        titleLabel.text = "Questions".localized
        tableView.estimatedRowHeight = 124.0
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        subscribeToChannel()
        fetchQuestions()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        roomChannel?.unsubscribe()
        client.disconnect()
    }

    @objc func refreshData(_ sender: Any) {
        self.questionPagination.paginationType = .reload
        self.questionPagination.currentPageNumber = 1
        fetchQuestions()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    @IBAction func searchIconTapped(_ sender: UIButton) {
        showTextField = !showTextField
        animateTextField()
    }

    @IBAction func valueChangedInSearchField(_ sender: Any) {
        if let req = request {
            req.cancel()
        }
        fetchQuestions(searchTextField.text!)
    }
    
    @IBAction func askQuestionButtonTapped(_ sender: UIButton) {
        let destination  =  self.storyboard?.instantiateViewController(withIdentifier: "AskQuestionVC") as! AskQuestionVC
        destination.eventId  =  self.eventId
        destination.agendaModel = self.agendaModel
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func animateTextField() {
        if showTextField {
            searchTextField.becomeFirstResponder()
        } else {
            searchTextField.text = ""
            searchTextField.resignFirstResponder()
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.tableViewTopConstraint.constant = self.showTextField ? 58 : 8
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }

    func subscribeToChannel() {
        
        self.client.willConnect = {
            print("Will Connect")
        }
        
        self.client.onConnected = {
            self.roomChannel.subscribe()
            print("Connected to \(self.client.url)")
        }
        
        self.client.onDisconnected = {(error: ConnectionError?) in
            print("Disconected with error: \(String(describing: error))")
        }
        
        self.client.willReconnect = {
            print("Reconnecting to \(self.client.url)")
            return true
        }
        
        
        let room_identifier = ["pooling_id" : agendaModel?.agendaListId! ?? "0"]
        self.roomChannel = client.create("PoolingsChannel", identifier: room_identifier, autoSubscribe: false, bufferActions: true)
        
        roomChannel.onReceive = { (JSON : Any?, error : Error?) in
            
            if error == nil, let JSONObject = JSON as? NSDictionary,
                let questionDict = JSONObject["message"] as? NSDictionary {
                let question = Question(responseDict: questionDict)
                if question.type == "add" {
                    self.questionPagination.questions.append(question)
                } else {
                    
                    let index = self.questionPagination.questions.index(where: { (ques) -> Bool in
                        question.id == ques.id
                    })
                    if let questIndex = index {
                        self.questionPagination.questions.remove(at: questIndex)
                    }
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        
        self.roomChannel.onSubscribed = {
            print("Yay!")
        }
        
        self.roomChannel.onUnsubscribed = {
            print("Unsubscribed")
        }
        
        self.roomChannel.onRejected = {
            print("Rejected")
        }
        
        client.connect()
        
    }

//    func fetchQuestionsList(_ query: String = "") {
//        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
//        if let auth_Token = auth_Token {
//            self.showHUD(forView: self.view, excludeViews: [])
//            let urlString = "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/\(agendaModel?.agendaListId! ?? "0")/questions?user_token=\(auth_Token)&api_token=\(apiToken)&query=\(query)"
//            request = CallApi.getData(url: urlString, parameter: nil, type: .get) { (netResponse) -> (Void)
//                in
//                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
//                    self.questions = [Question]()
//                    if let JSON = netResponse.responseDict as? [NSDictionary] {
//                        for questionDict in JSON {
//                            let question = Question(responseDict: questionDict)
//                            self.questions.append(question)
//                        }
//                        self.showDataAccordingly()
//                    } else {
//                        self.showDataAccordingly()
//                    }
//                    self.hideHUD(forView: self.view)
//                }else {
//                    self.hideHUD(forView: self.view)
//                    self.showDataAccordingly()
//                }
//            }
//        }
//    }
    
    func fetchQuestions(_ query: String = "") {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            if questionPagination.paginationType != .old {
                self.questionPagination.currentPageNumber = 1
                if isFirstTime == true {
                    self.showHUD(forView: self.view, excludeViews: [])
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                }
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            isFetchingData = true
            let urlString = "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/\(agendaModel?.agendaListId! ?? "0")/questions?user_token=\(auth_Token)&api_token=\(apiToken)&page=\(questionPagination.currentPageNumber)&query=\(query)"
            request = CallApi.getData(url: urlString, parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    self.isFetchingData = false
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        let newPagination = QuestionPagination(array: JSON)
                        self.questionPagination.appendDataFromObject(newPagination)
                        DispatchQueue.main.async {
                            if self.questionPagination.paginationType != .old && self.isFirstTime == true {
                                self.hideHUD(forView: self.view)
                                self.isFirstTime = false
                            } else {
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                            self.showDataAccordingly()
                        }
                    } else {
                        self.hideHUD(forView: self.view)
                    }
                }
            }
        }
    }
    
    func showDataAccordingly() {
        if questionPagination.questions.count == 0 {
            emptyPlaceholderLabel.isHidden = false
            tableView.reloadData()
            tableView.isHidden = true
        } else {
            emptyPlaceholderLabel.isHidden = true
            tableView.reloadData()
            tableView.isHidden = false
        }
    }
    
    func upVoteQuestion(questionId: String, atIndex index: Int) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/\(agendaModel?.agendaListId! ?? "0")/questions/\(questionId)/upvote?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .post) { (netResponse) -> (Void) in
                
                self.hideHUD(forView: self.view)
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    if let _ = netResponse.responseDict as? NSDictionary {
                        self.questionPagination.questions[index].hasVoted = true
                        self.questionPagination.questions[index].votes += 1
                        DispatchQueue.main.async {
                            self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
                        }
                    }
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }
        }
    }
    
    func downVoteQuestion(questionId: String, atIndex index: Int) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/agendas/\(agendaModel?.agendaListId! ?? "0")/questions/\(questionId)/downvote?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .post) { (netResponse) -> (Void) in
                
                self.hideHUD(forView: self.view)
                
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let _ = netResponse.responseDict as? NSDictionary {
                        self.questionPagination.questions[index].hasVoted = false
                        self.questionPagination.questions[index].votes -= 1
                        DispatchQueue.main.async {
                            self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
                        }
                    }
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }
        }
    }

    
}
extension QuestionListVc : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard questionPagination.questions.count > 0  else {
            return 0
        }
        return questionPagination.questions.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < questionPagination.questions.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }

        let cell  = tableView.dequeueReusableCell(withIdentifier: "QuestionListCell") as! QuestionListCell
        cell.configureCell(question: questionPagination.questions[indexPath.row], atIndex: indexPath.row)
        cell.delegate = self
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let destination  =  self.storyboard?.instantiateViewController(withIdentifier: "AnswerViewController") as! AnswerViewController
//        destination.eventId  =  self.eventId
//        destination.agendaId = self.agendaModel?.agendaListId
//        destination.questionId = questions[indexPath.row].id
//        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row < questionPagination.questions.count else {
            return questionPagination.hasMoreToLoad ? 50.0 : 0.0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.questionPagination.questions.count && self.questionPagination.hasMoreToLoad && !isFetchingData {
            self.questionPagination.paginationType = .old
            fetchQuestions(searchTextField.text!)
        }
    }

    
}

extension QuestionListVc: QuestionListCellDelegate {
    
    func likeButtonTapped(_ index: Int) {
        if questionPagination.questions[index].hasVoted {
            downVoteQuestion(questionId: String(questionPagination.questions[index].id), atIndex: index)
        } else {
            upVoteQuestion(questionId: String(questionPagination.questions[index].id), atIndex: index)
        }
    }
}
